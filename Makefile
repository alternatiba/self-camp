.DEFAULT_GOAL := full-start

### DOCKER_COMPOSE VARIABLES ###

# The base config is in `docker-compose.yml`
DOCKER_COMPOSE=docker compose -f docker-compose.yml
# In a local environment, add the `docker-compose.local.yml` configuration and env variables from `.env.local` file
DOCKER_COMPOSE_LOCAL=$(DOCKER_COMPOSE) -f docker-compose.local.yml
# For local development, add the `docker-compose.local.dev.yml` configuration to `docker-compose.local.yml`
DOCKER_COMPOSE_DEV=$(DOCKER_COMPOSE_LOCAL) -f docker-compose.local.dev.yml --env-file docker/dev-env/.env --env-file docker/dev-env/.env.website
# To deploy in production, add config from `docker-compose.prod.yml` and env variables from `.env.prod` file
DOCKER_COMPOSE_PROD=$(DOCKER_COMPOSE) -f docker-compose.prod.yml --env-file docker/prod-env/.env

# Listing of different services of the app
SERVICES=api inscription-front orga-front website

############################
########### LOGS ###########
############################

log:
	$(DOCKER_COMPOSE) logs -f --tail=1000 api
log-inscription-front:
	$(DOCKER_COMPOSE) logs -f --tail=1000 inscription-front
log-orga-front:
	$(DOCKER_COMPOSE) logs -f --tail=1000 orga-front
log-website:
	$(DOCKER_COMPOSE) logs -f --tail=1000 website
log-front:
	$(DOCKER_COMPOSE) logs -f --tail=1000 orga-front inscription-front
log-all:
	$(DOCKER_COMPOSE) logs -f --tail=1000 api orga-front inscription-front website

log-list-files:
	ls -l api/logs


############################
###### INITIALIZATION ######
############################

### DEVELOPMENT ###

init: install-deps-global-project build install-deps-full
init-prod: build install-deps-prod-full

install-deps-global-project:
	npm install

install-deps-full: install-deps-api install-deps-front install-deps-website
install-deps-api:
	$(DOCKER_COMPOSE) run --no-deps api npm install --omit=optional
install-deps-shared-front:
	$(DOCKER_COMPOSE) run --no-deps orga-front bash -c "npm install --legacy-peer-deps --prefix ../shared/front"
install-deps-front: install-deps-shared-front sync-node_modules-front sync-package-front
install-deps-website:
	$(DOCKER_COMPOSE) run --no-deps website npm install

install-deps-prod-full: install-deps-prod-api install-deps-prod-front install-deps-prod-website
install-deps-prod-api:
	$(DOCKER_COMPOSE_PROD) run --no-deps api npm ci --omit=optional
install-deps-prod-shared-front:
	$(DOCKER_COMPOSE_PROD) run --no-deps orga-front bash -c "npm ci --legacy-peer-deps --prefix ../shared/front"
install-deps-prod-front: install-deps-prod-shared-front sync-node_modules-front sync-package-front
install-deps-prod-website:
	$(DOCKER_COMPOSE_PROD) run --no-deps website npm ci

sync-package-front:
	# Copy package.json from shared/front/ to orga-front/ and inscription-front/
	cp -rf shared/front/package.json orga-front/
	cp -rf shared/front/package.json inscription-front/
	# Copy package-lock.json from shared/front/ to orga-front/ and inscription-front/
	cp -rf shared/front/package-lock.json orga-front/
	cp -rf shared/front/package-lock.json inscription-front/
sync-node_modules-front:
	# Copy node_modules from shared/front/ to orga-front/ and inscription-front/
	rm -rf inscription-front/node_modules
	cp -rf shared/front/node_modules inscription-front/node_modules
	rm -rf orga-front/node_modules
	cp -rf shared/front/node_modules orga-front/node_modules

############################
#### DEVELOPMENT START #####
############################

# Start all containers
full-start:
	$(DOCKER_COMPOSE_DEV) up -d api inscription-front orga-front
full-start-optional:
	$(DOCKER_COMPOSE_DEV) up -d

# In dev, allow soft restart without recreating the container for convenience
start-db:
	$(DOCKER_COMPOSE_DEV) up -d db
start-api:
	$(DOCKER_COMPOSE_DEV) up -d api --force-recreate
start-orga-front:
	$(DOCKER_COMPOSE_DEV) up -d orga-front
start-inscription-front:
	$(DOCKER_COMPOSE_DEV) up -d inscription-front
start-website:
	$(DOCKER_COMPOSE_DEV) up -d website

# Local production build for frontends
start-build-orga-front: stop-orga-front
	$(DOCKER_COMPOSE) up -d orga-front
start-build-inscription-front: stop-inscription-front
	$(DOCKER_COMPOSE) up -d inscription-front
start-build-website: stop-website
	$(DOCKER_COMPOSE) up -d website

# Testing apps
test-api:
	$(DOCKER_COMPOSE_DEV) run --no-deps api npm run test
test-orga-front:
	$(DOCKER_COMPOSE_DEV) run --no-deps orga-front npm run test
test-inscription-front:
	$(DOCKER_COMPOSE_DEV) run --no-deps inscription-front npm run test

# Get analytics about the bundle of frontends
analyze-build-orga-front:
	npm run analyze-build:orga-front
analyze-build-inscription-front:
	npm run analyze-build:inscription-front

# Get analytics about the dependency structure of frontends
analyze-deps-orga-front:
	npm run analyze-deps:orga-front
analyze-deps-inscription-front:
	npm run analyze-deps:inscription-front

# Stop containers
full-stop:
	$(DOCKER_COMPOSE_LOCAL) down
stop-db:
	$(DOCKER_COMPOSE_LOCAL) stop db
stop-api:
	$(DOCKER_COMPOSE) stop api
stop-orga-front:
	$(DOCKER_COMPOSE) stop orga-front
stop-inscription-front:
	$(DOCKER_COMPOSE) stop inscription-front
stop-website:
	$(DOCKER_COMPOSE) stop website

# Force restart for containers
full-restart: full-stop full-start
restart-db: stop-db start-db
restart-api: stop-api start-api
restart-orga-front: stop-orga-front start-orga-front
restart-inscription-front: stop-inscription-front start-inscription-front
restart-website: stop-website start-website

# Allows to build the frontends without starting the servers
build-prod-orga-front:
	$(DOCKER_COMPOSE) run --no-deps orga-front npm run build:with-timestamp
build-prod-inscription-front:
	$(DOCKER_COMPOSE) run --no-deps inscription-front npm run build:with-timestamp

############################
#### PRODUCTION  START #####
############################

# Start all containers
full-start-prod:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate api inscription-front orga-front traefik
full-start-prod-optional:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate

# Stop containers
full-stop-prod:
	$(DOCKER_COMPOSE_PROD) down
stop-prod-api: stop-api
stop-prod-orga-front: stop-orga-front
stop-prod-inscription-front: stop-inscription-front
stop-prod-website: stop-website

# Force restart for containers (no soft restart in production, we use --force-recreate)
full-restart-prod: full-stop-prod full-start-prod
start-prod-traefik:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate traefik
start-prod-api:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate api
start-prod-orga-front:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate orga-front
start-prod-inscription-front:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate inscription-front
start-prod-website:
	$(DOCKER_COMPOSE_PROD) up -d --force-recreate website

# Aliases for comfort
restart-prod-traefik: start-prod-traefik
restart-prod-api: start-prod-api
restart-prod-orga-front: start-prod-orga-front
restart-prod-inscription-front: start-prod-inscription-front
restart-prod-website: start-prod-website

############################
### LOCAL NETWORK START ####
############################

full-start-local:
	$(DOCKER_COMPOSE_LOCAL) up -d api inscription-front orga-front


########################
######## OTHERS ########
########################

### CONTINUOUS SYNC FOR FRONTENDS SHARED FOLDER ###

continuously-sync-frontends-shared-folder:
	node scripts/continuouslySyncFrontendsSharedFolder.js
check-folders-are-synced:
	node scripts/checkFoldersAreSynced.js orga-front/src/shared/ inscription-front/src/shared/
sync-folders:
	node scripts/syncFolders.js orga-front/src/shared/ inscription-front/src/shared/
	
### REORDER TRANSLATIONS ALPHABETICALLY ###

sort-translations:
	yaml-sort --input shared/locales/**/*.yml --lineWidth 120 --quotingStyle double

### DEPLOY TO PROD AND SANDBOX ###

deploy:
	./scripts/deploy.sh

### DOCKER ###

# Launch the building process for all the containers
build:
	$(DOCKER_COMPOSE) build api
	$(DOCKER_COMPOSE) build orga-front
	$(DOCKER_COMPOSE) build inscription-front
	$(DOCKER_COMPOSE) build website

# Clean all the build and temporary assets in all folders
clean:
	for service in $(SERVICES) ; do \
		$(DOCKER_COMPOSE) run --no-deps $$service npm run clean ; \
	done

# Kill and remove all docker containers
kill:
	$(DOCKER_COMPOSE_LOCAL) kill
	$(DOCKER_COMPOSE_LOCAL) rm -v

### WEBSITE UTILS

write-translations-website:
	$(DOCKER_COMPOSE_DEV) run --no-deps website npm run write-translations -- --locale fr --messagePrefix 'TODO '
	$(DOCKER_COMPOSE_DEV) run --no-deps website npm run write-translations -- --locale en --messagePrefix 'TODO '

### LINTING & FORMATTING ###

# Make a manual `npm run prettier` in all subdirectories
prettier:
	npm run prettier

# Make a manual `npm run prettier-check` in all subdirectories
prettier-check:
	npm run prettier-check

# Make a manual `npm run lint` in all subdirectories
lint:
	for service in $(SERVICES) ; do \
		npm run lint --prefix ./$$service ; \
	done

# Make a manual `npm run lint-show` in all subdirectories
lint-show:
	for service in $(SERVICES) ; do \
		npm run lint-show --prefix ./$$service || exit 1 ; \
	done