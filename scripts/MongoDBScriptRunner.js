const MongoClient = require("mongodb").MongoClient;
LOCAL_MONGO_URI = "mongodb://noe:noe@localhost:27017";
LOCAL_DB_NAME = "main";

/**
 * Easily run operations on your MongoDB database
 *
 * Use it like this:
 *
 * const myFunctionToRun = (db, collections) => {
 *   // Do what you want to do
 * }
 *
 * // To run on the local database:
 * MongoDBScriptRunner.runScript(myFunctionToRun);
 *
 * // To run on a remote database:
 * MongoDBScriptRunner.runScript(myFunctionToRun, MY_MONGO_URI, MY_DB_NAME);
 */
const MongoDBScriptRunner = {
  runScript: function (runFn, mongoUri = LOCAL_MONGO_URI, dbName = LOCAL_DB_NAME) {
    MongoClient.connect(mongoUri + "/" + dbName, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then(async (client) => {
      const db = await client.db(dbName);
      const collectionsArray = await db.collections();
      const collections = Object.fromEntries(
        collectionsArray.map((coll) => [coll.s.namespace.collection, coll])
      );
      try {
        await runFn(db, collections);
      } finally {
        await client.close();
      }
    });
  },
};

module.exports = MongoDBScriptRunner;
