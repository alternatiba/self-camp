# Import de participant·es

L'import de participant·es vous permet d'ajouter rapidement plusieurs participant·es à votre événement en utilisant un fichier CSV.

## Processus d'import

1. Téléchargez le fichier d'import d'exemple.

   > Dans le pop-up d'import, cliquez sur "Télécharger le fichier d'exemple" pour télécharger le fichier. Ce fichier contient les colonnes nécessaires et des exemples de données pour vous aider à formater correctement votre fichier d'import.

2. Ouvrez le fichier d'import d'exemple dans votre logiciel de tableur favori.

   > Si vous ne savez pas comment faire pour ouvrir le fichier, n'hésitez pas à rechercher comment faire sur le web avec l'outil que vous utilisez.

3. Préparez votre fichier CSV en suivant les conseils sur cette page.
4. Importez votre fichier CSV dans NOÉ.
5. Corrigez les erreurs d'import s'il y en a, et vérifiez l'aperçu des données à importer dans le tableau qui s'affiche.
6. Si tout est correct, validez l'import en cliquant sur "Importer les éléments".

## Colonnes de base

Le fichier d'import contient les colonnes suivantes :

| Nom de la colonne | Obligatoire | Format                               |
| ----------------- | ----------- | ------------------------------------ |
| Prénom            | Oui         | Texte simple                         |
| Nom               | Oui         | Texte simple                         |
| Email             | Oui         | Texte simple                         |
| Date d'arrivée    | Oui         | [Date](#format-date)                 |
| Date de départ    | Oui         | [Date](#format-date)                 |
| Encadrant⋅e lié⋅e |             | [ID de l'encadrant⋅e](#format-id)    |
| Tags              |             | [Tableau JSON](#format-tableau-json) |

## Importer des réponses au formulaire d'inscription

Vous pouvez inclure les réponses au formulaire d'inscription directement dans votre fichier d'import. Après les colonnes existantes, il vous suffit d'ajouter des colonnes pour les réponses aux questions du formulaire d'inscription que vous avez envie de pré-remplir.

:::warning

Il faut que vous ayiez déjà créé et paramétré les questions de votre formulaire d'inscription pour pouvoir importer des réponses à ces questions.

:::

D'abord, nous devons retrouver la clé de la question de formulaire que nous souhaitons pré-remplir.

1. Dans le formulaire d'inscription (page "Configuration" > "Formulaire"), dépliez la question à importer.

2. Allez dans l'onglet "Avancé" votre question et récupérez sa clé dans le champ "Clé en base de données" (pour la petite histoire, cette clé est le nom sous lequel sont stockées les réponses à la question dans la base de données de NOÉ).

   > _Exemple :_ La clé a souvent une forme du style `maQuestionEnToutAttache_9z7` avec trois caractères aléatoires à la fin (ici `9z7`).

Cette clé va devenir le titre de la colonne correspondante dans votre fichier d'import. Maintenant, nous pouvons remplir no :

1. Ajoutez une colonne pour chaque question du formulaire que vous souhaitez pré-remplir, avec à chaque fois, la clé de la question en guise de titre de colonne.

2. Remplissez les cellules avec les réponses appropriées, et dans le format approprié. **Attention, selon le type de votre question, le format de la réponse est différent !** Voici une table des correspondances sur les différents types de questions et le format de la réponse :

   | Type de question                         | Format de la réponse                                                  |
   | ---------------------------------------- | --------------------------------------------------------------------- |
   | Texte court                              | Texte simple                                                          |
   | Paragraphe                               | Texte simple (utilisez `\n` pour les retours à la ligne)              |
   | Téléphone                                | Texte simple (utilisez le format international : `+33 6 12 34 56 78`) |
   | Checkbox / validation                    | Texte simple (`true` ou `false`)                                      |
   | Choix multiples (un choix possible)      | [Clé de réponse simple](#format-clé-de-réponse)                       |
   | Select (un choix possible)               | [Clé de réponse simple](#format-clé-de-réponse)                       |
   | Checkboxes (plusieurs choix possibles)   | [Tableau JSON de clés de réponses](#format-clé-de-réponse)            |
   | Multi-select (plusieurs choix possibles) | [Tableau JSON de clés de réponses](#format-clé-de-réponse)            |
   | URL                                      | Texte simple (`https://exemple.com`)                                  |
   | Email                                    | Texte simple (`exemple@domaine.com`)                                  |
   | Nombre                                   | Texte simple (`42`)                                                   |
   | Date et heure                            | [Date](#format-date) (`DD/MM/YYYY HH:mm`)                             |
   | Jour                                     | [Date](#format-date) (`DD/MM/YYYY`)                                   |

:::warning **Attention à bien mettre vos cellules en format Texte.**

Voir l'encadré "Erreurs d'import et format de cellules" juste dessous

:::

## Format des colonnes

<a id="erreurs-dimport-et-format-de-cellules"></a>
:::info Erreurs d'import et format de cellules

Les logiciels de tableur formattent généralement les dates en cellules de dates, etc. Mais quand ils rééexportent ça en CSV, parfois le résultat est innatendu. pour cette raison, on passe le format de toutes les cellules en Texte, ce qui nous permet de voir ce qui sera réellement écrit dans le CSV final.

Si jamais NOÉ vous dit qu'il y a une erreur avec le format de vos dates, c'est donc souvent que vous devez mettre les cellules au format "Texte" dans tout votre document (recherchez "changer format de cellule \<votre logiciel\>" dans votre moteur de recherche préféré).

:::

### Format date

Vous devez nécessairement rentrer les dates au format "DD/MM/YYYY HH:mm" (oui, on est encore un peu rigide sur ça, on travaille à rendre ça plus flexible).

> _Exemple :_ `31/12/2024 14:30`

### Format ID

Certaines entrées sont des références à des objets existants. Par exemple, l'encadrant⋅e lié⋅e à un participant⋅e est une référence à un·e encadrant⋅e existant·e.

Pour aller chercher l'ID d'un objet existant, il suffit de regarder l'URL de la page de l'objet. Par exemple, allez sur la liste des encadrant⋅es, cliquez sur un·e encadrant⋅e pour l'afficher en pleine page. L'ID de votre objet se trouve à la fin de l'URL de la page.

> _Exemple :_ Dans https://orga.noe-app.io/mon-evenement/stewards/64ff3f4b40110eab4dc9f556, l'ID de l'encadrant⋅e est `64ff3f4b40110eab4dc9f556`.

### Format tableau JSON

Pour les entrées qui sont au format liste, il faut que vous les entriez au format de tableau JSON. Cela consiste à mettre les valeurs entre crochet, séparées par des virgules. On entoure ensuite tout ça avec des crochets [].

> _Exemple :_ `["valeur1", "valeur2", "valeur3"]`

### Format clé de réponse

Sur le même principe que la clé de bases de données de la question, chaque réponse de formulaire a une clé qui lui correspond.

> _Exemple :_ Si une question "Majorité" a deux options "J'ai plus de 18 ans" et "J'ai moins de 18 ans", les clés de réponse ressembleront à quelque chose comme `jAiPlusDe18Ans_xt6` et `jAiMoinsDe18Ans_6g3`.

On trouve ces clés dans la liste des options de cette question dans le formulaire, avec l'appellation "Clé en BDD".

#### Format clé de réponse simple

Pour des réponses à **choix unique**, il faudra donc copier la clé de la réponse dans la cellule.

> _Exemple :_ `jAiPlusDe18Ans_xt6`.

#### Format tableau JSON de clés de réponses

Pour des réponses à **choix multiple**, il faudra copier toutes les clés des réponses dans la cellule, sous le [format tableau JSON](#format-tableau-json).

> _Exemple :_ `["jAimeLeBleu_du8", "jAimeLeRouge_62z"]`.

## Le mot de la fin

N'oubliez pas que l'import CSV est une fonctionnalité expérimentale. Soyez attentif aux résultats et signalez tout problème à l'équipe NOÉ.

Pour toute question ou assistance supplémentaire, n'hésitez pas à contacter l'équipe NOÉ.
