---
sidebar_position: 3
---

# Champs imbriqués

Les champs imbriqués sont des champs qui contiennent d'autres champs. Par exemple, un partie des objets NOÉ ont un champ
`customFields` qui stocke toutes les valeurs des champs personnalisés de l'objet&nbsp;:

```json
{
  "id": "1234567890",
  "name": "Mon objet",
  ...
  "customFields": {
    "field1": "customValue1",
    "field2": "customValue2"
  }
}
```

## Modification partielle

Parfois, vous ne voulez modifier qu'une seule valeur d'un champ imbriqué. Par exemple, vous voulez modifier uniquement
la valeur de `field1` de `customFields`, et ne pas modifier le reste des champs. Pour cela, vous pouvez chaîner les champs imbriqués avec un point `.`&nbsp;:

#### Requête de modification
```json
{
  "id": "1234567890",
  "name": "Mon objet modifié",
  ...
  "customFields.field1": "customValue1Modifiée",
  "customFields.field3": "customValue3Nouvelle"
}
```

#### Résultat
```json
{
  "id": "1234567890",
  "name": "Mon objet modifié",
  ...
  "customFields": {
    "field1": "customValue1Modifiée",
    "field2": "customValue2",
    "field3": "customValue3Nouvelle"
  }
}
```

:::info Effacer un champ

Pour effacer un champ, il suffit de soumettre la valeur `null` :

```json
{
  "id": "1234567890",
  "name": "Mon objet modifié",
  ...
  "customFields.field1": null
}
```
:::

## Modification totale

Si à l'inverse vous voulez remplacer intégralement le champ `customFields`, vous pouvez le faire en soumettant directement un objet&nbsp;:

#### Requête de modification
```json
{
  "id": "1234567890",
  "name": "Mon objet modifié",
  ...
  "customFields": {
    "field3": "customValue3"
  }
}
```

#### Résultat
```json
{
  "id": "1234567890",
  "name": "Mon objet modifié",
  ...
  "customFields": {
    "field3": "customValue3"
  }
}
```