---
sidebar_position: 2
---

# Déployer l'application

Nous vous invitons à vous reporter au [fichier `CONTRIBUTING.md`](https://gitlab.com/noe-app/noe/-/blob/master/CONTRIBUTING.md#d%C3%A9ployer-lapplication) disponible sur notre dépôt GitLab.

Plus d'informations seront disponibles sur ce site internet bientôt.
