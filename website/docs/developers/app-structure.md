---
sidebar_position: 1
---

# Structure de l'application

Vous comprendrez ici comment se structure l'application NOÉ et quelles technologies nous utilisons.

## Division en micro-services

L'application est divisée en cinq micro-services :

- deux frontends qui vivent dans `/orga-front` (frontend orga) et `/inscription-front` (frontend participant⋅e)
- un backend API qui vit dans `/api`
- un second backend dédié aux calculs d'IA qui vit dans `/ia-back` (mais qui n'existe plus car il n'était pas utilisé)
- un site internet contenant la documentation, qui vit dans `website`

> Avant, le backend IA était utilisé au début de l'application mais est tombé en désuétude et n'est plus maintenu pour
> l'instant.

![Structure de l'application](https://docs.google.com/drawings/d/e/2PACX-1vQExKtbzUq3-qQ0I9ttBDyS4UmYkFu55WhCMGmT2OQtvLZxE4SStQ9Cc8F0sHSfdIDdXQTAKfof9PkA/pub?w=1102&amp;h=744)

## Stack technique

### Frontends orga et participant⋅e - `/orga-front` et `/inscription-front`

- **Base :** Javascript
- **Frameworks :** ReactJS / Redux
- **Librairie graphique :** [Ant.Design](https://ant.design/)

### Serveur backend API - `/api`

- **Base :** NodeJS / Typescript (compilé en Javascript avec `esbuild`)
- **Framework :** Express
- **Base de données :** Mongoose (base MongoDB)

### Site internet - `/website`

Basé sur [Docusaurus](https://docusaurus.io/).

## Conteneurisation

L'application est entièrement conteneurisée dans des containers Docker. Nous utilisons Docker Compose pour gérer le
lancement des containers.

La configuration de l'application est décrite dans le fichier `docker-compose.yml` à la racine du projet.

> **Comment installer ça ?**
>
> Pour Linux, allez voir [ici pour Docker](https://linuxize.com/post/how-to-install-and-use-docker-on-debian-10/)
> et [là pour Docker Compose](https://linuxize.com/post/how-to-install-and-use-docker-compose-on-debian-10/)
