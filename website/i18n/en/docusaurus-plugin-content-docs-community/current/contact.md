---
sidebar_position: 2
---

# Contact us

```mdx-code-block
import DocCardLinks from '@site/src/components/DocCardLinks';
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
```

Thanks for your interest in NOÉ! What brings you here?

```mdx-code-block
<DocCardLinks items={[
  {href: "https://tally.so/r/mR0eap", title: "Use NOÉ for my event", icon: "🆕"},
  {href: "https://tally.so/r/npyoaq", title: "Report a bug", icon: "🐛"},
  {href: "https://tally.so/r/3Edl9X", title: "Suggest a new feature", icon: "💡"},
]} />
```

---

Not found what you were looking for?

```mdx-code-block
<DocCardLinks items={[
 {
    href: `mailto:${useDocusaurusContext().siteConfig.customFields.CONTACT_US_EMAIL}?subject=Contact from NOÉ website`,
    title: `Send an email to ${useDocusaurusContext().siteConfig.customFields.CONTACT_US_EMAIL}`,
    description: "Please only send an email for other requests not covered by the other links. We can't handle all community emails otherwise. Thank youuuu!",
    icon: "✉️"
  },
]} />
```
