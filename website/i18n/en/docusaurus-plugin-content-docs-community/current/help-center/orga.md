---
hide_table_of_contents: true
sidebar_label: I am an organizer
title: ""
---

```mdx-code-block
import {NotionEmbedding} from "@site/src/components/NotionEmbedding";

<NotionEmbedding url={"https://v2-embednotion.com/Je-suis-organisateur-ice-94e684116a9649d49796698a68e51579?pvs=4"}/>
```