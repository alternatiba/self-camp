---
sidebar_position: 4
---

# For developers

Everything that might be of interest to people who code is here!

If you want to contribute to NOÉ or understand how to deploy it on your server, you're in the right place.

```mdx-code-block
import DocCardList from '@theme/DocCardList';

<DocCardList />
```