---
sidebar_position: 1
---

# Application structure

Here you will understand how the NOÉ application is structured and what technologies we use.

## Division into micro-services

The application is divided into five micro-services:

- two frontends that live in `/orga-front` (orga frontend) and `/inscription-front` (participant frontend)
- an API backend that lives in `/api`
- a second backend dedicated to AI computations that lives in `/ia-back` (that does not exist anymore, because it has fallen into disuse)
- a website containing the documentation, which lives in `website`


![Application structure](https://docs.google.com/drawings/d/e/2PACX-1vQExKtbzUq3-qQ0I9ttBDyS4UmYkFu55WhCMGmT2OQtvLZxE4SStQ9Cc8F0sHSfdIDdXQTAKfof9PkA/pub?w=1102&amp;h=744)

## Technical stack

### Orga and participant frontends - `/orga-front` and `/inscription-front`

- **Base:** Javascript
- **Frameworks:** ReactJS / Redux
- **Graphic library:** [Ant.Design](https://ant.design/)

### API backend server - `/api`

- **Base:** NodeJS / Typescript (compiled in Javascript with `esbuild`)
- **Framework:** Express
- **Database:** Mongoose (MongoDB database)

### Website - `/website`

Based on [Docusaurus](https://docusaurus.io/).

## Containerization

The application is fully containerized in Docker containers. We use Docker Compose to manage the launch of the containers.

The application configuration is described in the `docker-compose.yml` file at the root of the project.

> **How to install it?**
>
> For Linux, see [here for Docker](https://linuxize.com/post/how-to-install-and-use-docker-on-debian-10/)
> and [here for Docker Compose](https://linuxize.com/post/how-to-install-and-use-docker-compose-on-debian-10/)

Translated with www.DeepL.com/Translator (free version)