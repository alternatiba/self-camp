# Participant Import

Participant import allows you to quickly add multiple participants to your event using a CSV file.

## Import Process

1. Download the sample import file.

   > In the import pop-up, click on "Download the sample file" to download the file. This file contains the necessary columns and sample data to help you correctly format your import file.

2. Open the sample import file in your favorite spreadsheet software.

   > If you're not sure how to open the file, feel free to search online for instructions specific to the tool you're using.

3. Prepare your CSV file following the guidelines on this page.
4. Import your CSV file into NOÉ.
5. Correct any import errors if there are any, and check the preview of the data to be imported in the displayed table.
6. If everything is correct, validate the import by clicking on "Import items".

## Basic Columns

The import file contains the following columns:

| Column Name    | Required | Format                           |
| -------------- | -------- | -------------------------------- |
| First Name     | Yes      | Plain text                       |
| Last Name      | Yes      | Plain text                       |
| Email          | Yes      | Plain text                       |
| Arrival Date   | Yes      | [Date](#date-format)             |
| Departure Date | Yes      | [Date](#date-format)             |
| Linked Steward |          | [Steward ID](#id-format)         |
| Tags           |          | [JSON Array](#json-array-format) |

## Importing Registration Form Responses

You can include responses to the registration form directly in your import file. After the existing columns, simply add columns for the responses to the registration form questions that you want to pre-fill.

:::warning

You must have already created and configured the questions in your registration form to be able to import responses to these questions.

:::

First, we need to find the key of the form question we want to pre-fill.

1. In the registration form (on the "Configuration" > "Form" page), expand the question you want to import.

2. Go to the "Advanced" tab of your question and retrieve its key from the "Database key" field (for the record, this key is the name under which the responses to the question are stored in NOÉ's database).

   > _Example:_ The key often has a format like `myQuestionAllAttached_9z7` with three random characters at the end (here `9z7`).

This key will become the title of the corresponding column in your import file. Now, we can fill in our data:

1. Add a column for each form question you want to pre-fill, with the question key as the column title each time.

2. Fill in the cells with the appropriate answers, and in the appropriate format. **Attention, depending on the type of your question, the answer format is different!** Here's a table of correspondences for the different question types and the answer format:

   | Question Type                   | Answer Format                                              |
   | ------------------------------- | ---------------------------------------------------------- |
   | Short Text                      | Plain text                                                 |
   | Paragraph                       | Plain text (use `\n` for line breaks)                      |
   | Phone                           | Plain text (use international format: `+33 6 12 34 56 78`) |
   | Checkbox / validation           | Plain text (`true` or `false`)                             |
   | Multiple Choice (single choice) | [Simple answer key](#answer-key-format)                    |
   | Select (single choice)          | [Simple answer key](#answer-key-format)                    |
   | Checkboxes (multiple choices)   | [JSON Array of answer keys](#answer-key-format)            |
   | Multi-select (multiple choices) | [JSON Array of answer keys](#answer-key-format)            |
   | URL                             | Plain text (`https://example.com`)                         |
   | Email                           | Plain text (`example@domain.com`)                          |
   | Number                          | Plain text (`42`)                                          |
   | Date and Time                   | [Date](#date-format) (`DD/MM/YYYY HH:mm`)                  |
   | Day                             | [Date](#date-format) (`DD/MM/YYYY`)                        |

:::warning **Be sure to set your cells to Text format.**

See the "Import Errors and Cell Format" box just below

:::

## Column Formats

<a id="import-errors-and-cell-format"></a>
:::info Import Errors and Cell Format

Spreadsheet software usually formats dates in date cells, etc. But when they re-export to CSV, sometimes the result is unexpected. For this reason, we set the format of all cells to Text, which allows us to see what will actually be written in the final CSV.

If NOÉ tells you there's an error with the format of your dates, it's often because you need to set the cells to "Text" format throughout your document (search for "change cell format \<your software\>" in your preferred search engine).

:::

### Date Format

You must enter dates in the "DD/MM/YYYY HH:mm" format (yes, we're still a bit rigid on this, we're working on making it more flexible).

> _Example:_ `31/12/2024 14:30`

### ID Format

Some entries are references to existing objects. For example, the steward linked to a participant is a reference to an existing steward.

To find the ID of an existing object, simply look at the URL of the object's page. For example, go to the list of stewards, click on a steward to display it in full page. The ID of your object is at the end of the page URL.

> _Example:_ In https://orga.noe-app.io/my-event/stewards/64ff3f4b40110eab4dc9f556, the steward's ID is `64ff3f4b40110eab4dc9f556`.

### JSON Array Format

For entries that are in list format, you need to enter them in JSON array format. This consists of putting the values between brackets, separated by commas. We then surround all of this with square brackets [].

> _Example:_ `["value1", "value2", "value3"]`

### Answer Key Format

On the same principle as the database key of the question, each form response has a corresponding key.

> _Example:_ If a "Majority" question has two options "I am over 18" and "I am under 18", the answer keys will look something like `iAmOver18_xt6` and `iAmUnder18_6g3`.

These keys can be found in the list of options for this question in the form, labeled as "DB Key".

#### Simple Answer Key Format

For **single choice** answers, you'll need to copy the answer key into the cell.

> _Example:_ `iAmOver18_xt6`.

#### JSON Array of Answer Keys Format

For **multiple choice** answers, you'll need to copy all the answer keys into the cell, in [JSON array format](#json-array-format).

> _Example:_ `["iLikeBlue_du8", "iLikeRed_62z"]`.

## Final Word

Remember that CSV import is an experimental feature. Pay attention to the results and report any issues to the NOÉ team.

For any questions or additional assistance, don't hesitate to contact the NOÉ team.
