// @ts-ignore
import {themes as prismThemes} from "prism-react-renderer";
// @ts-ignore
import type {Config} from "@docusaurus/types";
// @ts-ignore
import type * as PresetClassic from "@docusaurus/preset-classic";
import type * as PresetPluginContentDocs from "@docusaurus/plugin-content-docs";
import type * as PresetPluginContentBlog from "@docusaurus/plugin-content-blog";
import type * as PresetPluginContentPages from "@docusaurus/plugin-content-pages";

// @ts-ignore
import {WEBSITE_URL, DONATE_URL, DISCORD_INVITE_LINK, GITLAB_URL} from "./config/sharedConfig";

const gitlabEditUrl = `${GITLAB_URL}/-/tree/master/website/`;

const config: Config = {
  title: "NOÉ",
  tagline: "Votre nouveau QG événementiel.",
  headTags: [
    // Schema.org metadata for SEO
    {
      tagName: "script",
      attributes: {
        type: "application/ld+json",
      },
      innerHTML: JSON.stringify({
        "@context": "https://schema.org",
        "@graph": [
          {
            "@type": "WebSite",
            "@id": "website",
            url: WEBSITE_URL,
            name: "NOÉ",
            description:
              "NOÉ : votre QG événementiel open-source. Simplifiez la gestion d'événements : inscriptions, planning bénévoles, programmation. Adieu Excel, bonjour efficacité !",
            image: {"@id": "primaryimage"},
            thumbnailUrl: `${process.env.REACT_APP_API_URL}/assets/images/seo-picture.png`,
            about: {"@id": "noe-app"},
            inLanguage: "fr",
          },
          {
            "@type": "ImageObject",
            "@id": "primaryimage",
            url: `${process.env.REACT_APP_API_URL}/assets/images/seo-picture.png`,
            contentUrl: `${process.env.REACT_APP_API_URL}/assets/images/seo-picture.png`,
            inLanguage: "fr",
          },
          {
            "@type": "WebApplication",
            "@id": "noe-app",
            url: process.env.REACT_APP_ORGA_FRONT_URL,
            name: "NOÉ",
            description:
              "NOÉ : votre QG événementiel open-source. Simplifiez la gestion d'événements : inscriptions, planning bénévoles, programmation. Adieu Excel, bonjour efficacité !",
            image: {"@id": "primaryimage"},
            subjectOf: {"@id": "website"},
            thumbnailUrl: `${process.env.REACT_APP_API_URL}/assets/images/seo-picture.png`,
            inLanguage: "fr",
          },
        ],
      }),
    },
  ],
  url: WEBSITE_URL,
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenAnchors: "throw",
  onBrokenMarkdownLinks: "throw",
  favicon: "favicons/favicon.ico",

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "fr",
    locales: ["fr", "en"],
    localeConfigs: {
      en: {label: "English"},
      fr: {label: "Français"},
    },
  },

  customFields: {
    CONTACT_US_EMAIL: process.env.REACT_APP_CONTACT_US_EMAIL,
    DISCORD_INVITE_LINK,
    DONATE_URL,
    API_DOMAIN: new URL(process.env.REACT_APP_API_URL).hostname,
    ORGA_FRONT_URL: process.env.REACT_APP_ORGA_FRONT_URL,
    INSCRIPTION_FRONT_URL: process.env.REACT_APP_INSCRIPTION_FRONT_URL,
    ANNOUNCEMENT_MESSAGE_FR: process.env.REACT_APP_ANNOUNCEMENT_MESSAGE_FR,
    ANNOUNCEMENT_MESSAGE_EN: process.env.REACT_APP_ANNOUNCEMENT_MESSAGE_EN,
  },

  presets: [
    [
      "classic",
      {
        docs: {
          sidebarPath: "./sidebarDocs.ts",
          editUrl: gitlabEditUrl,
          editLocalizedFiles: true,
        },
        blog: {
          showReadingTime: true,
        },
        theme: {
          customCss: "./src/css/custom.less",
        },
      } satisfies PresetClassic.Options,
    ],
  ],

  themeConfig: {
    image: "img/shared/social-card-website.gif", // Image for social cards sharing
    colorMode: {
      defaultMode: "light",
      disableSwitch: true,
      respectPrefersColorScheme: false,
    },
    navbar: {
      title: "NOÉ",
      hideOnScroll: true,
      logo: {
        alt: "NOÉ logo",
        src: "img/shared/logo-colors.svg",
        width: 30,
        style: {paddingRight: 5},
      },
      items: [
        // LEFT
        {
          label: "Documentation",
          to: "/docs",
          position: "left",
        },
        {
          label: "Centre d'aide",
          to: "/community/help-center",
          position: "left",
        },
        {
          label: "Open-roadmap",
          to: "/community/open-roadmap",
          position: "left",
        },

        // RIGHT
        {
          type: "search",
          position: "right",
          className: "search-bar",
        },
        {
          type: "custom-connectToNOEButton",
          position: "right",
        },
        {
          label: "Discord",
          href: DISCORD_INVITE_LINK,
          position: "right",
        },
        {
          label: "Faire un don",
          href: DONATE_URL,
          position: "right",
        },
        {
          type: "localeDropdown",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      logo: {
        alt: "NOÉ logo",
        src: "img/shared/logo-colors-with-white-text.svg",
        style: {paddingTop: 30, width: 200, opacity: 0.7},
      },
      links: [
        {
          title: "learn",
          items: [
            {
              label: "getting-started",
              to: "/docs",
            },
            {
              label: "developers",
              to: "/docs/developers",
            },
          ],
        },
        {
          title: "community",
          items: [
            {
              label: "Centre d'aide",
              to: "/community/help-center",
            },
            {
              label: "Open-Roadmap",
              to: "/community/open-roadmap",
            },
            {
              label: "Traduire NOÉ",
              to: "/community/translate",
            },
          ],
        },
        {
          title: "social-links",
          items: [
            {
              label: "Discord",
              href: DISCORD_INVITE_LINK,
            },
            {
              label: "Open Collective",
              href: "https://opencollective.com/noeappio",
            },
            {
              label: "Telegram",
              href: "https://t.me/noeappio",
            },
            {
              label: "Facebook",
              href: "https://facebook.com/noeappio",
            },
            {
              label: "Instagram",
              href: "https://instagram.com/noeappio",
            },
          ],
        },
        {
          title: "more info",
          items: [
            {
              label: "our team",
              href: "/team",
            },
            {
              label: "GitLab",
              href: GITLAB_URL,
            },
            {
              label: "contact us",
              href: "/community/contact",
            },
          ],
        },
      ],
    },
  } satisfies PresetClassic.ThemeConfig,

  plugins: [
    "docusaurus-plugin-less",
    [
      "content-docs",
      {
        id: "community",
        path: "community",
        routeBasePath: "community",
        sidebarPath: "./sidebarOther.ts",
      } satisfies PresetPluginContentDocs.Options,
    ],
    [
      // TODO this plugin is actually not used. See the docs to use it properly :
      "ideal-image",
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        disableInDev: false,
      },
    ],
    [
      require.resolve("docusaurus-lunr-search"),
      {
        languages: ["en", "fr"], // language codes
        highlightResult: true,
      },
    ],
  ],
};

export default config;
