import {SectionTertiary} from "../../components/design-system/Section";
import {Carousel} from "antd";
import {PartnerCard} from "../../components/PartnerCard";
import Translate from "@docusaurus/Translate";
import {partners} from "../../websiteData/partners";

export function TheyTrustUs() {
  return (
    <SectionTertiary>
      <h2 className={"text-h1"} style={{margin: "unset", textAlign: "center"}}>
        <Translate id="theyTrustUs" />
      </h2>
      <Carousel dots={{className: "dots-carousel"}} autoplay autoplaySpeed={5000}>
        {partners.map((partnerCard) => (
          <div>
            <PartnerCard {...partnerCard} key={partnerCard.name} />
          </div>
        ))}
      </Carousel>
    </SectionTertiary>
  );
}
