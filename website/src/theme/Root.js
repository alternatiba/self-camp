import {ConfigProvider} from "antd";
import {CssVarsProvider, extendTheme} from "@mui/joy/styles";
import {LIGHT_THEME} from "../../config/theme";
import {useLocation} from "@docusaurus/router";
import {AnnouncementMessageBanner} from "@site/src/components/AnnouncementMessageBanner/AnnouncementMessageBanner";

const muiTheme = extendTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 996,
      lg: 1200,
      xl: 1536,
    },
  },
  spacing: 4,
});

export default function Root({children}) {
  const location = useLocation();

  return (
    <CssVarsProvider theme={muiTheme}>
      <ConfigProvider componentSize="large" theme={LIGHT_THEME}>
        <AnnouncementMessageBanner /> {/* Store the path inside so we can access it with css */}
        <main data-route={location.pathname}>{children}</main>
      </ConfigProvider>
    </CssVarsProvider>
  );
}
