import ContactUsButton from "./ContactUsButton";
import Translate from "@docusaurus/Translate";
import Link from "@docusaurus/Link";
import {SectionSecondary} from "./design-system/Section";
import {Stack} from "@mui/joy";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";

export function ContactUsBanner() {
  const {siteConfig} = useDocusaurusContext();
  const DISCORD_INVITE_LINK = siteConfig.customFields.DISCORD_INVITE_LINK;
  return (
    <SectionSecondary maxWidth={"45rem"}>
      <Stack gap={6} alignItems={"center"}>
        <p className={"displayText"} style={{textAlign: "center"}}>
          <Translate id="homepage.contactUs" />
        </p>

        <ContactUsButton className="button--primary button--lg" />

        <Link className="button button--secondary" href={DISCORD_INVITE_LINK}>
          <Translate id="homepage.contactUs.joinCommunityOnDiscord" />
        </Link>
      </Stack>
    </SectionSecondary>
  );
}
