import {default as MuiGrid, GridProps as MuiGridProps} from "@mui/joy/Grid";

type GridProps = Omit<MuiGridProps, "gap">;
export default function Grid(props: GridProps) {
  return <MuiGrid spacing={{xs: 12, md: 14}} {...props} />;
}
