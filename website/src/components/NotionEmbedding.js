import BrowserOnly from "@docusaurus/BrowserOnly";

const DynamicNotionEmbedding = ({url, style}) => {
  const isMobile = window.innerWidth < 768;

  const leftMarginOffset = isMobile ? 0 : 20;

  return (
    <div style={{prefersColorScheme: "light", marginLeft: -leftMarginOffset, overflow: "hidden"}}>
      <iframe
        id={"help-center-iframe"}
        style={{
          marginLeft: (isMobile ? -20 : -95) + leftMarginOffset,
          marginRight: isMobile ? -20 : -95,
          marginTop: isMobile ? -70 : -130,
          marginBottom: -200,

          padding: 0,
          height: 5000,
          width: `calc(100% + ${isMobile ? 40 : 190}px)`,
          ...style,
        }}
        src={url}
      />
    </div>
  );
};

export const NotionEmbedding = ({url, style}) => {
  return <BrowserOnly>{() => <DynamicNotionEmbedding url={url} style={style} />}</BrowserOnly>;
};
