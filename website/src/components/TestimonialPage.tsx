import useBaseUrl from "@docusaurus/core/lib/client/exports/useBaseUrl";
import Link from "@docusaurus/Link";
import Translate from "@docusaurus/Translate";
import {Box, Stack} from "@mui/joy";
import {ReactNode} from "react";
import {partners} from "../websiteData/partners";
import {Apostroph} from "./Apostroph";
import {PartnerStatistics} from "./PartnerCard";
import {Signature} from "./Signature";

type TestimonialPageProps = {
  title: string;
  partnerCardId: string;
  thanksTo: string;
  signature: string;
  children: ReactNode;
};

export function TestimonialPage({
  title,
  partnerCardId,
  thanksTo,
  signature,
  children,
}: TestimonialPageProps) {
  const partner = partners.find((card) => card.id === partnerCardId);

  return (
    <Stack gap={{xs: 3, md: 5}} mb={10}>
      <h2 style={{opacity: 0.7, fontWeight: "unset", marginTop: 20, marginBottom: 0}}>
        <Translate id="testimonial" />
      </h2>
      <h1>{title}</h1>

      <Stack
        sx={{
          position: "relative",
          right: "calc(calc(calc(100vw - 100%) / 2) - 16px)",
          minWidth: "calc(100vw - 32px)",
          mb: 5,
        }}
        alignItems={"center"}>
        <img
          className={"shadow"}
          alt={partner.name}
          src={useBaseUrl("/img/testimonials/" + partner.testimonialPicture)}
          style={{
            maxWidth: "min(1200px, 100%)",
            borderRadius: 10,
          }}
        />
      </Stack>

      {partner.knowMoreLink && (
        <Link to={partner.knowMoreLink}>
          <Translate id="knowMore" /> ➜ {partner.knowMoreLink.replace("https://", "")}
        </Link>
      )}

      <PartnerStatistics statistics={partner.statistics} />

      {thanksTo && <p style={{fontStyle: "italic"}}>{thanksTo}</p>}

      <Apostroph />

      <Box>{children}</Box>

      <Signature title={signature || partner.signature} />
    </Stack>
  );
}
