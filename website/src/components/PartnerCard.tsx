import useBaseUrl from "@docusaurus/core/lib/client/exports/useBaseUrl";
import Link from "@docusaurus/Link";
import Translate from "@docusaurus/Translate";
import {Box, Stack} from "@mui/joy";
import {Statistic} from "antd";
import {CSSProperties} from "react";
import {Apostroph} from "../components/Apostroph";
import {Signature} from "../components/Signature";
import Grid from "./design-system/Grid";

type StatisticInfo = {
  title: string;
  value: string | number;
  style?: CSSProperties;
};

type PartnerStatistics = {statistics?: Array<StatisticInfo>};

export type PartnerCardProps = {
  id: string;
  picture: string;
  testimonialPicture?: string;
  name: string;
  text: string;
  signature: string;
  knowMoreLink?: string;
  testimonialPage?: string;
} & PartnerStatistics;

export const PartnerStatistics = ({statistics}: PartnerStatistics) =>
  statistics ? (
    <Stack my={6} gap={4} direction={"row"} justifyContent={"stretch"} flexWrap={"wrap"}>
      {statistics.map((stat) => (
        <Statistic {...stat} style={{...stat.style, flexGrow: 1, minWidth: 150}} />
      ))}
    </Stack>
  ) : null;

export const PartnerCard = ({
  picture,
  name,
  text,
  signature,
  knowMoreLink,
  testimonialPage,
  statistics,
}: PartnerCardProps) => {
  return (
    <Grid container spacing={8} my={4}>
      <Grid xs={12} md={6}>
        <h3 className={"text-h2"}>{name}</h3>

        <Box className={"displayText"} ml={8} mt={4} sx={{opacity: 0.8}}>
          <Apostroph />
          {text}

          <Signature title={signature} />

          <PartnerStatistics statistics={statistics} />

          <Box mt={4}>
            {testimonialPage ? (
              <Link to={`/testimonials/${testimonialPage}`}>
                ➜ <Translate id="readTestimonial" />
              </Link>
            ) : (
              knowMoreLink && (
                <Link to={knowMoreLink} target={"_blank"}>
                  <Translate id="knowMore" />
                </Link>
              )
            )}
          </Box>
        </Box>
      </Grid>
      <Grid xs={12} md={6}>
        {/*Need one px because otherwise images not shown are visble on the sides */}
        <Box m={"1px"}>
          <img
            src={useBaseUrl("/img/partners/" + picture)}
            style={{width: "100%", borderRadius: 10, height: "auto", margin: "auto"}}
          />
        </Box>
      </Grid>
    </Grid>
  );
};
