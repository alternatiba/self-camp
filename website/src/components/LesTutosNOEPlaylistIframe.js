export const LesTutosNOEPlaylistIframe = ({style}) => (
  <iframe
    style={{width: "100%", aspectRatio: "16 / 9", borderRadius: 12, borderWidth: 0, ...style}}
    className={"shadow"}
    src="https://www.youtube-nocookie.com/embed/videoseries?si=NQwCVuo7k9FXzA_Y&amp;list=PLxTDjZn9usXIewHmWius7tiWdcm6zdcAY"
    allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    title="Les tutos NOÉ playlist"
    allowFullScreen
  />
);
