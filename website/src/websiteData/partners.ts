import type {PartnerCardProps} from "../components/PartnerCard";

export const partners: Array<PartnerCardProps> = [
  {
    id: "edc",
    name: "Ethereal Decibel Festival",
    text: "Économie de temps, réduction de la charge mentale, transparence d'un modèle open-source, écoute attentive de nos besoins et engagement militant pour la protection des données : autant d'éléments qui ont rendu NOÉ indispensable pour l'organisation de notre festival !",
    picture: "ethereal-decibel-festival-2023.png",
    signature: "Thomas, responsable bénévoles.",
    testimonialPage: "ethereal-decibel-festival",
    testimonialPicture: "ethereal-decibel-festival-2023-entete.jpg",
    knowMoreLink: "https://etherealdecibel.com",
    statistics: [
      {title: "Bénévoles sur NOÉ", value: 400},
      {title: "Créneaux bénévoles", value: 126},
      {title: "Ancienneté", value: "3 ans avec NOÉ"},
    ],
  },
  {
    id: "oasis",
    name: "Festival Oasis",
    text:
      "Auparavant, la gestion des espaces et des inscriptions était un véritable casse-tête. " +
      "Avec NOÉ, nous avons pu anticiper les inscriptions pour plus de fluidité sur place le jour J.",
    picture: "cooperative-oasis.jpg",
    signature: "Fabrice, chargé information et accueil participant·es.",
    testimonialPage: "festival-oasis",
    testimonialPicture: "festival-oasis-2023-entete.jpg",
    knowMoreLink: "https://cooperative-oasis.org",
    statistics: [
      {title: "Participant·es sur NOÉ", value: 661},
      {title: "Ateliers", value: 187},
      {title: "Créneaux bénévoles", value: 39},
    ],
  },
  {
    id: "cb",
    name: "Crème Brûlée",
    text:
      "Tout simplement le meilleur outil de gestion de bénévoles que nous connaissons, maintenu par une équipe" +
      " dynamique et à notre écoute ! C'est un logiciel libre, construit en co-création, qui respecte la " +
      "confidentialité, et qui répond à notre besoin d'avoir des participants qui font aussi des créneaux bénévoles.",
    picture: "creme-brulee-2024.jpg",
    signature: "Gabriel, lead bénévoles.",
    knowMoreLink: "https://cb.frenchburners.org/",
    statistics: [
      {title: "Participant·es sur NOÉ", value: 800},
      {title: "Ancienneté", value: "3 ans avec NOÉ"},
    ],
  },
  {
    id: "decroissance",
    name: "Décroissance, le Festival",
    text:
      "NOÉ a été d'une aide précieuse sur l'inscription des bénévoles et la gestion en amont : " +
      "hébergements, arrivées, repas... Utiliser une plate-forme dédiée avec une page d'accueil " +
      "personalisable donne confiance aux futur·es bénévoles, et montre que l'on attache de " +
      "l'importance à leur bien-être.",
    picture: "decroissance-festival-2023.jpg",
    signature: "Roméo, coordinateur bénévoles.",
    knowMoreLink: "https://decroissancelefestival.org",
    statistics: [
      {title: "Bénévoles sur NOÉ", value: 250},
      {title: "Ancienneté", value: "2 ans avec NOÉ"},
    ],
  },
];
