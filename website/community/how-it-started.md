---
sidebar_label: Comment ça a commencé
sidebar_position: 5
---

# Comment le projet NOÉ s'est-il lancé ?

Le projet NOÉ a été lancé par Alternatiba en 2019. Alternatiba est une association qui milite pour la justice sociale et
climatique. À l'origine, NOÉ a été créé pour aider à organiser les Camps Climat (qui sont des rassemblements de
militant⋅es dans toute la France pour se former sur les questions de climat et de justice sociale, se rencontrer,
partager et fédérer le mouvement climat).

Puis nous avons vu que NOÉ attirait également d'autres acteurs et associations, qui avaient également besoin d'un outil
nativement horizontal pour l'organisation de leurs événements... C'est ainsi que NOÉ a commencé à être utilisé dans des
festivals, et autres séminaires.