import {createSelector, createSlice} from "@reduxjs/toolkit";
import {getLocalStorage, setLocalStorage} from "@shared/utils/localStorageUtilities";
import {DEFAULT_AGENDA_PARAMS, getAgendaDisplayStartDate} from "@utils/agendaUtilities";
import {getAllSearchScopes} from "@routes/sessions/atoms/SearchBox";

export const paginationPageSizes = [10, 20, 40, 60, 100];
export const defaultPagination = {pageSize: paginationPageSizes[2], current: 1};

export const getUrlSearchParams = () => {
  const urlSearchString = window.location.search;
  const searchParamsInfo = {};
  if (urlSearchString?.length > 0) {
    const urlSearchParams = new URLSearchParams(urlSearchString);
    searchParamsInfo.firstName = urlSearchParams.get("firstName");
    searchParamsInfo.lastName = urlSearchParams.get("lastName");
    searchParamsInfo.email = urlSearchParams.get("email");
    searchParamsInfo.ticketId = urlSearchParams.get("ticketId");
    searchParamsInfo.invitationToken = urlSearchParams.get("invitationToken");
  }
  return searchParamsInfo;
};

export const viewSlice = createSlice({
  name: "view",
  initialState: {
    darkMode: getLocalStorage("darkMode"),
    searchParams: getUrlSearchParams(),
    scroll: {},
    sorting: {},
    search: {},
    pagination: {},
    filter: {},
    isModified: {},
    columnsBlacklist: getLocalStorage("columnsBlacklist", {}),
    agendaParams: getLocalStorage("agendaParams", {}),
  },
  reducers: {
    setSearchParams: (state, action) => {
      state.searchParams = action.payload;
    },
    changeScroll: (state, action) => {
      state.scroll = {
        ...state.scroll,
        [window.location.pathname]: action.payload,
      };
    },
    changeSorting: (state, action) => {
      state.sorting = {
        ...state.sorting,
        [window.location.pathname]: action.payload,
      };
    },
    changeSearch: (state, action) => {
      state.search = {
        ...state.search,
        [window.location.pathname]: action.payload,
      };
    },
    changePagination: (state, action) => {
      state.pagination = {
        ...state.pagination,
        [window.location.pathname]: action.payload,
      };
    },
    changeFilter: (state, action) => {
      state.filter = {
        ...state.filter,
        [window.location.pathname]: action.payload,
      };
    },
    changeDarkMode: (state, action) => {
      state.darkMode = action.payload;
      setLocalStorage("darkMode", state.darkMode);
    },
    changeIsModified: (state, action) => {
      state.isModified = {
        ...state.isModified,
        [action.payload.key]: action.payload.value,
      };
    },
    changeColumnsBlacklist: (state, action) => {
      state.columnsBlacklist = {
        ...state.columnsBlacklist,
        [action.payload.key]: action.payload.blacklist,
      };
      setLocalStorage("columnsBlacklist", state.columnsBlacklist);
    },
    changeAgendaParams: (state, action) => {
      const projectId = action.payload.projectId;

      // Update the state
      const newProjectParams = {
        ...state.agendaParams[projectId],
        ...action.payload.params,
      };
      state.agendaParams = {
        ...state.agendaParams,
        [projectId]: newProjectParams,
      };

      // Exclude search bar from being saved to local storage
      const {searchBarValue, ...newProjectParamsWithoutSearch} = newProjectParams;
      setLocalStorage("agendaParams", {
        ...state.agendaParams,
        [projectId]: newProjectParamsWithoutSearch,
      });
    },
  },
});

const asyncActions = {};

export const viewSelectors = {
  selectDarkMode: (state) => state.view.darkMode,
  selectIsModified: (state, givenKey?) =>
    state.view.isModified[givenKey || window.location.pathname],
  selectScroll: (state) => state.view.scroll[window.location.pathname],
  selectSorting: (state) => state.view.sorting[window.location.pathname],
  selectSearch: (state) => state.view.search[window.location.pathname],
  selectPagination: (state) => state.view.pagination[window.location.pathname],
  selectSessionFilterPeriod: (state) => state.view.filter[window.location.pathname],
  selectColumnsBlacklist: (key) => (state) => state.view.columnsBlacklist[key],
  selectSearchParams: (state) => state.view.searchParams,

  // Memoized selector for agenda params
  selectAgendaParams: createSelector(
    [(state) => state.view.agendaParams, (state) => state.currentProject.project],
    (agendaParams, project) => {
      const currentProjectAgendaParams = agendaParams?.[project._id];
      console.log("currentProjectAgendaParams", currentProjectAgendaParams);
      return {
        ...DEFAULT_AGENDA_PARAMS,
        ...currentProjectAgendaParams,
        currentAgendaDate:
          currentProjectAgendaParams?.currentAgendaDate || getAgendaDisplayStartDate(project),
        searchBarValue: currentProjectAgendaParams?.searchBarValue || {
          text: "",
          scopes: getAllSearchScopes(project).map((scope) => scope.value),
        },
        // Only filter with custom fields that are actually defined in the project. It could happen that
        // filtering is still activated for a custom field that has been deleted in beteween.
        customFieldsFilters: (currentProjectAgendaParams?.customFieldsFilters || []).filter(
          (filter) =>
            project.customFieldsComponents.find((component) => component.key === filter.key)
        ),
      };
    }
  ),
};

export const viewReducer = viewSlice.reducer;

export const viewActions = {
  ...viewSlice.actions,
  ...asyncActions,
};
