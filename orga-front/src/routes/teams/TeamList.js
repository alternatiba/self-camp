import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {teamsActions, teamsSelectors} from "@features/teams";
import {ListPage} from "@shared/pages/ListPage";
import {TeamOutlined} from "@ant-design/icons";
import {useLoadList} from "@shared/hooks/useLoadList";
import {useTeamsColumns} from "@utils/columns/useTeamsColumns";

function TeamList() {
  const teams = useSelector(teamsSelectors.selectList);
  const dispatch = useDispatch();

  const columns = useTeamsColumns("./..");

  useLoadList(() => {
    dispatch(teamsActions.loadList());
  });

  return (
    <ListPage
      icon={<TeamOutlined />}
      i18nNs="teams"
      searchInFields={["name"]}
      elementsActions={teamsActions}
      elementsSelectors={teamsSelectors}
      columns={columns}
      dataSource={teams}
      groupEditable
      groupImportable
    />
  );
}

export default TeamList;
