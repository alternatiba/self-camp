import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "@features/activities";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {placesActions, placesSelectors} from "@features/places";
import {categoriesActions, categoriesSelectors} from "@features/categories";
import {Alert, Collapse, Form} from "antd";
import {TableElement} from "@shared/components/TableElement";
import {CardElement} from "@shared/components/CardElement";
import {EditPage, ElementEditProps} from "@shared/pages/EditPage";
import {currentProjectSelectors} from "@features/currentProject";
import {useLocation} from "react-router-dom";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {useLocalStorageState} from "@shared/utils/localStorageUtilities";
import {Trans, useTranslation} from "react-i18next";
import {
  BookOutlined,
  ClockCircleOutlined,
  QuestionCircleOutlined,
  ScheduleOutlined,
  TagOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {TextInput} from "@shared/inputs/TextInput";
import {NumberInput} from "@shared/inputs/NumberInput";
import {SwitchInput} from "@shared/inputs/SwitchInput";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {useNewElementModal} from "@shared/hooks/useNewElementModal";
import {RichTextInput} from "@shared/inputs/RichTextInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {SelectInput} from "@shared/inputs/SelectInput";
import {WithEntityLinks} from "@shared/components/WithEntityLinks";
import {TagsSelectInput} from "@shared/components/TagsSelectInput";
import {useStewardsColumns} from "@utils/columns/useStewardsColumns";
import {usePlacesColumns} from "@utils/columns/usePlacesColumns";
import {useSessionsColumns} from "@utils/columns/useSessionsColumns";
import {withFallBackOnUrlId} from "@shared/utils/withFallbackOnUrlId";
import {categoriesOptionsWithColorDot} from "../sessions/atoms/categoriesOptionsWithColorDot";
import {TableWithEditModal} from "@shared/components/TableWithEditModal";
import {ElementsTableWithModal} from "@shared/components/ElementsTableWithModal";
import {GroupEditionNavigationState} from "@shared/components/buttons/GroupEditionButton";
import SessionEdit from "../sessions/SessionEdit";
import {DurationSelectInput} from "@shared/inputs/DurationSelectInput";
import {CustomFieldsInputs} from "@shared/inputs/CustomFieldsInputs";

import {timeFormatter} from "@shared/utils/formatters";

const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../stewards/StewardEdit")
);
const CategoryEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../categories/CategoryEdit.js")
);
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../places/PlaceEdit.js"));

function ActivityEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const location = useLocation();
  const [form] = Form.useForm();
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const activity = useSelector(activitiesSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const dispatch = useDispatch();
  const [setShowNewSessionModal, NewSessionModal] = useNewElementModal(SessionEdit);

  const endpoint = "activities";
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true, endpoint, id});

  const [activitySessionsHelpCollapseKeys, setActivitySessionsHelpCollapseKeys] =
    useLocalStorageState("activitySessionsHelpCollapseKeys", false);

  const activitySessions = sessions.filter((s) => s.activity?._id === activity._id);

  const groupEditing: GroupEditionNavigationState = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    activitiesActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(placesActions.loadList());
      dispatch(categoriesActions.loadList());
      dispatch(sessionsActions.loadList());
      dispatch(activitiesActions.loadList()); // For tags
    },
    clonedElement
  );

  const sessionsColumns = useSessionsColumns("./../..", currentProject.usePlaces, true, true);
  const slotsColumns = [{dataIndex: "duration", render: timeFormatter.duration}];
  const stewardsColumns = useStewardsColumns();
  const placesColumns = usePlacesColumns();

  const activityPostTransform = (values) => {
    const categoryIndex = categories.map((d) => d._id).indexOf(values.category);
    if (categoryIndex >= 0) {
      values.category = {...categories[categoryIndex]};
    }
    return values;
  };

  return (
    <>
      <EditPage
        icon={<BookOutlined />}
        i18nNs={endpoint}
        clonable
        form={form}
        clonedElement={clonedElement}
        asModal={asModal}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        onCreate={onCreate}
        deletable
        elementsActions={activitiesActions}
        record={activity}
        initialValues={{
          ...activity,
          category: activity.category?._id,
          stewardVolunteeringCoefficient: activity.stewardVolunteeringCoefficient || 1.5,
        }}
        postTransform={activityPostTransform}
        groupEditing={groupEditing}>
        <div className="container-grid two-thirds-one-third">
          <div className="container-grid">
            <CardElement>
              <div className="container-grid two-per-row">
                <TextInput
                  i18nNs="activities"
                  name="name"
                  required
                  rules={[
                    {
                      max: 75,
                      message: t("activities:schema.name.tooLong"),
                      warningOnly: true,
                    },
                  ]}
                />

                <WithEntityLinks
                  endpoint="categories"
                  name="category"
                  createButtonText={t("categories:label", {context: "createNew"})}
                  ElementEdit={CategoryEdit}>
                  <SelectInput
                    icon={<TagOutlined />}
                    label={t("categories:label")}
                    placeholder={t("categories:label").toLowerCase()}
                    required
                    options={categoriesOptionsWithColorDot(categories)}
                    showSearch
                  />
                </WithEntityLinks>

                <TagsSelectInput
                  i18nNs="activities"
                  name="secondaryCategories"
                  elementsSelectors={activitiesSelectors}
                />

                <SwitchInput i18nNs="activities" name="allowSameTime" />
              </div>
            </CardElement>

            <CustomFieldsInputs form={form} endpoint={endpoint} />
          </div>

          <CardElement>
            <div className="container-grid">
              <NumberInput
                i18nNs="activities"
                name="volunteeringCoefficient"
                step={0.1}
                min={0}
                max={5}
              />

              <NumberInput
                i18nNs="activities"
                name="stewardVolunteeringCoefficient"
                step={0.1}
                min={0}
                max={5}
              />

              <NumberInput
                i18nNs="activities"
                name="maxNumberOfParticipants"
                tooltip={<Trans ns="activities" i18nKey="schema.maxNumberOfParticipants.tooltip" />}
                min={0}
              />
            </div>
          </CardElement>
        </div>

        <CardElement>
          <div className="container-grid two-thirds-one-third">
            <RichTextInput i18nNs="activities" name="description" />

            <TextAreaInput
              i18nNs="activities"
              name="summary"
              rules={[
                {max: 250, message: <Trans i18nKey="schema.summary.tooLong" ns="activities" />},
              ]}
            />

            <RichTextInput i18nNs="activities" name="notes" />

            <TagsSelectInput
              i18nNs="activities"
              name="tags"
              elementsSelectors={activitiesSelectors}
            />
          </div>
        </CardElement>

        <Collapse
          style={{marginBottom: 26}}
          onChange={setActivitySessionsHelpCollapseKeys}
          activeKey={activitySessionsHelpCollapseKeys}>
          <Collapse.Panel
            header={t("activities:helpForSessionsCreation.title")}
            key="sessions-help">
            <Alert
              message={
                <Trans
                  ns="activities"
                  i18nKey="helpForSessionsCreation.information"
                  components={{questionMarkIcon: <QuestionCircleOutlined />}}
                />
              }
              style={{marginBottom: 20}}
            />

            <div
              style={{marginBottom: -26}}
              className={
                "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
              }>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableWithEditModal
                  name={"slots"}
                  title={t("activities:schema.slots.label")}
                  modalTitle={t("activities:schema.slots.editModal.title")}
                  icon={<ClockCircleOutlined />}
                  buttonTitle={t("activities:schema.slots.buttonTitle")}
                  tooltip={t("activities:schema.slots.tooltip")}
                  columns={slotsColumns}>
                  <DurationSelectInput name={"duration"} required />
                </TableWithEditModal>
              </div>

              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <ElementsTableWithModal
                  name={"stewards"}
                  onChange={() => setIsModified(true)}
                  allElements={stewards}
                  title={t("activities:schema.stewards.label")}
                  icon={<UserOutlined />}
                  buttonTitle={t("activities:schema.stewards.buttonTitle")}
                  tooltip={t("activities:schema.stewards.tooltip")}
                  navigableRootPath="./../../stewards"
                  columns={stewardsColumns}
                  ElementEdit={StewardEdit}
                  elementSelectionModalProps={{
                    title: t("activities:schema.slots.editModal.stewards.label"),
                    searchInFields: ["firstName", "lastName"],
                    createNewElementButtonTitle: t(
                      "activities:schema.slots.editModal.stewards.buttonTitle"
                    ),
                  }}
                />
              </div>

              {currentProject.usePlaces && (
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <ElementsTableWithModal
                    name={"places"}
                    onChange={() => setIsModified(true)}
                    allElements={places}
                    title={t("activities:schema.places.label")}
                    icon={<UserOutlined />}
                    buttonTitle={t("activities:schema.places.buttonTitle")}
                    tooltip={t("activities:schema.places.tooltip")}
                    navigableRootPath="./../../places"
                    columns={placesColumns}
                    ElementEdit={PlaceEdit}
                    elementSelectionModalProps={{
                      title: t("activities:schema.slots.editModal.places.label"),
                      searchInFields: ["name"],
                      createNewElementButtonTitle: t(
                        "activities:schema.slots.editModal.places.buttonTitle"
                      ),
                    }}
                  />
                </div>
              )}
            </div>
          </Collapse.Panel>
        </Collapse>

        {activity._id !== "new" && (
          <TableElement.WithTitle
            title={t("activities:schema.sessions.label")}
            icon={<ScheduleOutlined />}
            showHeader
            buttonTitle={t("activities:schema.sessions.buttonTitle")}
            onClickButton={() => {
              dispatch(
                sessionsActions.changeEditing({
                  _id: "new",
                  activity,
                  places: [],
                  stewards: [],
                  slots: [],
                })
              );
              setShowNewSessionModal(true);
            }}
            navigableRootPath="./../../sessions"
            columns={sessionsColumns}
            dataSource={activitySessions}
            pagination
          />
        )}
      </EditPage>

      <NewSessionModal />
    </>
  );
}

export default withFallBackOnUrlId(ActivityEdit);
