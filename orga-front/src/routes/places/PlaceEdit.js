import React from "react";
import {useSelector} from "react-redux";
import {placesActions, placesSelectors} from "@features/places";
import {EditPage, ElementEditProps} from "@shared/pages/EditPage";
import {CardElement} from "@shared/components/CardElement";
import {EnvironmentOutlined} from "@ant-design/icons";
import {TextInput} from "@shared/inputs/TextInput";
import {NumberInput} from "@shared/inputs/NumberInput";
import {useLoadEditing} from "@shared/hooks/useLoadEditing";
import {RichTextInput} from "@shared/inputs/RichTextInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {useLocation} from "react-router-dom";
import {withFallBackOnUrlId} from "@shared/utils/withFallbackOnUrlId";
import {CustomFieldsInputs} from "@shared/inputs/CustomFieldsInputs";
import {Form} from "antd";
import {FormAvailabilitySlots} from "@shared/inputs/FormAvailabilitySlots";
import {useTranslation} from "react-i18next";

function PlaceEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const {t} = useTranslation();
  const location = useLocation();
  const place = useSelector(placesSelectors.selectEditing);

  const endpoint = "places";
  const [form] = Form.useForm();

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(placesActions, id, undefined, clonedElement);

  return (
    <EditPage
      i18nNs={endpoint}
      icon={<EnvironmentOutlined />}
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      onCreate={onCreate}
      deletable
      elementsActions={placesActions}
      record={place}
      initialValues={place}
      groupEditing={groupEditing}>
      <CardElement>
        <div className="container-grid two-per-row">
          <TextInput
            label={t("places:schema.name.label")}
            name="name"
            placeholder={t("common:schema.name.placeholder")}
            required
          />

          <NumberInput i18nNs={"places"} name="maxNumberOfParticipants" min={1} />
        </div>
      </CardElement>

      <CardElement>
        <div className="container-grid">
          <TextAreaInput i18nNs={"places"} name="summary" />

          <RichTextInput i18nNs={"common"} name="notes" />
        </div>
      </CardElement>

      <CustomFieldsInputs form={form} endpoint={endpoint} />

      <FormAvailabilitySlots
        name={"availabilitySlots"}
        label={t("places:schema.availabilitySlots.label")}
        disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
        allowImportProjectDates
      />
    </EditPage>
  );
}

export default withFallBackOnUrlId(PlaceEdit);
