import {useSelector} from "react-redux";
import {FieldsBuilder} from "@shared/components/FieldsBuilder/FieldsBuilder";
import {FieldsBuilderLivePreview} from "@shared/components/FieldsBuilder/FieldsBuilderLivePreview";
import React, {useEffect, useState} from "react";
import {projectsSelectors} from "@features/projects";
import {EditPage} from "@shared/pages/EditPage";
import {FormElement} from "@shared/inputs/FormElement";
import {customFieldCompCustomPropsConfig} from "@shared/components/FieldsBuilder/fieldCompCustomProps/fieldCompCustomPropsConfig";
import {ClosableAlert} from "@shared/components/ClosableAlert";
import {useTranslation} from "react-i18next";
import {NOEObjectSelectInput, useNOEObjectsWithLabels} from "@shared/inputs/NOEObjectSelectInput";
import {CardElement} from "@shared/components/CardElement";
import {useFieldCompsFormInstance} from "@shared/components/FieldsBuilder/atoms/useFieldCompsFormInstance";
import {Alert, Form, Tag} from "antd";
import {Stack} from "@shared/layout/Stack";
import {
  EyeOutlined,
  FilterFilled,
  FilterOutlined,
  FullscreenOutlined,
  ScheduleOutlined,
} from "@ant-design/icons";
import {BetaTag} from "@shared/components/BetaTag";
import {allowedTypesInFilters} from "@shared/hooks/useGetCustomFieldsForEndpointAndMode";
import {CheckboxGroupInput} from "@shared/inputs/CheckboxGroupInput";
import {DisplayOption, DisplayOptionsIconsList} from "./DisplayOptionsIconsList";

export const CustomFieldsBuilderTab = ({customFieldsComponentsForm}) => {
  const customFieldsComponents = useSelector(
    (state) => projectsSelectors.selectEditing(state)?.customFieldsComponents
  );

  const [selectedEndpoint, setSelectedEndpoint] = useState();
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();

  const displayOptions: Array<DisplayOption & {onlyFilterableFields?: boolean}> = [
    {
      icon: <EyeOutlined />,
      value: "inListView",
      label: t("fieldsBuilder:schema.displayOptions.options.inListView"),
    },
    {
      icon: <FilterOutlined />,
      value: "inOrgaFilters",
      label: t("fieldsBuilder:schema.displayOptions.options.inOrgaFilters"),
      onlyFilterableFields: true,
    },
    {
      icon: <FilterFilled />,
      value: "inParticipantFilters",
      label: t("fieldsBuilder:schema.displayOptions.options.inParticipantFilters"),
      onlyFilterableFields: true,
    },
    {
      icon: <ScheduleOutlined />,
      value: "inSessionsCards",
      label: t("fieldsBuilder:schema.displayOptions.options.inSessionsCards"),
    },
    {
      icon: <FullscreenOutlined />,
      value: "inParticipantSessionView",
      label: t("fieldsBuilder:schema.displayOptions.options.inParticipantSessionView"),
    },
  ];

  const filterDisplayOptionsValues = displayOptions
    .filter((o) => o.onlyFilterableFields)
    .map((o) => o.value);

  const CustomFieldCompTitleExtras = ({componentRootName}) => {
    const form = useFieldCompsFormInstance();
    const endpointVal = Form.useWatch([...componentRootName, "meta", "endpoint"], form);
    const noeObjectsWithLabels = useNOEObjectsWithLabels(true);

    return (
      <Stack row gap={2} mx={2} alignStart>
        <DisplayOptionsIconsList
          componentRootName={componentRootName}
          displayOptions={displayOptions}
        />

        {endpointVal && (
          <Tag>
            {noeObjectsWithLabels.find((noeObject) => noeObject.value === endpointVal).label}
          </Tag>
        )}
      </Stack>
    );
  };
  const CustomDisplayConfig = ({name, rootName}) => {
    const form = useFieldCompsFormInstance();
    const typeVal = Form.useWatch([...rootName, name, "type"], form);

    // If the type is not compatible for filtering, we can't use the filters. So we automatically disable them
    const typeIsNotAllowedForFiltering = typeVal && !allowedTypesInFilters.includes(typeVal);
    useEffect(() => {
      if (typeIsNotAllowedForFiltering) {
        const displayOptionsValue = form.getFieldValue([...rootName, name, "displayOptions"]);
        form.setFieldValue(
          [...rootName, name, "displayOptions"],
          displayOptionsValue?.filter((val) => !filterDisplayOptionsValues.includes(val))
        );
      }
    }, [typeIsNotAllowedForFiltering, rootName, name]);

    const disabledProps = typeIsNotAllowedForFiltering
      ? {
          disabled: true,
          title: t(
            "projects:schema.customFieldsComponents.fieldsBuilder.typeIsNotAllowedToBeFiltered"
          ),
        }
      : undefined;

    const displayOptionsForCheckboxGroup = displayOptions.map((o) => ({
      ...o,
      // Don't allow if not a filterable field type
      ...(o.onlyFilterableFields ? disabledProps : undefined),
      // Add the icon at the beginning of the option
      label: (
        <>
          {o.icon} {o.label}
        </>
      ),
    }));

    return (
      <CheckboxGroupInput
        label={t("fieldsBuilder:schema.displayOptions.label")}
        options={displayOptionsForCheckboxGroup}
        name={[name, "displayOptions"]}
      />
    );
  };

  return (
    <div className="container-grid">
      <Alert
        type={"warning"}
        icon={<BetaTag />}
        showIcon
        message={t("projects:schema.customFieldsComponents.betaWarning.message")}
        description={t("projects:schema.customFieldsComponents.betaWarning.description")}
        style={{marginBottom: 26}}
      />

      <div className="container-grid two-thirds-one-third" style={{rowGap: 40, marginBottom: 40}}>
        <FormElement
          requiredMark
          form={customFieldsComponentsForm}
          initialValues={{customFieldsComponents}}
          onValuesChange={() => setIsModified(true)}>
          <FieldsBuilder
            name={"customFieldsComponents"}
            config={{
              customPropsConfig: customFieldCompCustomPropsConfig,
              customDisplayConfig: CustomDisplayConfig,
              allowPanel: false,
              allowFreeContent: false,
              allowConditionals: false,
              FieldCompTitleExtras: CustomFieldCompTitleExtras,
              i18nOverrides: {
                addAQuestion: t(
                  "projects:schema.customFieldsComponents.fieldsBuilder.addANewCustomField"
                ),
                newFieldInputComp: t(
                  "projects:schema.customFieldsComponents.fieldsBuilder.newCustomField"
                ),
                labelLabel: t("projects:schema.customFieldsComponents.fieldsBuilder.label.label"),
                labelPlaceholder: t(
                  "projects:schema.customFieldsComponents.fieldsBuilder.label.placeholder"
                ),
                typeLabel: t("projects:schema.customFieldsComponents.fieldsBuilder.type.label"),
              },
            }}
          />
        </FormElement>

        <div>
          <ClosableAlert
            localStorageKey={"fieldsBuilderPreview"}
            message={t("projects:schema.customFieldsComponents.livePreviewNotice")}
            style={{marginBottom: 26}}
          />

          <CardElement>
            <FormElement>
              <NOEObjectSelectInput
                label={t(
                  "projects:schema.customFieldsComponents.visualizeCustomFieldsOnObjects.label"
                )}
                placeholder={t(
                  "projects:schema.customFieldsComponents.visualizeCustomFieldsOnObjects.placeholder"
                )}
                value={selectedEndpoint}
                onChange={setSelectedEndpoint}
                extended
              />
            </FormElement>
          </CardElement>

          <FieldsBuilderLivePreview
            name={"customFieldsComponents"}
            filterField={(comp) => comp.meta?.endpoint === selectedEndpoint}
            fieldsComponentsForm={customFieldsComponentsForm}
          />
        </div>
      </div>
    </div>
  );
};
