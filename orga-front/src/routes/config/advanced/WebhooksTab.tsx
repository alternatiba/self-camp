import {ReadOutlined} from "@ant-design/icons";
import {URLS} from "@app/configuration";
import {projectsSelectors} from "@features/projects";
import {CardElement} from "@shared/components/CardElement";
import {FormElement} from "@shared/inputs/FormElement";
import {NOEObjectSelectInput} from "@shared/inputs/NOEObjectSelectInput";
import {SelectInput} from "@shared/inputs/SelectInput";
import {SimpleFormList} from "@shared/inputs/SimpleFormList";
import {TextInput} from "@shared/inputs/TextInput";
import {Stack} from "@shared/layout/Stack";
import {EditPage} from "@shared/pages/EditPage";
import {Alert, Button, FormInstance} from "antd";
import React from "react";
import {Trans, useTranslation} from "react-i18next";
import {useSelector} from "react-redux";

export const WebhooksTab = ({webhooksForm}: {webhooksForm: FormInstance}) => {
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();
  const webhooks = useSelector((s) => projectsSelectors.selectEditing(s).webhooks);
  return (
    <div>
      <Alert
        message={t("projects:schema.webhooks.explanationAlert.message")}
        description={
          <>
            <div>
              <Trans i18nKey="schema.webhooks.explanationAlert.description" ns="projects" />
            </div>
            <Button
              type={"link"}
              icon={<ReadOutlined />}
              style={{padding: 0}}
              href={`${URLS.WEBSITE}/docs/api/webhooks`}>
              {t("projects:schema.webhooks.explanationAlert.readWebhooksDocumentationButton")}
            </Button>
          </>
        }
        style={{marginBottom: 26}}
      />

      <Alert
        type={"warning"}
        message={t("projects:schema.webhooks.securityAlert.message")}
        description={
          <Trans
            i18nKey="schema.webhooks.securityAlert.description"
            ns="projects"
            components={{
              linkToIpFindWebsite: (
                <a
                  href={"https://www.site24x7.com/tools/find-ip-address-of-web-site.html"}
                  target={"_blank"}
                  rel="noreferrer"
                />
              ),
            }}
            values={{apiUrl: new URL(URLS.API).hostname}}
          />
        }
        style={{marginBottom: 26}}
      />

      <CardElement>
        <FormElement
          form={webhooksForm}
          initialValues={{webhooks}}
          onValuesChange={() => setIsModified(true)}>
          <SimpleFormList
            name={"webhooks"}
            addButtonText={t("projects:schema.webhooks.addAWebhook")}
            noItemText={t("projects:schema.webhooks.noWebhookConfigured")}
            getDefaultValue={() => ({apiKey: crypto.randomUUID()})}>
            {(name) => (
              <Stack
                row
                width={"100%"}
                justifySpaceBetween
                rowGap={2}
                columnGap={4}
                sx={{
                  flexWrap: {xs: "wrap", lg: "nowrap"},
                  "> *": {flexGrow: 1, flexBasis: {xs: "100%", md: "45%"}},
                }}>
                <TextInput
                  name={[name, "url"]}
                  label={t("projects:schema.webhooks.url.label")}
                  placeholder={t("projects:schema.webhooks.url.placeholder")}
                  required
                  rules={[{type: "url"}]}
                />
                <NOEObjectSelectInput
                  name={[name, "endpoints"]}
                  label={t("projects:schema.webhooks.endpoints.label")}
                  placeholder={t("projects:schema.webhooks.endpoints.placeholder")}
                  mode={"multiple"}
                  required
                />
                <SelectInput
                  name={[name, "actions"]}
                  label={t("projects:schema.webhooks.actions.label")}
                  placeholder={t("projects:schema.webhooks.actions.placeholder")}
                  mode={"multiple"}
                  required
                  options={[
                    {
                      label: t("projects:schema.webhooks.actions.options.create"),
                      value: "create",
                    },
                    {
                      label: t("projects:schema.webhooks.actions.options.update"),
                      value: "update",
                    },
                    {
                      label: t("projects:schema.webhooks.actions.options.delete"),
                      value: "delete",
                    },
                  ]}
                />
                <TextInput
                  name={[name, "apiKey"]}
                  label={t("projects:schema.webhooks.apiKey.label")}
                  placeholder={t("projects:schema.webhooks.apiKey.placeholder")}
                />
              </Stack>
            )}
          </SimpleFormList>
        </FormElement>
      </CardElement>
    </div>
  );
};
