import {pick} from "@shared/utils/utilities";

export const pickSelectedImportExportData = (
  dataToImportExport,
  globalFieldsSelection,
  projectFieldsSelection
) => {
  // Pick the global fields form the import
  const pickedData = pick(
    dataToImportExport,
    globalFieldsSelection.map(({key}) => key)
  );

  // Pick the data in the project object as well
  if (pickedData.project) {
    const projectFieldsSelectionKeys = projectFieldsSelection.map(({key}) => key);

    pickedData.project = pick(pickedData?.project, projectFieldsSelectionKeys);
  }

  return pickedData;
};
