import {Alert, Button, Form, Popconfirm} from "antd";
import {NumberInput} from "@shared/inputs/NumberInput";
import {FormElement} from "@shared/inputs/FormElement";
import React from "react";
import {projectsActions} from "@features/projects";
import {useDispatch} from "react-redux";

export const RollbackTab = () => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  return (
    <FormElement
      form={form}
      onFinish={({rollbackPeriod}) => dispatch(projectsActions.rollback(rollbackPeriod))}>
      <Alert
        style={{marginBottom: 10}}
        message="Fonctionnalité expérimentale."
        description="Cela peut faire dysfonctionner votre projet. Le résultat n'est pas garanti."
        showIcon
        type="warning"
      />
      <Alert
        style={{marginBottom: 10}}
        message="Ici, vous pouvez récupérer des données supprimées. Il suffit de mettre jusqu'à combien de minutes en arrière vous souhaitez retourner."
        type="info"
      />
      <NumberInput
        min={0}
        name="rollbackPeriod"
        defaultValue={0}
        placeholder="minute"
        label="Nombre de minutes à revenir en arrière"
      />
      <Popconfirm
        title="Si vous ne savez pas ce que vous faites, ne le faites pas."
        onConfirm={form.submit}
        okText="Je suis conscient⋅e des risques"
        okButtonProps={{danger: true}}
        cancelText="Annuler">
        <Button type="primary" danger>
          Récupérer les éléments supprimés
        </Button>
      </Popconfirm>
    </FormElement>
  );
};
