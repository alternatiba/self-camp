import {useDispatch, useSelector} from "react-redux";
import {Form} from "antd";
import {TabsPage} from "@shared/pages/TabsPage";
import {ToolOutlined} from "@ant-design/icons";
import {projectsActions} from "@features/projects";
import {MembersConfigTab} from "./MembersConfigTab";
import {registrationsSelectors} from "@features/registrations";
import {GeneralConfigTab} from "./GeneralConfigTab";
import {TicketingConfigTab} from "./TicketingConfigTab";
import {SubscriptionsConfigTab} from "./SubscriptionsConfigTab";
import {WelcomePageConfigTab} from "./WelcomePageConfigTab";
import {PendingSuspense} from "@shared/components/Pending";
import {useUserTour} from "@shared/utils/userTourUtilities";
import configUserTour from "@utils/userTours/configUserTour";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useTranslation} from "react-i18next";
import {RegistrationFormBuilderTab} from "./RegistrationFormBuilderTab";
import {EditPage} from "@shared/pages/EditPage";
import {CustomFieldsBuilderTab} from "./CustomFieldsBuilderTab";
import {BetaTag} from "@shared/components/BetaTag";
import {useValidateAllFormsThenDo} from "./atoms/useValidateAllFormsThenDo";

const AdvancedConfig = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./AdvancedConfigTab")
);

export function ConfigEdit() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const setIsModified = EditPage.useSetIsModified();
  const isUserAdminOnThisProject = currentRegistration.role === "admin";

  useUserTour("config", configUserTour(currentRegistration));

  const [generalConfigForm] = Form.useForm();
  const [formComponentsForm] = Form.useForm();
  const [customFieldsComponentsForm] = Form.useForm();
  const [ticketingConfigForm] = Form.useForm();
  const [webhooksForm] = Form.useForm();

  const validateAllFormsThenDo = useValidateAllFormsThenDo();

  const onConfigSaveButtonClick = () => {
    validateAllFormsThenDo(
      [
        {
          form: generalConfigForm,
          errorMessage: t("projects:generalConfig.errorMessage.incorrectGeneralConfigFields"),
        },
        {
          form: formComponentsForm,
          errorMessage: t("projects:generalConfig.errorMessage.incorrectFormComponents"),
        },
        {
          form: customFieldsComponentsForm,
          errorMessage: t("projects:generalConfig.errorMessage.incorrectCustomFields"),
        },
        {
          form: ticketingConfigForm,
          errorMessage: t("projects:generalConfig.errorMessage.incorrectTicketing"),
        },
        {
          form: webhooksForm,
          errorMessage: t("projects:generalConfig.errorMessage.incorrectWebhookFields"),
        },
      ],
      () => {
        // All forms validated successfully, save the project
        dispatch(
          projectsActions.persist({
            ...formComponentsForm.getFieldsValue(),
            ...customFieldsComponentsForm.getFieldsValue(),
            ...webhooksForm.getFieldsValue(),
          })
        );
        setIsModified(false);
      }
    );
  };

  return (
    <TabsPage
      title={t("projects:eventConfiguration.title")}
      icon={<ToolOutlined />}
      fullWidth
      onValidation={onConfigSaveButtonClick}
      items={[
        {
          label: t("projects:eventConfiguration.tabs.general"),
          key: "main",
          className: "with-margins",
          children: <GeneralConfigTab generalConfigForm={generalConfigForm} />,
        },
        {
          label: t("projects:eventConfiguration.tabs.members"),
          key: "members",
          className: "with-margins",
          children: <MembersConfigTab isAdmin={isUserAdminOnThisProject} />,
        },
        {
          label: t("projects:schema.content.label"),
          key: "welcome-page",
          destroyInactiveTabPane: true, // Prevent the react-page module to trigger fake isModified updates
          children: <WelcomePageConfigTab />,
        },
        {
          label: t("projects:eventConfiguration.tabs.form"),
          key: "form",
          className: "with-margins",
          children: <RegistrationFormBuilderTab formComponentsForm={formComponentsForm} />,
        },
        {
          label: t("projects:eventConfiguration.tabs.registrations"),
          key: "registrations",
          className: "with-margins",
          children: <SubscriptionsConfigTab />,
        },
        {
          label: (
            <span>
              <BetaTag title={"beta"} />
              {t("projects:schema.customFieldsComponents.label")}
            </span>
          ),
          key: "custom-fields",
          className: "with-margins",
          children: (
            <CustomFieldsBuilderTab customFieldsComponentsForm={customFieldsComponentsForm} />
          ),
        },
        isUserAdminOnThisProject && {
          label: t("projects:eventConfiguration.tabs.ticketing"),
          key: "ticketing",
          className: "with-margins",
          children: <TicketingConfigTab ticketingConfigForm={ticketingConfigForm} />,
        },
        isUserAdminOnThisProject && {
          label: t("projects:eventConfiguration.tabs.advanced"),
          key: "advanced",
          className: "with-margins",
          children: (
            <PendingSuspense>
              <AdvancedConfig webhooksForm={webhooksForm} />
            </PendingSuspense>
          ),
        },
      ]}
    />
  );
}
