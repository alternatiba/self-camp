import React, {useState} from "react";
import {Alert, Avatar, Button, Form, Modal, Popconfirm, Tooltip} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {TableElement} from "@shared/components/TableElement";
import {FormElement, safeValidateFields} from "@shared/inputs/FormElement";
import {fieldToData} from "@shared/utils/tableUtilities";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {CardElement} from "@shared/components/CardElement";
import {MailOutlined, UserOutlined} from "@ant-design/icons";
import {personName} from "@shared/utils/utilities";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {TextInput, TextInputEmail} from "@shared/inputs/TextInput";
import {useDebounce} from "@shared/hooks/useDebounce";
import {useLoadList} from "@shared/hooks/useLoadList";
import {WaitingInvitationTag} from "@shared/components/WaitingInvitationTag";
import {fetchWithMessages} from "@shared/utils/api/fetchWithMessages";
import {SelectInput} from "@shared/inputs/SelectInput";
import {DisplayInput} from "@shared/inputs/DisplayInput";
import {ROLES_MAPPING, RoleTag} from "@shared/components/RoleTag";
import {sorter} from "@shared/utils/sorters";
import {Trans, useTranslation} from "react-i18next";
import {t} from "i18next";

export const useRegistrationEditModal = (
  registrationsEligibleToModification,
  newRegistrationTemplate = {}
) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const [showModalRegistrationMode, setShowModalRegistrationMode] = useState(); // edit | new | undefined (=closed)

  // Save the current edited registration in the Redux "editing"
  const setSelectedRegistration = (value) => dispatch(registrationsActions.setEditing(value));

  ////// MODAL ACTIONS //////

  // Called to display the modal to create/add a new registration/invitation
  const onNewRegistration = () => {
    setSelectedRegistration(newRegistrationTemplate);
    form.setFieldsValue(newRegistrationTemplate);
    setShowModalRegistrationMode("new");
  };

  // Called to display the modal to modify an existing registration/invitation
  const onEditRegistration = (record) => {
    const registrationToBeEdited = {
      ...record,
      steward: record.steward?._id,
    };
    setSelectedRegistration(registrationToBeEdited);
    form.setFieldsValue(registrationToBeEdited);
    setShowModalRegistrationMode("edit");
  };

  ////// MODAL COMPONENT //////

  const RegistrationEditModal = ({children, persistRegistration, addNewRegistrationTitle}) => {
    const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

    // The registration/invitation that is being edited
    const selectedRegistration = useSelector(registrationsSelectors.selectEditing);

    // Stuff about the current user
    const isAdmin = currentRegistration.role === "admin";
    const userIsModifyingItsOwnRegistration = selectedRegistration._id === currentRegistration._id;

    // If the user iss undefined, it means that the email given is not correct, so we haven't earched for a user yet
    const emailSearchIsCorrect = selectedRegistration?.user !== undefined;
    // If the email is correct and the user is not equl to false, then it means that a user already exists for this email
    const userAlreadyExists = emailSearchIsCorrect && selectedRegistration?.user !== false;

    // Called by the FormElement when a change occurs
    const onChangeSelectedRegistration = (changedFields, allFields) => {
      const data = fieldToData(allFields);
      setSelectedRegistration({...selectedRegistration, ...data});
    };

    // Modal actions
    const registrationModalButtons = {
      closeModal: async () => {
        await setSelectedRegistration({});
        setShowModalRegistrationMode(undefined);
        // Reset fields only at the end because otherwise it will take the selectedRegistration values again
        form.resetFields();
      },
      ok: () =>
        safeValidateFields(form, () => {
          persistRegistration(selectedRegistration);
          registrationModalButtons.closeModal();
        }),
      sendInvitationEmail: () =>
        safeValidateFields(form, () => {
          dispatch(
            registrationsActions.sendInvitationEmail(
              {
                email: selectedRegistration.searchedEmail,
                firstName: selectedRegistration.firstName,
                lastName: selectedRegistration.lastName,
              },
              {role: selectedRegistration.role, steward: selectedRegistration.steward}
            )
          );
          registrationModalButtons.closeModal();
        }),
      removeRole: () => {
        persistRegistration({...selectedRegistration, role: null});
        registrationModalButtons.closeModal();
      },
      removeEntireRegistration: () => {
        dispatch(registrationsActions.remove(selectedRegistration._id));
        registrationModalButtons.closeModal();
      },
    };

    ////// USER SEARCH BY EMAIL //////

    const debouncedOnChangeUserEmail = useDebounce(
      async (searchedEmail) => {
        const emailErrors = form.getFieldError(["searchedEmail"]);
        // If input is not a valid email, set user to "undefined"
        if (emailErrors.length > 0) {
          setSelectedRegistration({...selectedRegistration, user: undefined});
          return;
        }

        // Else, try to find if the email belongs to a user or not
        try {
          // Find a user already registered to the project
          let foundRegistration = registrationsEligibleToModification.find(
            (r) => r.user.email === searchedEmail
          );

          // If not found, then search in all users from the backend, and see if it can be edited
          let user;
          if (!foundRegistration) {
            user = await fetchWithMessages("users/" + searchedEmail, {method: "GET"}, false, false);

            foundRegistration = registrationsEligibleToModification.find(
              (r) => r.user._id === user._id
            );
          }

          // If we found an eligible user, edit it
          // (the user has an account, or at least an invitation pending in this project)
          if (foundRegistration) {
            onEditRegistration(foundRegistration);
          } else {
            // The user has an account, maybe has registration, but doesn't belong to the editable users.
            // We only set the registration in the Redux store
            setSelectedRegistration({...selectedRegistration, user});
          }
        } catch {
          // If the searched email is valid, but no user found in the whole NOÉ database
          // then initialize a new registration, with user=false as a way of telling that we haven't found a matching user.
          setSelectedRegistration({...selectedRegistration, searchedEmail, user: false});
        }
      },
      400,
      [JSON.stringify(registrationsEligibleToModification)]
    );

    const RegistrationCard = ({style}) =>
      emailSearchIsCorrect && (
        <CardElement size="small" style={style}>
          {userAlreadyExists ? (
            // If a user is found, show a pretty avatar stuff to tell "hey, I recognized this user, look !"
            <>
              <Avatar
                style={{marginRight: 10}}
                className="bg-noe-gradient"
                icon={<UserOutlined />}
              />
              {personName(selectedRegistration.user)} ({selectedRegistration.user.email}){" "}
              {selectedRegistration.invitationToken && <WaitingInvitationTag />}
            </>
          ) : (
            // If no existing user is found ==> ask for invitation email
            t("users:noUserFound")
          )}
        </CardElement>
      );

    return (
      <Modal
        title={
          showModalRegistrationMode === "new"
            ? addNewRegistrationTitle
            : userIsModifyingItsOwnRegistration
            ? t("users:edit.editOwnRights.modalTitle")
            : selectedRegistration.invitationToken
            ? t("users:edit.editUserInvitation.modalTitle", {
                email: selectedRegistration.user.email,
              })
            : t("users:edit.editUserRights.modalTitle", {
                personName: personName(selectedRegistration.user),
              })
        }
        onCancel={registrationModalButtons.closeModal}
        open={showModalRegistrationMode}
        footer={
          <>
            {/* Delete registration button + remove rights buttons. Open when:
                 - registration exists (not only an invitation)
                 - current user is admin, and the modified registration is not its own registration */}
            {isAdmin &&
              !selectedRegistration.invitationToken &&
              !userIsModifyingItsOwnRegistration &&
              showModalRegistrationMode === "edit" && [
                // Delete registration button
                <Popconfirm
                  title={
                    <Trans
                      i18nKey={"edit.deleteUserRegistration.title"}
                      ns={"users"}
                      components={{redStrong: <strong style={{color: "red"}} />}}
                    />
                  }
                  okText={t("users:edit.deleteUserRegistration.yesDeleteRegistration")}
                  okButtonProps={{danger: true}}
                  cancelText={t("common:cancel")}
                  onConfirm={registrationModalButtons.removeEntireRegistration}>
                  <Button style={{opacity: 0.5}} danger>
                    {t("users:edit.deleteUserRegistration.deleteRegistration")}
                  </Button>
                </Popconfirm>,

                // Remove rights button
                <Tooltip title={t("users:edit.editUserRights.theUserWillLooseHisRights")}>
                  <Button danger onClick={registrationModalButtons.removeRole}>
                    {t("users:edit.editUserRights.removeRights")}
                  </Button>
                </Tooltip>,
              ]}

            {/* Cancel button */}
            <Button onClick={registrationModalButtons.closeModal}>{t("common:cancel")}</Button>

            {/* Remove invitation button --> only if an invitation token is found */}
            {selectedRegistration.invitationToken && (
              <Button danger onClick={registrationModalButtons.removeEntireRegistration}>
                {t("users:edit.editUserInvitation.deleteInvitation")}
              </Button>
            )}

            {/* Validate button --> if user exists, or anytime
            when the user has not given a correct email address yet */}
            {userAlreadyExists || !emailSearchIsCorrect ? (
              <>
                {userIsModifyingItsOwnRegistration ? (
                  <Popconfirm
                    title={t("users:edit.editOwnRights.areYouSure")}
                    okText={t("users:edit.editOwnRights.yesEditOwnRights")}
                    okButtonProps={{danger: true}}
                    cancelText={t("common:cancel")}
                    onConfirm={registrationModalButtons.ok}>
                    <Button type="primary" danger>
                      {t("common:ok")}
                    </Button>
                  </Popconfirm>
                ) : (
                  <Button type="primary" onClick={registrationModalButtons.ok}>
                    {t("common:ok")}
                  </Button>
                )}
              </>
            ) : (
              <Button
                type="primary"
                className="bounce-in"
                icon={<MailOutlined />}
                onClick={registrationModalButtons.sendInvitationEmail}>
                {t("users:schema.sendInvitationByEmail")}
              </Button>
            )}
          </>
        }>
        {/*
            Beginning of modal content
        */}
        {/* Info alert at the top of the modal */}
        {!userIsModifyingItsOwnRegistration && (
          <Alert
            style={{marginBottom: 26}}
            showIcon
            icon={<MailOutlined />}
            message={
              showModalRegistrationMode === "new"
                ? t("users:edit.infoAlert.sendEmailInvitation")
                : t("users:edit.infoAlert.emailNotificationWhenRightChange")
            }
          />
        )}

        <FormElement form={form} onFieldsChange={onChangeSelectedRegistration}>
          <CardElement style={{marginBottom: 0}}>
            <div className="container-grid">
              {/* Email search input in "new" mode */}
              {showModalRegistrationMode === "new" ? (
                <>
                  <TextInputEmail
                    label={t("users:schema.email.label")}
                    name="searchedEmail"
                    required
                    onChange={(e) => debouncedOnChangeUserEmail(e.target.value?.toLowerCase())}
                    disabled={showModalRegistrationMode === "edit"}
                    readOnly={showModalRegistrationMode === "edit"}
                    placeholder={t("users:schema.email.placeholder")}
                  />
                  <RegistrationCard style={{marginTop: -22, marginBottom: 15}} />
                </>
              ) : (
                // Or if "edit" mode, just display the user
                <DisplayInput label={t("users:label")}>
                  <RegistrationCard style={{marginBottom: 0}} />
                </DisplayInput>
              )}

              {emailSearchIsCorrect && (
                <div className="fade-in container-grid">
                  {!userAlreadyExists && showModalRegistrationMode === "new" && (
                    <>
                      <TextInput
                        label={`${t("users:schema.firstName.label")} (${t("common:optional")})`}
                        name="firstName"
                        placeholder={t("users:schema.firstName.placeholder")}
                      />
                      <TextInput
                        label={`${t("users:schema.lastName.label")} (${t("common:optional")})`}
                        name="lastName"
                        placeholder={t("users:schema.lastName.placeholder")}
                      />
                    </>
                  )}

                  {children}
                </div>
              )}
            </div>
          </CardElement>
        </FormElement>
      </Modal>
    );
  };

  return [onNewRegistration, onEditRegistration, RegistrationEditModal];
};

export function MembersConfigTab({isAdmin}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const registrations = useSelector(registrationsSelectors.selectList);
  const invitedRegistrations = registrations.filter((r) => r.invitationToken);
  const stewards = useSelector(stewardsSelectors.selectList);
  const projectMembersRegistrations = registrations.filter((r) => r.role);

  const columns = [
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      render: (text, record) => record.user.firstName,
      sorter: (a, b) => sorter.text(a.user.firstName, b.user.firstName),
      searchable: true,
      searchText: (record) => record.user.firstName,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      render: (text, record) => record.user.lastName,
      sorter: (a, b) => sorter.text(a.user.lastName, b.user.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user.lastName,
    },
    {
      title: t("users:schema.email.label"),
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    {
      title: t("registrations:schema.role.label"),
      dataIndex: "role",
      sorter: (a, b) => sorter.text(a.role, b.role),
      render: (text) => <RoleTag roleValue={text} />,
    },
  ];

  const [onNewRegistration, onEditRegistration, RegistrationEditModal] = useRegistrationEditModal(
    projectMembersRegistrations
  );

  const persistRegistration = (selectedRegistration) => {
    dispatch(
      registrationsActions.persist({
        _id: registrations.find((r) => r.user._id === selectedRegistration.user._id)?._id || "new", // If no id found, it means it's a new one
        user: selectedRegistration.user._id,
        role: selectedRegistration.role,
        steward: selectedRegistration.steward,
      })
    );
  };

  ////// ASYNC LOAD //////

  useLoadList(() => {
    dispatch(registrationsActions.loadList());
    dispatch(stewardsActions.loadList());
  });

  return (
    <>
      <TableElement.WithTitle
        title={t("projects:schema.projectMembers.label")}
        buttonClassName={"userTourAddMembers"}
        showHeader
        columns={columns}
        onEdit={isAdmin && onEditRegistration}
        dataSource={projectMembersRegistrations.filter((r) => !r.invitationToken)}
        buttonTitle={isAdmin ? t("projects:schema.projectMembers.addNewMembers") : undefined}
        onClickButton={isAdmin && onNewRegistration}
        pagination
      />

      {invitedRegistrations.length > 0 && (
        <TableElement.WithTitle
          title={t("projects:schema.pendingInvitations.label")}
          showHeader
          columns={[
            ...columns,
            {
              title: t("registrations:schema.steward.label"),
              dataIndex: "steward",
              render: (text, record) =>
                personName(stewards.find((s) => s._id === record?.steward?._id)),
            },
          ]}
          onEdit={isAdmin && onEditRegistration}
          dataSource={invitedRegistrations}
          pagination
        />
      )}

      <CardElement title={t("registrations:schema.role.roleScale")} className="mt-4">
        {ROLES_MAPPING.map((role) => (
          <p key={role.value}>
            <span>
              <RoleTag roleValue={role.value} />
            </span>
            {t(`registrations:schema.role.options.${role.value}.description`)}
          </p>
        ))}
      </CardElement>

      <RegistrationEditModal
        persistRegistration={persistRegistration}
        addNewRegistrationTitle={t("registrations:schema.role.addRole")}>
        <SelectInput
          rules={[atLeastOneRequired("role", "steward")]}
          label={t("registrations:schema.role.label")}
          name="role"
          placeholder={t("registrations:schema.role.placeholder")}
          options={ROLES_MAPPING.map((role) => ({
            label: <RoleTag roleValue={role.value} />,
            ...role,
          }))}
        />

        <SelectInput
          rules={[atLeastOneRequired("role", "steward")]}
          label={t("stewards:linkASteward")}
          name="steward"
          placeholder={t("stewards:labelDown")}
          options={stewards.map((steward) => ({label: personName(steward), value: steward._id}))}
          showSearch
        />
      </RegistrationEditModal>
    </>
  );
}

const atLeastOneRequired =
  (...fields) =>
  ({getFieldValue}) => ({
    validator(_, value) {
      const atLeastOneFilled = fields.find((field) => getFieldValue(field)?.length > 0);
      if (!atLeastOneFilled) return Promise.reject(t("users:edit.atLeastOneFieldMustBeFilled"));

      return Promise.resolve();
    },
  });
