import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {placesActions, placesSelectors} from "@features/places";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {categoriesActions, categoriesSelectors} from "@features/categories";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {currentProjectSelectors} from "@features/currentProject";
import {useLoadList} from "@shared/hooks/useLoadList";
import {teamsActions} from "@features/teams";

export const useLoadAgendaData = () => {
  const dispatch = useDispatch();

  const allSlots = useSelector(sessionsSelectors.selectSlotsListForAgenda);
  const places = useSelector(placesSelectors.selectList);
  const placesLoaded = useSelector(placesSelectors.selectIsLoaded);
  const stewards = useSelector(stewardsSelectors.selectList);
  const stewardsLoaded = useSelector(stewardsSelectors.selectIsLoaded);
  const categories = useSelector(categoriesSelectors.selectList);
  const categoriesLoaded = useSelector(categoriesSelectors.selectIsLoaded);
  const registrations = useSelector(registrationsSelectors.selectList);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  useLoadList(() => {
    // Load crucial resources first (sessions + project that is blocking places and teams)
    Promise.all([dispatch(sessionsActions.loadList())]).then(() =>
      Promise.all([
        dispatch(stewardsActions.loadList()),
        dispatch(categoriesActions.loadList()),
        dispatch(registrationsActions.loadList()),
        dispatch(placesActions.loadList()),
        dispatch(teamsActions.loadList()),
      ])
    );
  });

  return {
    allSlots,
    places,
    placesLoaded,
    stewards,
    stewardsLoaded,
    categories,
    categoriesLoaded,
    registrations,
    currentProject,
  };
};
