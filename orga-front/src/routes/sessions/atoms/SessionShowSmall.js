import {Card, Tag} from "antd";
import React from "react";
import Paragraph from "antd/es/typography/Paragraph";
import {getFullSessionName, getVolunteeringCoefficient} from "@shared/utils/sessionsUtilities";
import {EnvironmentOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import {DeleteButton} from "@shared/components/buttons/DeleteButton";
import {personName} from "@shared/utils/utilities";
import {SessionFilling} from "./SessionFilling";
import {teamsSelectors} from "@features/teams";
import {useSelector} from "react-redux";
import {getSessionSubscription} from "@utils/registrationsUtilities";
import {CategoryTagWithVolunteeringMajoration} from "./CategoryTagWithVolunteeringMajoration";
import {currentProjectSelectors} from "@features/currentProject";
import {useGetCustomFieldsForEndpointAndMode} from "@shared/hooks/useGetCustomFieldsForEndpointAndMode";
import {getCustomFieldForSession} from "./getCustomFieldForSession";

import {dateFormatter} from "@shared/utils/formatters";

function SessionShowSmall({
  session,
  style,
  onClick,
  withMargins,
  registrations,
  onDelete,
  fullContent = false,
  preview,
}) {
  const sessionNameTemplate = useSelector(
    (state) => currentProjectSelectors.selectProject(state).sessionNameTemplate
  );
  const teams = useSelector(teamsSelectors.selectList);

  // TODO safe session should be outside of this component
  const safeSession = {
    ...session,
    activity: session?.activity
      ? session.activity
      : {name: "-----", category: {name: "-----", color: "#aaa"}},
  };

  const volunteeringCoefficient = getVolunteeringCoefficient(safeSession);
  const stewardsNames = safeSession.stewards?.map(personName);
  const placesNames = safeSession.places?.map((p) => p.name);
  const registrationsNames = registrations
    ?.filter((r) => getSessionSubscription(r, safeSession))
    .map((r) => personName(r.user));

  const elementsListing = (Icon, elements) =>
    elements?.length > 0 && (
      <div className="containerH" style={{marginTop: 6, alignItems: "start"}}>
        <Icon style={{marginTop: 3, marginRight: 5}} />
        <div className="containerV">
          {elements?.map((name) => (
            <div key={name}>{name}</div>
          ))}
        </div>
      </div>
    );

  const customFieldsToDisplay = useGetCustomFieldsForEndpointAndMode({
    mode: "inSessionsCards",
  });

  return (
    <div
      className="containerV"
      style={{height: "100%", flexGrow: 1, padding: withMargins && "15px 15px"}}>
      <div style={{position: "relative", zIndex: 1}}>
        <CategoryTagWithVolunteeringMajoration
          volunteeringCoefficient={volunteeringCoefficient}
          category={safeSession.activity?.category}
          invertedColors
          style={{
            position: "absolute",
            top: 4,
            right: -4,
          }}
        />
      </div>
      <Card
        title={
          <div style={{textAlign: "center", marginTop: 12, marginBottom: 12}}>
            <div
              style={{
                // Allow activity name to go on 3 lines not more
                whiteSpace: "normal",
                fontWeight: "bold",
                overflow: "hidden",
                display: "-webkit-box",
                WebkitLineClamp: "3",
                WebkitBoxOrient: "vertical",
              }}>
              {getFullSessionName(safeSession, sessionNameTemplate, teams)}
            </div>
            <CategoryTagWithVolunteeringMajoration
              category={safeSession.activity?.category}
              style={{
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                overflow: "hidden",
                maxWidth: 300,
                margin: "-5px 0 -10px 0",
                opacity: 0.85,
              }}>
              {safeSession.activity?.category.name}
            </CategoryTagWithVolunteeringMajoration>
          </div>
        }
        style={{
          transition: "opacity 0.3s ease-in-out",
          border: volunteeringCoefficient > 0 && `2px solid ${safeSession.activity.category.color}`,
          overflow: !fullContent && "hidden",
          height: "fit-content",
          ...style,
        }}
        headStyle={{
          background: safeSession.activity?.category.color,
          color: "white",
          borderRadius: 0,
        }}
        onClick={onClick}>
        <div style={{textAlign: "center", marginBottom: 15}}>
          {dateFormatter.longDateTimeRange(safeSession.start, safeSession.end)}
        </div>

        {safeSession.activity?.summary && (
          <Paragraph ellipsis={{rows: 3}} style={{color: "grey"}}>
            {safeSession.activity.summary}
          </Paragraph>
        )}

        {customFieldsToDisplay.map((customField) => {
          const customFieldValue = getCustomFieldForSession(customField, session);
          return customFieldValue ? (
            <div className="containerH buttons-container">
              <strong style={{flexGrow: 0, marginBottom: 15}}>{customField.label}:</strong>
              {customFieldValue}
            </div>
          ) : null;
        })}

        <div className="containerH buttons-container" style={{flexWrap: "nowrap"}}>
          {!safeSession.everybodyIsSubscribed && (
            <>
              <strong style={{flexGrow: 0, marginBottom: 15}}>Remplissage:</strong>
              <SessionFilling
                showLabel={false}
                computedMaxNumberOfParticipants={safeSession.computedMaxNumberOfParticipants}
                numberOfParticipants={safeSession.numberParticipants}
              />
            </>
          )}
        </div>

        <div
          className="containerH"
          style={{
            alignItems: "baseline",
            flexWrap: "wrap",
            margin: 5,
            paddingBottom: 7,
            rowGap: 8,
            justifyContent: "center",
          }}>
          {safeSession.activity?.secondaryCategories?.map((tagName, index) => (
            <Tag key={index}>{tagName}</Tag>
          ))}
        </div>

        {!preview && (
          <div
            className="containerV"
            style={{maxHeight: !fullContent && "25vh", overflowY: "auto"}}>
            {elementsListing(EnvironmentOutlined, placesNames)}
            {elementsListing(UserOutlined, stewardsNames)}
            {elementsListing(TeamOutlined, registrationsNames)}
          </div>
        )}
      </Card>

      {onDelete && (
        <div style={{textAlign: "center", marginTop: 10}}>
          <DeleteButton block onConfirm={onDelete} />
        </div>
      )}
    </div>
  );
}

export default SessionShowSmall;
