import dayjs from "@shared/services/dayjs";
import {personName} from "@shared/utils/utilities";
import {sorter} from "@shared/utils/sorters";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {fetchWithMessages} from "@shared/utils/api/fetchWithMessages";

import {dateFormatter, timeFormatter} from "@shared/utils/formatters";

/**
 * Returns a function that builds and formats session data for export in CSV format.
 */
export const useBuildSessionsExport = () => {
  const projectId = useSelector((s) => currentProjectSelectors.selectProject(s)._id);

  return async () => {
    try {
      const sessions = await fetchWithMessages(
        `projects/${projectId}/sessions/export`,
        {
          method: "GET",
        },
        undefined,
        undefined,
        true
      );

      return sessions
        .map((session) => {
          return {
            "ID Session": session._id,
            "ID Activité": session.activity._id,
            "ID Catégorie": session.activity.category._id,
            Catégorie: session.activity.category.name,
            "Couleur de catégorie": session.activity.category.color,
            "Catégories secondaires": session.activity.secondaryCategories?.join("|"),
            "Nom de l'activité": session.activity.name,
            "Nom de la session": session.name,
            Résumé: session.activity.summary,
            "Description détaillée": session.activity.description,
            "Notes orga session": session.notes,
            "Notes orga activité": session.activity.notes,
            "Tags session": session.tags?.join("|"),
            "Tags activité": session.activity.tags?.join("|"),
            "Début (heure)": timeFormatter.time(session.start),
            "Fin  (heure)": timeFormatter.time(session.end),
            "Début (jour + heure)": dateFormatter.longDateTime(session.start),
            "Fin  (jour + heure)": dateFormatter.longDateTime(session.end),
            "Début - Fin (heure)": timeFormatter.timeRange(session.start, session.end),
            "Début - Fin (jour + heure, court)": dateFormatter.longDateTimeRange(
              session.start,
              session.end,
              true
            ),
            "Début - Fin (jour + heure, long)": dateFormatter.longDateTimeRange(
              session.start,
              session.end,
              false
            ),
            "Début (Format ISO)": session.start,
            "Fin (Format ISO)": session.end,
            "Durée totale": session.slots
              ?.map((s) =>
                timeFormatter.duration(dayjs(s.end).diff(dayjs(s.start), "minute"), true)
              )
              .join(" + "),
            "Durée totale (brute)": session.slots
              ?.map((s) => dayjs(s.end).diff(dayjs(s.start), "minute"))
              .reduce((acc, duration) => acc + duration, 0),
            "Participant⋅es inscrit⋅es": session.numberParticipants,
            "Jauge max. de participant⋅es": session.computedMaxNumberOfParticipants,
            "Encadrant⋅es": session.stewards.map(personName).join("|"),
            Espaces: session.places.map((place) => place.name).join("|"),
          };
        })
        .sort((a, b) => sorter.date(a["Début (Format ISO)"], b["Début (Format ISO)"]));
    } catch (e) {
      /**/
    }
  };
};
