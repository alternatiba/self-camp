import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {useAgendaParams} from "./useAgendaParams";
import React from "react";

// Renders the date labels on the side of the agenda
export const TimeScaleLabel = React.memo(({time, ...otherProps}) => {
  const [{cellDisplayHeight}] = useAgendaParams();

  // If there is no 'time', it means it's the very first, which has no time in it.
  const style = time
    ? {height: cellDisplayHeight, lineHeight: `${cellDisplayHeight}px`}
    : {height: cellDisplayHeight / 2}; // It should be twice as small.

  // Only display time when it's o'clock
  const isOClock = time?.getMinutes() === 0;

  return (
    <DayView.TimeScaleLabel
      className="timescale-cell" // Needed to get a hook
      time={isOClock ? time : undefined}
      style={style}
      {...otherProps}
    />
  );
});
