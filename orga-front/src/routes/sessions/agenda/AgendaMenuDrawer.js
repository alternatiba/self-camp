import React, {useState} from "react";
import {useAgendaParams} from "./useAgendaParams";
import {Button, Drawer, Slider} from "antd";
import {MoreOutlined, RollbackOutlined} from "@ant-design/icons";
import {t} from "i18next";
import {FormElement} from "@shared/inputs/FormElement";
import {CELLS_DISPLAY_HEIGHTS} from "@utils/agendaUtilities";
import {SelectInput} from "@shared/inputs/SelectInput";
import {useDebounce} from "@shared/hooks/useDebounce";
import {DisplayInput} from "@shared/inputs/DisplayInput";
import {ExportAgendaViewButton} from "@shared/components/buttons/ExportAgendaViewButton";
import {useAgendaDisplayHours} from "./useAgendaDisplayHours";
import {CardElement} from "@shared/components/CardElement";

import {timeFormatter} from "@shared/utils/formatters";

export const AgendaMenuDrawer = ({children, buttonStyle}) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [{cellDisplayHeight, slotsOnEachOther, forcedDisplayHours}, setAgendaParams] =
    useAgendaParams();

  const agendaDisplayHours = useAgendaDisplayHours(forcedDisplayHours);
  const debouncedSetDisplayHours = useDebounce(
    ([start, end]) => setAgendaParams({forcedDisplayHours: {start, end}}),
    200
  );

  return (
    <>
      <ExportAgendaViewButton
        small
        extraMenus={[
          {type: "divider"},
          {
            label: (
              <Button onClick={() => setDrawerOpen(true)} block>
                {t("sessions:agenda.menuDrawer.exportAgendaView.moreOptions")}
              </Button>
            ),
          },
        ]}
      />

      <Button
        style={buttonStyle}
        icon={<MoreOutlined style={{transform: "rotate(90deg)"}} />}
        type="link"
        shape="circle"
        onClick={() => setDrawerOpen(true)}
      />

      <Drawer
        title={t("common:settings")}
        placement="right"
        onClose={() => setDrawerOpen(false)}
        open={drawerOpen}>
        <FormElement className="container-grid">
          <ExportAgendaViewButton style={{marginBottom: 20}} />

          <CardElement>
            {children}

            <DisplayInput
              label={t("sessions:agenda.menuDrawer.agendaDisplayHours.label")}
              formItemProps={{style: {marginBottom: 36}}}>
              <div
                className={"containerH buttons-container"}
                style={{flexWrap: "nowrap", overflow: "visible"}}>
                <Slider
                  style={{width: "100%"}}
                  range
                  tooltip={{formatter: (val) => timeFormatter.duration(val * 60, {short: true})}}
                  max={24}
                  marks={{
                    0: "0h",
                    4: "4h",
                    8: "8h",
                    12: "12h",
                    16: "16h",
                    20: "20h",
                    24: "24h",
                  }}
                  value={[agendaDisplayHours?.start, agendaDisplayHours?.end]}
                  onChange={debouncedSetDisplayHours}
                />
                <Button
                  icon={<RollbackOutlined />}
                  disabled={!forcedDisplayHours}
                  danger
                  block={false}
                  type={"link"}
                  title={"Rétablir les heures automatiques"}
                  onClick={() => setAgendaParams({forcedDisplayHours: undefined})}></Button>
              </div>
            </DisplayInput>

            {/*Increase or decrease the cells heights*/}
            <SelectInput
              label={t("sessions:agenda.menuDrawer.cellDisplayHeight.label")}
              onChange={(value) => setAgendaParams({cellDisplayHeight: value})}
              defaultValue={cellDisplayHeight}
              options={[
                {
                  value: CELLS_DISPLAY_HEIGHTS[0],
                  label: t("sessions:agenda.menuDrawer.cellDisplayHeight.options.compact"),
                },
                {
                  value: CELLS_DISPLAY_HEIGHTS[1],
                  label: t("sessions:agenda.menuDrawer.cellDisplayHeight.options.comfort"),
                },
                {
                  value: CELLS_DISPLAY_HEIGHTS[2],
                  label: t("sessions:agenda.menuDrawer.cellDisplayHeight.options.large"),
                },
              ]}
            />

            {/*Select if we want to see events one on the other or one next to each other. */}
            <SelectInput
              label={t("sessions:agenda.menuDrawer.slotsOnEachOther.label")}
              onChange={(value) => setAgendaParams({slotsOnEachOther: value})}
              defaultValue={slotsOnEachOther}
              options={[
                {
                  value: false,
                  label: t("sessions:agenda.menuDrawer.slotsOnEachOther.options.false"),
                },
                {
                  value: true,
                  label: t("sessions:agenda.menuDrawer.slotsOnEachOther.options.true"),
                },
              ]}
            />
          </CardElement>
        </FormElement>
      </Drawer>
    </>
  );
};
