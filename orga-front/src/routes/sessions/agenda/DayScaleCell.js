import {useAgendaParams} from "./useAgendaParams";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import {dayjsBase} from "@shared/services/dayjs";
import React from "react";

export const DayScaleCell = (props) => {
  const [, setAgendaParams] = useAgendaParams();

  return (
    <DayView.DayScaleCell
      style={{cursor: "pointer"}}
      onDoubleClick={() =>
        setAgendaParams({
          currentAgendaDate: props.startDate.toISOString(),
          numberOfDaysDisplayed: 1,
        })
      }
      {...props}
      // We use dayjsBase because we are in the agenda component with timezones set to the user's timezone
      formatDate={(date, {weekday}) => dayjsBase(date).format(weekday ? "dddd" : "D")}
    />
  );
};
