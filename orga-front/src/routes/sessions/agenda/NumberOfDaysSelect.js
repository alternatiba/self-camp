import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {useAgendaParams} from "./useAgendaParams";
import {Button, Dropdown} from "antd";
import dayjs from "@shared/services/dayjs";
import {DownOutlined} from "@ant-design/icons";
import React from "react";

export const NumberOfDaysSelect = ({isMobileView = false}) => {
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [{numberOfDaysDisplayed}, setAgendaParams] = useAgendaParams();

  return (
    <Dropdown
      trigger={["click"]}
      menu={{
        onClick: (item) => setAgendaParams({numberOfDaysDisplayed: item.key}),
        items: [
          {
            label: t("sessions:agenda.numberOfDaysSelector.allTheEvent"),
            key:
              dayjs(currentProject.end)
                .startOf("day")
                .diff(dayjs(currentProject.start).startOf("day"), "day") + 1,
          },
          {type: "divider"},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 1}), key: 1},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 3}), key: 3},
          {label: t("sessions:agenda.numberOfDaysSelector.days", {count: 5}), key: 5},
          {type: "divider"},
          {
            label: "Autre",
            children: [...Array(14).keys()].map((i) => ({
              label: t("sessions:agenda.numberOfDaysSelector.days", {count: i + 2}),
              key: i + 2,
            })),
          },
        ],
      }}>
      <Button>
        {isMobileView
          ? t("sessions:agenda.numberOfDaysSelector.daysMobile", {
              count: parseInt(numberOfDaysDisplayed),
            })
          : t("sessions:agenda.numberOfDaysSelector.days", {
              count: parseInt(numberOfDaysDisplayed),
            })}
        {!isMobileView && <DownOutlined style={{marginLeft: 6}} />}
      </Button>
    </Dropdown>
  );
};
