import {NamePath} from "rc-field-form/es/interface";
import {CardElement, CardElementProps} from "@shared/components/CardElement";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Form} from "antd";
import {Stack} from "@shared/layout/Stack";
import React from "react";

import {dateFormatter} from "@shared/utils/formatters";

export const RecapSlot = ({
  name,
  row,
  ...cardProps
}: {name: NamePath, row?: boolean} & CardElementProps) => {
  const form = useFormInstance();
  const slotVal = Form.useWatch(name, form);
  return (
    <CardElement greyedOut {...cardProps}>
      {slotVal?.start && slotVal?.end ? (
        <Stack row={row} gap={3} wrap>
          <div style={{color: "grey", flexShrink: 0}}>Récap :</div>
          {dateFormatter.longDateTimeRange(slotVal.start, slotVal.end, {short: true})}
        </Stack>
      ) : (
        <div style={{color: "grey"}}>Veuillez renseigner une date de début et une durée.</div>
      )}
    </CardElement>
  );
};
