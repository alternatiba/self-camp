import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Collapse, Form} from "antd";
import {CardElement} from "@shared/components/CardElement";
import {Stack} from "@shared/layout/Stack";
import {SwitchInputInline} from "@shared/inputs/SwitchInput";
import {StewardsTableWithModal} from "./StewardsTableWithModal";
import {PlacesTableWithModal} from "./PlacesTableWithModal";
import {TableWithEditModal} from "@shared/components/TableWithEditModal";
import {ClockCircleOutlined} from "@ant-design/icons";
import React, {useRef} from "react";
import {RecapSlot} from "./RecapSlot";
import {SlotStartAndDurationInput} from "./SlotStartAndDurationInput";
import {activitiesSelectors} from "@features/activities";
import {hasSameStartAndEnd, slotOverlapsAnOtherOne} from "@utils/slotsUtilities";
import {useTranslation} from "react-i18next";
import {FormHelperText} from "@mui/material";
import {ClosableAlert} from "@shared/components/ClosableAlert";

import {dateFormatter, timeFormatter} from "@shared/utils/formatters";

const columnsSlots = [
  {
    dataIndex: "start",
    render: (text, record, index) =>
      dateFormatter.longDateTimeRange(record.start, record.end, {short: true}),
  },
  {
    dataIndex: "duration",
    render: timeFormatter.duration,
  },
];

export const AdvancedSlots = () => {
  const form = useFormInstance();

  const ModalContent = () => {
    const slotForm = Form.useFormInstance();
    const sessionVal = Form.useWatch(undefined, form);
    const slotVal = Form.useWatch(undefined, slotForm);
    const {t} = useTranslation();

    const oldSlot = useRef();
    if (!oldSlot.current && slotVal) {
      oldSlot.current = form
        .getFieldValue("slots")
        .find((slot) => hasSameStartAndEnd(slot, slotVal));
    }

    const slotOverlapError =
      slotVal?.start &&
      slotVal?.duration &&
      slotOverlapsAnOtherOne(slotVal, sessionVal?.slots, oldSlot.current);

    return (
      <div className="container-grid">
        <CardElement>
          <Stack row columnGap={5} rowGap={2} wrap>
            <SlotStartAndDurationInput name={[]} />
          </Stack>
          {slotOverlapError && (
            <FormHelperText error>
              {t("common:availability.errors.slotOverlapsAnotherOne")}
            </FormHelperText>
          )}
        </CardElement>

        <RecapSlot name={[]} row style={{marginBottom: 0}} size={"small"} />
      </div>
    );
  };

  return (
    <TableWithEditModal
      name={"slots"}
      title="Plages horaires"
      subtitle={
        <ClosableAlert
          localStorageKey={"warningMultipleSlotsSessions"}
          type={"info"}
          message={"Vous voulez peut-être créer plusieurs sessions ?"}
          description={
            <>
              <p>
                Une plage horaire est un temps pendant lequel se déroule la session. Souvent, il n'y
                en a qu'un seul, mais parfois il peut y en avoir plusieurs (par ex. une journée de
                journée de formation qui se déroule en deux temps le matin et l'après-midi).
              </p>
              Un·e participant·e qui s'inscrit à une session est forcément inscrit·e à toutes les
              plages horaires de la session (dans notre exemple, on s'inscrit donc d'un coup à la
              journée de formation entière, car c'est une seule session).
            </>
          }
        />
      }
      modalTitle="Éditer une plage horaire"
      icon={<ClockCircleOutlined />}
      buttonTitle="Ajouter une plage horaire"
      modalProps={{
        zIndex: 100, // cause otherwise the ElementsSelectionModals are behind it
      }}
      columns={columnsSlots}>
      <ModalContent />
    </TableWithEditModal>
  );
};
