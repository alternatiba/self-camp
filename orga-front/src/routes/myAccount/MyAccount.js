import MyAccountModal from "@shared/pages/MyAccountModal";
import {Alert, Collapse} from "antd";
import {Trans, useTranslation} from "react-i18next";
import {EyeInvisibleOutlined} from "@ant-design/icons";
import {DEFAULT_HEADERS} from "@shared/utils/api/fetchWithMessages";
import {TextInputPassword} from "@shared/inputs/TextInput";

const {Panel} = Collapse;

export default function MyAccount({openState}) {
  const {t} = useTranslation();

  return (
    <MyAccountModal
      openState={openState}
      extra={
        // API Token information
        <Collapse style={{marginTop: 26}}>
          <Panel header={t("users:edit.apiToken.title")}>
            <Trans
              i18nKey="edit.apiToken.body"
              ns="users"
              components={{icon: <EyeInvisibleOutlined />}}
              values={{
                requestCode: JSON.stringify(
                  {...DEFAULT_HEADERS, Authorization: `JWT xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`},
                  null,
                  2
                ),
              }}
            />

            <Alert
              style={{marginBottom: 15}}
              type="warning"
              showIcon
              message={t("users:edit.apiToken.privacyAlert.message")}
              description={t("users:edit.apiToken.privacyAlert.description")}
            />
            <TextInputPassword
              label={t("users:edit.apiToken.label")}
              value={localStorage.getItem("token")}
            />
          </Panel>
        </Collapse>
      }
    />
  );
}
