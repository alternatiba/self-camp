import {useLayoutEffect} from "react";
import {useSelector} from "react-redux";
import {CalendarOutlined, PlaySquareOutlined} from "@ant-design/icons";
import {currentUserSelectors} from "@features/currentUser";
import {LayoutStructure} from "@shared/layout/LayoutStructure";
import {useRedirectToProjectIfOffline} from "@shared/utils/offlineModeUtilities";
import {instanceName, URLS} from "@app/configuration";
import {useTranslation} from "react-i18next";
import {MenuLayout} from "@shared/components/Menu";
import {useBrowserTabTitle} from "@shared/hooks/useBrowserTabTitle";
import {Outlet} from "react-router";
import {setAppTheme} from "@shared/layout/DynamicProjectThemeProvider";

import {useNOEInstanceName} from "@shared/hooks/useNOEInstanceName";

export default function MainLayout({page}) {
  const {t} = useTranslation();

  const currentUser = useSelector(currentUserSelectors.selectUser);

  useRedirectToProjectIfOffline();

  // ****** TAB TITLE ******

  useBrowserTabTitle(
    page,
    {projects: t("projects:labelMyProjects")},
    {
      suffix: useNOEInstanceName("orgaFront"),
    }
  );

  // ***** RESET STUFF ******

  // On Main layout, reset app theme
  useLayoutEffect(() => {
    setAppTheme(undefined);
  }, []);
  // ****** SIDE MENU ******

  const menu = {
    top: (
      <MenuLayout
        rootNavUrl={"/"}
        selectedItem={page}
        items={[
          // User events
          {
            label: t("projects:labelMyProjects"),
            key: "projects",
            icon: <CalendarOutlined />,
          },
        ]}
      />
    ),
    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Inscription front
          {
            label: t("common:pagesNavigation.inscriptionFront"),
            url: `${URLS.INSCRIPTION_FRONT}/projects?no-redir`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure
      title={instanceName}
      ribbon={t("common:orga")}
      menu={menu}
      profileUser={currentUser}>
      <Outlet />
    </LayoutStructure>
  );
}
