import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {ConfigEdit} from "./config/ConfigEdit.js";
import {
  BookOutlined,
  CalendarOutlined,
  DashboardOutlined,
  DoubleLeftOutlined,
  EnvironmentOutlined,
  PlaySquareOutlined,
  ScheduleOutlined,
  TagOutlined,
  TeamOutlined,
  ToolOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {Route, Routes, useNavigate, useParams} from "react-router-dom";
import {currentUserSelectors} from "@features/currentUser";
import {currentProjectActions, currentProjectSelectors} from "@features/currentProject";
import {LayoutStructure} from "@shared/layout/LayoutStructure";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {
  DISPLAY_OFFLINE_MODE_FEATURE,
  OFFLINE_MODE,
  OnlineOfflineSwitch,
  useOfflineMode,
} from "@shared/utils/offlineModeUtilities";
import {URLS} from "@app/configuration";
import {MenuLayout} from "@shared/components/Menu";
import {PendingSuspense} from "@shared/components/Pending";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {WebsocketConcurrentEditingWorker} from "@utils/api/WebsocketConcurrentEditingWorker";
import {useBrowserTabTitle} from "@shared/hooks/useBrowserTabTitle";
import {Redirect} from "@shared/pages/Redirect";
import {DynamicProjectThemeProvider} from "@shared/layout/DynamicProjectThemeProvider";
import {useTranslation} from "react-i18next";
import {useNOEInstanceName} from "@shared/hooks/useNOEInstanceName";
import {useProjectStatusNotification} from "@shared/hooks/useProjectStatusNotification";

const Cockpit = lazyWithRetry(() => import(/* webpackPreload: true */ "./cockpit/Cockpit.js"));

const ActivityList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./activities/ActivityList.js")
);
const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./stewards/StewardEdit.js")
);
const RegistrationEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./participants/RegistrationEdit.js")
);
const SessionList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionList.js")
);
const SessionEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionEdit.js")
);
const SessionAgenda = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./sessions/SessionAgenda.js")
);
const ParticipantList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./participants/ParticipantList.js")
);
const ActivityEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./activities/ActivityEdit.js")
);
const StewardList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./stewards/StewardList.js")
);
const PlaceList = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./places/PlaceList.js"));
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./places/PlaceEdit.js"));
const CategoryList = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./categories/CategoryList.js")
);
const CategoryEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./categories/CategoryEdit.js")
);
const TeamEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./teams/TeamEdit"));
const TeamList = lazyWithRetry(() => import(/* webpackPrefetch: true */ "./teams/TeamList"));

export default function ProjectLayout() {
  const {t} = useTranslation();
  const {projectId, "*": page} = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // ****** SELECTORS & STATES ******

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  // ****** DATA FETCHING ******

  // Load project and project registration
  useEffect(() => {
    if (currentUser._id) {
      dispatch(currentProjectActions.load(projectId)).catch(() => navigate("/projects"));
    }
  }, [currentUser, projectId]);
  useEffect(() => {
    //(on orga front, the registration is independent from the project)
    dispatch(registrationsActions.loadCurrent(projectId));
  }, [projectId]);

  useOfflineMode(currentRegistration?._id);

  useProjectStatusNotification();

  // ****** TAB TITLE ******

  useBrowserTabTitle(
    page,
    {
      cockpit: t("cockpit:label"),
      config: t("projects:eventConfiguration.titleShort"),
      participants: t("registrations:label_other"),
      categories: t("categories:label_other"),
      places: t("places:label_other"),
      stewards: t("stewards:label_other"),
      activities: t("activities:label_other"),
      teams: t("teams:label_other"),
      sessions: t("sessions:label_other"),
      "sessions/agenda": t("sessions:label_other"),
    },
    {
      baseName: currentProject.name,
      suffix: useNOEInstanceName("orgaFront"),
    }
  );

  // ****** DATA DEFINITION ******

  const projectAndUserValid = currentUser._id && currentProject?._id && currentRegistration?._id;

  // If there is a loaded registration, but no role, then the user doesn't have the right to access the project
  if (projectAndUserValid && !currentRegistration?.role) {
    navigate("/projects");
    return;
  }

  // ****** SIDE MENU ******

  const collapsedSidebar = page?.includes("agenda");

  const menu = {
    top: (
      <>
        <MenuLayout
          rootNavUrl={`/${projectId}/`}
          selectedItem={page}
          items={[
            {
              label: t("cockpit:label"),
              key: "cockpit",
              icon: <DashboardOutlined />,
            },
            {
              label: t("projects:eventConfiguration.titleShort"),
              key: "config",
              icon: <ToolOutlined />,
            },
            {
              label: t("registrations:label_other"),
              key: "participants",
              icon: <TeamOutlined />,
            },
          ]}
        />

        <MenuLayout
          rootNavUrl={`/${projectId}/`}
          selectedItem={page}
          divider
          items={[
            {
              label: t("categories:label_other"),
              key: "categories",
              icon: <TagOutlined />,
            },
            currentProject.usePlaces && {
              label: t("places:label_other"),
              key: "places",
              icon: <EnvironmentOutlined />,
            },
            {
              label: t("stewards:label_other"),
              key: "stewards",
              icon: <UserOutlined />,
            },
            {
              label: t("activities:label_other"),
              key: "activities",
              icon: <BookOutlined />,
            },
            currentProject.useTeams && {
              label: t("teams:label_other"),
              key: "teams",
              icon: <TeamOutlined />,
            },
          ]}
        />

        <MenuLayout
          rootNavUrl={`/${projectId}/`}
          divider
          selectedItem={page}
          items={[
            {
              label: t("sessions:label_other"),
              key: "sessions",
              icon: <ScheduleOutlined />,
            },
            {
              label: t("sessions:labelAgenda"),
              key: "sessions/agenda",
              icon: <CalendarOutlined />,
            },
          ]}
        />
      </>
    ),

    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Inscription front
          {
            label: t("common:pagesNavigation.inscriptionFront"),
            url: `${URLS.INSCRIPTION_FRONT}/${projectId}/${page}`,
            icon: <PlaySquareOutlined />,
          },

          // Back to events page
          {
            label: t("projects:labelMyProjects"),
            onClick: () => navigate({pathname: "/projects", search: "?no-redir"}),
            icon: <DoubleLeftOutlined />,
            disabled: OFFLINE_MODE,
          },

          // Offline mode
          DISPLAY_OFFLINE_MODE_FEATURE &&
            !collapsedSidebar && {
              label: <OnlineOfflineSwitch />,
              url: false,
            },
        ]}
      />
    ),
  };

  return (
    <DynamicProjectThemeProvider>
      <WebsocketConcurrentEditingWorker />
      <LayoutStructure
        title={currentProject.name}
        ribbon="ORGA"
        menu={menu}
        profileUser={currentUser}
        showSocialIcons={false}
        collapsedSidebar={collapsedSidebar}>
        {projectAndUserValid && (
          <PendingSuspense minHeight={"100vh"}>
            <Routes style={{height: "100%"}}>
              {/* Redirect when switching from one frontend to another*/}
              <Route path="/sessions/subscribed" element={<Redirect to="./.." />} />
              <Route path="/sessions/all" element={<Redirect to="./.." />} />
              <Route path="/sessions/all/agenda" element={<Redirect to="./../../agenda" />} />
              <Route
                path="/sessions/subscribed/agenda"
                element={<Redirect to="./../../agenda" />}
              />

              <Route path="/cockpit" element={<Cockpit />} />
              <Route path="/config" element={<ConfigEdit />} />
              <Route path="/activities" element={<ActivityList />} />
              <Route path="/activities/:id" element={<ActivityEdit />} />
              <Route path="/stewards" element={<StewardList />} />
              <Route path="/stewards/:id" element={<StewardEdit />} />
              {currentProject.usePlaces && <Route path="/places" element={<PlaceList />} />}
              {currentProject.usePlaces && <Route path="/places/:id" element={<PlaceEdit />} />}
              <Route path="/categories" element={<CategoryList />} />
              <Route path="/categories/:id" element={<CategoryEdit />} />
              <Route path="/participants" element={<ParticipantList />} />
              <Route path="/participants/:id" element={<RegistrationEdit />} />
              <Route path="/sessions" element={<SessionList />} />
              <Route path="/sessions/:id" element={<SessionEdit />} />
              <Route path="/sessions/agenda" element={<SessionAgenda />} />
              {currentProject.useTeams && <Route path="/teams" element={<TeamList />} />}
              {currentProject.useTeams && <Route path="/teams/:id" element={<TeamEdit />} />}

              <Route path="/*" element={<Redirect to="./cockpit" />} />
            </Routes>
          </PendingSuspense>
        )}
      </LayoutStructure>
    </DynamicProjectThemeProvider>
  );
}
