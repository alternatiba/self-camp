import React, {Suspense, useState} from "react";
import {DashboardOutlined} from "@ant-design/icons";
import dayjs from "@shared/services/dayjs";
import {useTranslation} from "react-i18next";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {TabsPage} from "@shared/pages/TabsPage";
import {PendingSuspense} from "@shared/components/Pending";

const GlobalDashboard = lazyWithRetry(() => import(/* webpackPreload: true */ "./GlobalDashboard"));

const ParticipantDashboard = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./ParticipantDashboard/ParticipantDashboard")
);
const CockpitConfigDrawer = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./CockpitConfigDrawer")
);
const ShareEventButton = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./atoms/ShareEventButton")
);

/**
 * Main Cockpit component containing multiple tabs for the dashboard.
 * @returns {ReactNode} - JSX for Cockpit component.
 */
export default function Cockpit() {
  const {t} = useTranslation();
  const [activeTab, setActiveTab] = useState();
  const currentHour = dayjs().hour();

  return (
    <TabsPage
      title={
        (currentHour > 19 || currentHour < 4 ? t("cockpit:goodEvening") : t("cockpit:hello")) +
        " " +
        t("cockpit:welcomeInTheCockpit")
      }
      customButtons={
        <Suspense fallback={null}>
          <ShareEventButton />
        </Suspense>
      }
      icon={<DashboardOutlined />}
      activeKey={activeTab}
      onTabClick={setActiveTab}
      fullWidth
      tabBarExtraContent={
        <Suspense fallback={null}>
          <CockpitConfigDrawer />
        </Suspense>
      }
      items={[
        {
          label: t("cockpit:dashboard.dashboardTabLabel"),
          key: "dashboard",
          className: "with-margins",
          children: (
            <PendingSuspense>
              <GlobalDashboard setActiveTab={setActiveTab} />
            </PendingSuspense>
          ),
        },
        {
          label: t("cockpit:dashboard.participantsDataTabLabel"),
          key: "attendance-data",
          className: "with-margins",
          children: (
            <Suspense fallback={null}>
              <ParticipantDashboard />
            </Suspense>
          ),
        },
      ]}
    />
  );
}
