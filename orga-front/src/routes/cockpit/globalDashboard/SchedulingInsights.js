import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {useTranslation} from "react-i18next";
import {InsightCard} from "../atoms/InsightCard";
import {sessionsActions, sessionsSelectors} from "@features/sessions";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {activitiesActions, activitiesSelectors} from "@features/activities";
import {PendingSuspense} from "@shared/components/Pending";
import {categoriesActions, categoriesSelectors} from "@features/categories";
import {placesActions, placesSelectors} from "@features/places";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import {useLoadList} from "@shared/hooks/useLoadList";
import {projectsSelectors} from "@features/projects";

const PieChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/PieChart")
);

export const SchedulingInsights = React.memo(
  () => {
    const {t} = useTranslation();

    const dispatch = useDispatch();

    useLoadList(() => {
      Promise.all([
        dispatch(sessionsActions.loadList()),
        dispatch(categoriesActions.loadList()),
        dispatch(activitiesActions.loadList()),
        dispatch(stewardsActions.loadList()),
        dispatch(placesActions.loadList()),
        dispatch(registrationsActions.loadList()),
      ]);
    });

    const sessions = useSelector(sessionsSelectors.selectList);
    const activities = useSelector(activitiesSelectors.selectList);
    const categories = useSelector(categoriesSelectors.selectList);
    const places = useSelector(placesSelectors.selectList);
    const stewards = useSelector(stewardsSelectors.selectList);
    const registrations = useSelector(registrationsSelectors.selectListWithMetadata);

    const schedulingInsights = [
      {
        name: t("categories:label_other"),
        isPlot: true,
        elements: categories,
        calculateOnEach: (el) => {
          const numberOfSessions = sessions.filter(
            (session) => session.activity.category._id === el._id
          ).length;
          const numberOfSessionsPercentage = numberOfSessions / sessions.length;
          return {...el, numberOfSessions, numberOfSessionsPercentage};
        },
        calculateOnAll: (elements) => {
          const sorted = elements.sort((a, b) =>
            a.numberOfSessions > b.numberOfSessions ? -1 : 1
          );
          return [
            ...sorted.slice(0, 6),
            {
              name: t("cockpit:schedulingAndSupervision.other"),
              numberOfSessions: sorted.slice(6).reduce((acc, el) => acc + el.numberOfSessions, 0),
              color: "#555555",
            },
          ];
        },
        noAvailableData: categories.length === 0,
        formatter: (data) => (
          <PendingSuspense>
            <PieChart
              config={{
                data,
                radius: 0.85,
                height: 200,
                angleField: "numberOfSessions",
                colorField: "name",
                color: (el) => {
                  return data.find((category) => category.name === el.name).color;
                },
              }}
            />
          </PendingSuspense>
        ),
      },
      {
        name: t("activities:label_other"),
        endpoint: "activities",
        elements: activities,
        statType: "length",
        precisions: [
          {
            name: t("cockpit:schedulingAndSupervision.withAtLeastOneSessionCreated"),
            elements: activities,
            filter: (el) => sessions.find((session) => session.activity._id === el._id),
            statType: "length",
          },
        ],
      },
      {
        name: t("stewards:label_other"),
        endpoint: "stewards",
        elements: stewards,
        statType: "length",
        precisions: [
          {
            name: t("cockpit:schedulingAndSupervision.linkedToAParticipant"),
            elements: stewards,
            filter: (el) =>
              registrations.find((registration) => registration.steward?._id === el._id),
            statType: "length",
          },
        ],
      },
      {
        name: t("sessions:label_other"),
        endpoint: "sessions",
        elements: sessions,
        statType: "length",
      },
      {
        name: t("places:label_other"),
        endpoint: "places",
        elements: places,
        statType: "length",
      },
    ];

    return schedulingInsights?.map((insight, index) => <InsightCard key={index} {...insight} />);
  },
  () => true
);
