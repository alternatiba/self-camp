import React from "react";
import {useTranslation} from "react-i18next";
import {CardElement} from "@shared/components/CardElement";
import {SelectInput} from "@shared/inputs/SelectInput";
import {DEFAULT_COCKPIT_PREFERENCES} from "../CockpitConfigDrawer";

const FiguresSelect = () => {
  const {t} = useTranslation();

  return (
    <CardElement title={t("cockpit:dashboard.dashboardTabLabel")}>
      <SelectInput
        mode="multiple"
        name={"figures"}
        placeholder={t("cockpit:dashboard.figureSelect.placeholder")}
        options={DEFAULT_COCKPIT_PREFERENCES.figures.map((figureName) => ({
          label: t(`cockpit:${figureName}.label`),
          value: figureName,
        }))}
      />
    </CardElement>
  );
};

export default FiguresSelect;
