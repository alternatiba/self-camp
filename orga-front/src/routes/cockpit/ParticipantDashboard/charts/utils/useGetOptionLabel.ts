import {FieldCompInput} from "@shared/components/FieldsBuilder/FieldComp/FieldComp";
import {useTranslation} from "react-i18next";

export const useGetOptionLabel = () => {
  const {t} = useTranslation();

  /**
   * Retrieves the label for a given answer option using the form components map.
   *
   * @param optionKey - The answer option's identifier.
   * @param formComp the formComponent for this answer
   * @returns The corresponding label for the answer option.
   */
  return (optionKey: string, fieldComp: FieldCompInput): string => {
    if (optionKey === "true") return t("common:yes");
    if (optionKey === "false") return t("common:no");
    if (optionKey === "undefined") return t("common:undefined");
    return fieldComp.options?.find((o) => o.value === optionKey)?.label ?? optionKey;
  };
};
