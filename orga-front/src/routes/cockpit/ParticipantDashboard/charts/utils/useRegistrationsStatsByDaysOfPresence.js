import {useSelector} from "react-redux";
import {registrationsSelectors} from "@features/registrations";
import {useMemo} from "react";
import {removeDuplicates} from "@shared/utils/utilities";
import {calculateStatsForEachDay} from "./calculateMealsAndFreqStat";
import {meals} from "./meals";
import {useDaysOfPresenceBaseStats} from "./useDaysOfPresenceBaseStats";

export const useRegistrationsStatsByDaysOfPresence = () => {
  const registrationsBooked = useSelector(registrationsSelectors.selectListWithMetadata).filter(
    (registration) => registration.availabilitySlots?.length > 0
  );
  const daysOfPresenceBaseStats = useDaysOfPresenceBaseStats();

  return useMemo(() => {
    const mealsAndFreqStats = [];

    // Base tags container object with unique tags as keys and initial counts set to 0.
    const uniqueTagsOptions = removeDuplicates(
      registrationsBooked.reduce((acc, a) => (a.tags ? [...a.tags, ...acc] : acc), [])
    );
    const baseTagsContainer = Object.fromEntries(uniqueTagsOptions.map((tag) => [tag, 0]));

    daysOfPresenceBaseStats.forEach(
      // Initialize stats entry for each date render.
      (dateRender) => {
        mealsAndFreqStats.push({
          ...dateRender,
          participantsOnSite: 0,
          breakfast: 0,
          lunch: 0,
          dinner: 0,
          tags: {...baseTagsContainer}, // Destructure to clone the object
        });
      }
    );

    // Calculate meals and frequency statistics.
    registrationsBooked.forEach((registration) => {
      calculateStatsForEachDay(
        registration,
        mealsAndFreqStats,
        (mealsAndFreqStatForDay, dayOfPresence) => {
          // Increment participants on site count
          mealsAndFreqStatForDay.participantsOnSite += 1;

          // Increment meal counts if the participant had those meals on the current day
          meals.forEach((meal) => {
            if (dayOfPresence[meal.field]) {
              mealsAndFreqStatForDay[meal.field] += 1;
            }
          });

          // Increment tag counts for the current day
          registration.tags?.forEach((tag) => {
            mealsAndFreqStatForDay.tags[tag] += 1;
          });
        }
      );
    });
    return mealsAndFreqStats;
  }, [daysOfPresenceBaseStats, registrationsBooked]);
};
