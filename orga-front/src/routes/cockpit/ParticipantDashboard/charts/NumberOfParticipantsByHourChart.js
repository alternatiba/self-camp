import {useState} from "react";
import {CardElement} from "@shared/components/CardElement";
import {PendingSuspense} from "@shared/components/Pending";
import {addKeyToItemsOfList} from "@shared/utils/tableUtilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {Trans, useTranslation} from "react-i18next";
import {sorter} from "@shared/utils/sorters";
import dayjs from "@shared/services/dayjs";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {CategoriesSelectComponent} from "@shared/components/CategoriesSelectComponent";
import {useNumberOfParticipantsStats} from "./utils/useNumberOfParticipantsStats";
import {HideableContent} from "@shared/components/HideableContent";
import {Stack} from "@shared/layout/Stack";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/DualAxesChart")
);

export default function NumberOfParticipantsByHourChart() {
  const {t} = useTranslation();
  const [categoriesFilter, setCategoriesFilter] = useState([]);

  const numberOfParticipantsStats = useNumberOfParticipantsStats(categoriesFilter);

  // Create the stats objects for the Ant design plot
  const plottedMaxNumberOfParticipantsStats = numberOfParticipantsStats.map((stat) => ({
    hour: stat.hour,
    maxNumberOfParticipants: stat.maxNumberOfParticipants,
    name: t("cockpit:numberOfParticipantsByHourStats.maxNumberOfParticipants"),
  }));
  const plottedNumberParticipantsStats = numberOfParticipantsStats.map((stat) => ({
    hour: stat.hour,
    numberParticipants: stat.numberParticipants,
    name: t("cockpit:numberOfParticipantsByHourStats.numberParticipants"),
  }));

  const columns = [
    {
      title: t("cockpit:numberOfParticipantsByHourStats.hour"),
      dataIndex: "hour",
      sorter: (a, b) => sorter.date(dayjs(a.hour), dayjs(b.hour)),
    },
    {
      title: t("cockpit:numberOfParticipantsByHourStats.maxNumberOfParticipants"),
      dataIndex: "maxNumberOfParticipants",
    },
    {
      title: t("cockpit:numberOfParticipantsByHourStats.numberParticipants"),
      dataIndex: "numberParticipants",
    },
  ];

  return (
    <CardElement
      title={t("cockpit:numberOfParticipantsByHourStats.label")}
      extra={<CategoriesSelectComponent value={categoriesFilter} onChange={setCategoriesFilter} />}>
      <Stack gap={2}>
        <HideableContent buttonOpenTitle={t("cockpit:showPlotExplanation")}>
          <div style={{color: "gray"}}>
            <Trans ns="cockpit" i18nKey="numberOfParticipantsByHourStats.description" />
          </div>
        </HideableContent>

        <PendingSuspense>
          <DualAxesChart
            config={{
              data: [plottedMaxNumberOfParticipantsStats, plottedNumberParticipantsStats],
              xField: "hour",
              yField: ["maxNumberOfParticipants", "numberParticipants"],
              xAxis: {
                label: {
                  rotate: true,
                  offset: 40, // Décalage des abscisses vers le bas
                },
              },
              geometryOptions: [
                {geometry: "column", seriesField: "name"},
                {geometry: "line", smooth: true, lineStyle: {lineWidth: 3}, seriesField: "name"},
              ],

              // Use sync to synchronize both axis and hide the yAxis of the second chart
              meta: {
                maxNumberOfParticipants: {sync: "numberParticipants"},
                numberParticipants: {sync: true},
              },
              yAxis: {numberParticipants: false},
              interactions: [],
            }}
          />
        </PendingSuspense>
        <RawDataCollapsibleTable
          exportTitle={t("cockpit:numberOfParticipantsByHourStats.label")}
          columns={columns}
          dataSource={addKeyToItemsOfList(numberOfParticipantsStats)}
        />
      </Stack>
    </CardElement>
  );
}
