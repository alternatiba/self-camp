import React from "react";
import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {useTranslation} from "react-i18next";
import {getNumberOfParticipantsForDay} from "./utils/getNumberOfParticipantsForDay";

const ColumnChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "@shared/components/charts/ColumnChart")
);

/**
 * Component that displays a column chart with provided data, slider dates, and meal and frequency statistics.
 *
 * @component
 * @example
 * return <ColumnChartWithData data={chartData} sliderDates={dates} daysOfPresenceStats={stats} />;
 *
 * @param {Array<Object>} data - Data for the column chart.
 * @param {Array<Object>} daysOfPresenceStats - The meal and frequency statistics.
 * @param {"perDay" | "absolute"} displayMode the type of chart display mode
 * @returns {ReactNode} React component for displaying a column chart.
 */
const ColumnChartWithData = ({data, daysOfPresenceStats, displayMode}: {}) => {
  const {t} = useTranslation();

  // Configuration for chart labels and layout.
  const labelConfig = {
    position: "middle",
    formatter: (v) => (v.count !== 0 ? `${v.count}` : ""),
    layout: [
      {type: "interval-adjust-position"},
      {type: "interval-hide-overlap"},
      {type: "adjust-color"},
    ],
  };

  if (displayMode === "absolute") {
    const getTooltipContent = (v) => {
      return {
        name: t("registrations:label_other"),
        value: v.count,
      };
    };
    return (
      <ColumnChart
        config={{
          data,
          isStack: true,
          xField: "name",
          yField: "count",
          tooltip: {formatter: getTooltipContent},
          slider: false,
        }}
      />
    );
  } else if (displayMode === "perDay") {
    const getTooltipContent = (v) => {
      const participantsCount = getNumberOfParticipantsForDay(v.dateShort, daysOfPresenceStats);
      const percentage = (v.count / participantsCount) * 100;
      return {
        name: `${v.name}`,
        value: `${v.count} / ${participantsCount} (${percentage.toFixed(0)}%)`,
      };
    };

    return (
      <ColumnChart
        config={{
          data,
          isStack: true,
          xField: "dateShort",
          yField: "count",
          seriesField: "name",
          label: labelConfig,
          tooltip: {formatter: getTooltipContent},
          legend: {
            layout: "horizontal",
            position: "top-left",
          },
        }}
      />
    );
  }
};

export default ColumnChartWithData;
