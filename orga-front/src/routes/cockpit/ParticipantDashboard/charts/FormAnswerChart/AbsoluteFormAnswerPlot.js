import {truncate} from "@shared/utils/stringUtilities";
import BaseFormAnswerPlot from "./BaseFormAnswerPlot";
import {FieldComp} from "@shared/components/FieldsBuilder/FieldComp/FieldComp";
import {useFormAnswerStatsAbsolute} from "../utils/useFormAnswerStatsAbsolute";
import {useTranslation} from "react-i18next";
import {useGetOptionLabel} from "../utils/useGetOptionLabel";

export const AbsoluteFormAnswerPlot = ({formComp}: {formComp: FieldComp}) => {
  const {t} = useTranslation();
  const data = useFormAnswerStatsAbsolute(formComp.key);
  const getOptionLabel = useGetOptionLabel();
  const columns = [
    {title: t("cockpit:formAnswerChart.answer"), dataIndex: "name", key: "name"},
    {title: t("cockpit:formAnswerChart.total"), dataIndex: "count", key: "count"},
  ];

  data.forEach((stat) => {
    stat.name = getOptionLabel(stat.key, formComp);
  });

  const dataWithTruncatedLabels = data.map((v) => ({
    ...v,
    name: truncate(v.name, 40),
    fullName: v.name,
  }));

  return (
    <BaseFormAnswerPlot
      data={dataWithTruncatedLabels}
      columns={columns}
      answerLabel={formComp.label}
      displayMode="absolute"
      dataSourceTable={data}
    />
  );
};
