import {Alert} from "antd";
import {PendingSuspense} from "@shared/components/Pending";
import ColumnChartWithData from "../ColumnChartWithData";
import RawDataCollapsibleTable from "../utils/RawDataCollapsibleTable";
import {Stack} from "@shared/layout/Stack";
import {useTranslation} from "react-i18next";

const BaseFormAnswerPlot = ({
  data,
  columns,
  dataSourceTable,
  answerLabel,
  displayMode,
  daysOfPresenceStats,
  ...additionalProps
}) => {
  const {t} = useTranslation();
  if (!data || data.length === 0) {
    return (
      <Alert
        message={t("cockpit:formAnswerChart.youHaveNoAnswerYet.message")}
        description={t("cockpit:formAnswerChart.youHaveNoAnswerYet.description")}
      />
    );
  }

  return (
    <Stack gap={2}>
      <PendingSuspense>
        <ColumnChartWithData
          data={data}
          displayMode={displayMode}
          daysOfPresenceStats={daysOfPresenceStats}
          {...additionalProps}
        />
      </PendingSuspense>
      <RawDataCollapsibleTable
        exportTitle={`${t("cockpit:formAnswerChart.formAnswer")} - ${answerLabel}`}
        columns={columns}
        dataSource={dataSourceTable}
      />
    </Stack>
  );
};

export default BaseFormAnswerPlot;
