import {sorter} from "@shared/utils/sorters";
import {useTranslation} from "react-i18next";

import {dateFormatter} from "@shared/utils/formatters";

export const useDayStatColumn = () => {
  const {t} = useTranslation();

  return {
    title: t("common:dayInput.day"),
    dataIndex: "date",
    sorter: (a, b) => sorter.date(a.dayjsDate, b.dayjsDate),
    render: (text, record) =>
      dateFormatter.longDate(record.dayjsDate, {short: true, withYear: true}),
  };
};
