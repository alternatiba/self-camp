import {Button} from "antd";
import {UserSwitchOutlined} from "@ant-design/icons";
import {URLS} from "@app/configuration";
import React from "react";
import {useTranslation} from "react-i18next";
import {useParams} from "react-router-dom";
import {WithTooltipOrNot} from "@shared/components/WithTooltipOrNot";

export const ChangeIdentityButton = ({registration, noTooltip}) => {
  const {projectId} = useParams();
  const {t} = useTranslation();
  return (
    <WithTooltipOrNot
      noTooltip={noTooltip}
      title={t("registrations:actions.changeIdentity")}
      placement="left">
      <Button
        type="link"
        icon={<UserSwitchOutlined />}
        href={`${URLS.INSCRIPTION_FRONT}/${projectId}/participants?connectedAs=${registration._id}`}
      />
    </WithTooltipOrNot>
  );
};
