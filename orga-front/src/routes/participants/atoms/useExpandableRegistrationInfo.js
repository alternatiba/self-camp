import {FormElement} from "@shared/inputs/FormElement";
import {CardElement} from "@shared/components/CardElement";
import {Tag} from "antd";
import {TextInput} from "@shared/inputs/TextInput";
import {TableElement} from "@shared/components/TableElement";
import {EditableInlineRegistrationForm} from "./EditableInlineRegistrationForm";
import React from "react";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {useSelector} from "react-redux";
import {projectsSelectors} from "@features/projects";
import {useTranslation} from "react-i18next";
import {useRegistrationTicketsColumns} from "@utils/columns/useRegistrationTicketsColumns";
import {DisplayInput} from "@shared/inputs/DisplayInput";

export const useExpandableRegistrationInfo = () => {
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);

  const ticketColumns = useRegistrationTicketsColumns();

  // We have to really match the real width of the thing. Maybe there is a flex way to do this, but haven't found how to do so.
  const {windowWidth} = useWindowDimensions();
  const contentWidth =
    windowWidth - document.getElementsByClassName("ant-layout-sider")?.[0]?.offsetWidth - 30;

  return (record) => (
    <div style={{width: contentWidth, position: "sticky", left: 8}}>
      <FormElement>
        <div className="container-grid two-per-row">
          <CardElement greyedOut>
            <div className="container-grid two-per-row">
              <DisplayInput label="Tags">
                {record.tags?.map((tagName) => (
                  <Tag>{tagName}</Tag>
                ))}
              </DisplayInput>
              <TextInput label="Formulaire OK" readOnly value={record.formIsOk ? "✔️" : "❌"} />
            </div>
          </CardElement>
          {project.ticketingMode && (
            <TableElement.WithTitle
              title={t("registrations:schema.tickets.label")}
              showHeader
              fullWidth
              columns={ticketColumns}
              dataSource={record[`${project.ticketingMode}Tickets`]}
            />
          )}
        </div>
      </FormElement>

      <EditableInlineRegistrationForm record={record} />
    </div>
  );
};
