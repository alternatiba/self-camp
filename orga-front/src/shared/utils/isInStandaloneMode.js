// This is a functional component that checks if the application is running in standalone mode.
// Standalone mode refers to when a web application is launched from a user's home screen on a mobile device,
// and it appears as a separate application rather than inside a web browser.
export const isInStandaloneMode = () =>
  navigator.standalone ||
  window.matchMedia("(display-mode: standalone)").matches ||
  document.referrer.includes("android-app://");
