// USEEFFECT HELPERS
export const trySeveralTimes = (
  actionToTry,
  whatToDoAfter,
  ...{limitOfTrials = 5, frequency = 100, errorHandling}
) => {
  let trials = 0;
  const tryInterval = setInterval(() => {
    try {
      if (actionToTry()) {
        clearInterval(tryInterval);
        whatToDoAfter && whatToDoAfter();
      } else {
        trials += 1;
        if (trials >= limitOfTrials) {
          clearInterval(tryInterval);
        }
      }
    } catch (e) {
      errorHandling && errorHandling(e);
    }
  }, frequency);
};
