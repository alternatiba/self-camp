import {personName} from "../utilities";

export const searchInSessionFields = (session) => [
  session.name,
  session.activity.name,
  session.activity.category.name,
  ...(session.activity.secondaryCategories || []),
  ...(session.stewards?.map(personName) || []),
  ...(session.places?.map((p) => p.name) || []),
  session.team?.name,
];
