import {useTranslation} from "react-i18next";
import {Trans} from "react-i18next";
import {Timeline} from "antd";
import {DiscordLink, ContactNOELink} from "./NOESocialIcons";
import {HeartOutlined, QuestionCircleOutlined, ReadOutlined} from "@ant-design/icons";
import {LinkContainer} from "./LinkContainer";
import {URLS} from "@app/configuration";
import {DONATE_URL} from "@config/sharedConfig";

export const NOEUsefulLinksGuide = ({skipProjectCreation}) => {
  const {t} = useTranslation();

  const TimelineItem = ({title, description, extra}) => {
    return (
      <div style={{marginLeft: 12, marginBottom: 12}}>
        <p>
          <strong>{title}</strong>
        </p>
        <p>{description}</p>
        {extra && <div>{extra}</div>}
      </div>
    );
  };

  return (
    <Timeline
      style={{marginLeft: 16, marginTop: 20}}
      className="custom-icons-timeline"
      items={[
        !skipProjectCreation && {
          dot: "👋",
          children: (
            <TimelineItem
              title={t("common:NOEHelpGuide.content.createEvent.title")}
              description={t("common:NOEHelpGuide.content.createEvent.description")}
            />
          ),
        },
        {
          dot: "👥",
          children: (
            <TimelineItem
              title={t("common:NOEHelpGuide.content.joinCommunity.title")}
              description={t("common:NOEHelpGuide.content.joinCommunity.description")}
              extra={<DiscordLink />}
            />
          ),
        },
        {
          dot: "📖",
          children: (
            <TimelineItem
              title={t("common:NOEHelpGuide.content.learnToUse.title")}
              description={t("common:NOEHelpGuide.content.learnToUse.description")}
              extra={
                <div className="containerH" style={{gap: 26}}>
                  <LinkContainer
                    href={`${URLS.WEBSITE}/community/help-center`}
                    icon={<QuestionCircleOutlined />}>
                    {t("common:NOEHelpGuide.content.learnToUse.helpCenterButton")}
                  </LinkContainer>
                  <LinkContainer href={`${URLS.WEBSITE}/docs`} icon={<ReadOutlined />}>
                    {t("common:NOEHelpGuide.content.learnToUse.docsButton")}
                  </LinkContainer>
                </div>
              }
            />
          ),
        },
        {
          dot: "☎️",
          children: (
            <TimelineItem
              title={t("common:NOEHelpGuide.content.contactTeam.title")}
              description={t("common:NOEHelpGuide.content.contactTeam.description")}
              extra={<ContactNOELink />}
            />
          ),
        },
        {
          dot: "💝",
          children: (
            <TimelineItem
              title={t("common:NOEHelpGuide.content.makeDonation.title")}
              description={t("common:NOEHelpGuide.content.makeDonation.description")}
              extra={
                <LinkContainer icon={<HeartOutlined />} href={DONATE_URL}>
                  {t("common:NOEHelpGuide.content.makeDonation.donationInstructionsButton")}
                </LinkContainer>
              }
            />
          ),
        },
        {
          dot: "🚀",
          children: (
            <TimelineItem
              title={t("common:NOEHelpGuide.content.launchEvent.title")}
              description={t("common:NOEHelpGuide.content.launchEvent.description")}
            />
          ),
        },
      ].filter((el) => !!el)}
    />
  );
};

export const NOEHelpGuide = () => (
  <>
    <div style={{marginBottom: 40}}>
      <Trans
        i18nKey="NOEHelpGuide.content.intro"
        ns="common"
        components={{helpIcon: <QuestionCircleOutlined />}}
      />
    </div>
    <NOEUsefulLinksGuide />
  </>
);
