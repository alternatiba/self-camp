import {CheckOutlined, EditOutlined} from "@ant-design/icons";
import {store} from "@app/store";
import {MakeOptional} from "@mui/x-date-pickers/internals/models/helpers";
import {cleanAnswer} from "@shared/utils/columns/cleanAnswer";
import {Button, Form} from "antd";
import {ColumnType} from "antd/es/table";
import React, {ReactNode, useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import {useHover} from "usehooks-ts";
import {FormElement} from "../inputs/FormElement";
import {FieldCompInput} from "./FieldsBuilder/FieldComp/FieldComp";
import FormRenderer from "./FormRenderer";
import {UserNudgeDotPopover} from "./UserNudgeDotPopover";

const editableCellStyle = {
  base: {
    // Set a minimum height of 32 (22 + 2*5) to be exactly lie the height if the edit button
    minHeight: 32,
    // Add radius and transition timings
    borderRadius: 12,
    transition: "background 0.5s",
  },
  baseDisplay: {
    overflow: "hidden",
    padding: "5px 8px 5px 5px",
    margin: "-2px -8px -2px -5px",
  },
  baseEdit: {
    overflow: "unset",
    paddingRight: 8,
    marginRight: -8,
  },
  hover: {
    background: "var(--colorBgSelected)",
  },
  buttonBase: {
    background: "var(--colorBgContainer)",
    // On hover, add a border so we think that the edit button is inside the edit cell
    border: "2px solid var(--colorBgContainer)",
  },
  buttonHover: {
    border: "2px solid var(--colorBgSelected)",
  },
};

type EditableCellInnerFormProps = {
  fieldComp: Omit<FieldCompInput, "input">; // A definition of the form component to display
  value: any;
  editCell?: ReactNode; // A replacement of the default editable cell
  onChange: (value: any) => Promise<void>;
};

const EditableCellInnerForm = ({
  fieldComp,
  editCell,
  value,
  onChange,
}: EditableCellInnerFormProps) => {
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const [isLoading, setIsLoading] = useState(false);

  return (
    <FormElement
      form={form}
      onFinish={(values) => onChange?.(values[fieldComp.key])}
      initialValues={{[fieldComp.key]: value}}>
      <div
        className={"containerH flex-space reveal-on-hover-container"}
        style={{
          flexWrap: "wrap",
          flexDirection: "row-reverse",
          alignItems: "start",
          columnGap: 0,
          rowGap: 2,
          ...editableCellStyle.base,
          ...editableCellStyle.baseEdit,
          ...editableCellStyle.hover,
        }}>
        {/* And the button to validate the modification*/}
        <div
          style={{
            flexGrow: 0,
            marginLeft: "auto",
            translate: 8,
          }}>
          <UserNudgeDotPopover
            tourKey={"editableCellShortcut"}
            title={t("common:editableCell.shortcut.title")}
            content={t("common:editableCell.shortcut.content")}
            dotStyle={{top: -7, right: -7}}
          />
          <Button
            icon={<CheckOutlined />}
            type={"link"}
            loading={isLoading}
            onClick={() => {
              setIsLoading(true);
              form.submit();
            }}
            style={{
              ...editableCellStyle.buttonBase,
              ...editableCellStyle.buttonHover,
            }}
          />
        </div>

        {/* Render the field for the field, in minimalist mode */}
        {editCell || (
          <FormRenderer
            fields={[{...fieldComp, autoFocus: true, input: true}]}
            form={form}
            minimalist
          />
        )}
      </div>
    </FormElement>
  );
};

export type EditableCellProps = MakeOptional<EditableCellInnerFormProps, "onChange"> & {
  simpleDisplayCell?: ReactNode; // A replacement of the default cell in not-editable mode
};

export const EditableCell = ({
  fieldComp,
  value,
  onChange,
  simpleDisplayCell = cleanAnswer(value, undefined, fieldComp, "html"),
  editCell,
}: EditableCellProps) => {
  const {t} = useTranslation();
  const [isEditing, setIsEditing] = useState(false);

  const DisplayCellWithEditButton = () => {
    const editButtonRef = useRef(null);
    const isHover = useHover(editButtonRef);
    return (
      // Main container to reveal the edit button
      <div
        className={"containerH flex-space reveal-on-hover-container"}
        style={{
          overflow: "unset",
          // Align content at the center
          flexWrap: "nowrap",
          alignItems: "center",
          justifyContent: "center",
          // Extend the area in which the edit button reveal will work
          minHeight: 52,
          marginTop: -8,
          marginBottom: -8,
        }}>
        <div
          style={{
            ...editableCellStyle.base,
            ...editableCellStyle.baseDisplay,
            ...(isHover && editableCellStyle.hover),
          }}>
          {simpleDisplayCell}
        </div>

        {!fieldComp.disabled && (
          <Button
            ref={editButtonRef}
            title={t("common:editableCell.editFieldX", {
              fieldName: fieldComp.displayName || fieldComp.label,
            })}
            icon={<EditOutlined />}
            className={"reveal-on-hover-item"}
            type={"link"}
            onClick={() => setIsEditing(true)}
            style={{
              position: "absolute",
              right: 0,
              flexGrow: 0,
              flexShrink: 0,
              ...editableCellStyle.buttonBase,
              ...(isHover && editableCellStyle.buttonHover),
            }}
          />
        )}
      </div>
    );
  };

  return isEditing ? (
    // If the user is editing, show the editing inner form
    <div
      onDoubleClick={
        // Prevent double click from triggering some action outside the cell
        (e) => e.stopPropagation()
      }>
      <EditableCellInnerForm
        fieldComp={fieldComp}
        editCell={editCell}
        value={value}
        onChange={async (value) => {
          await onChange?.(value);
          setIsEditing(false);
        }}
      />
    </div>
  ) : // Else, if there is an onChange function, show the answer with the edit button...
  onChange ? (
    <DisplayCellWithEditButton />
  ) : (
    // ...and if no onChange, just display the answer alone (null because we should always return something to render)
    <>{simpleDisplayCell}</>
  );
};

export type EditableCellColumnOptions = {
  title: string;
  editableValueTitle: string;
  dataIndex: string;
  name?: string;
  beforeChange: (newRecord: any, value: any | Array<any>, currentRecord: any) => Promise<void>;
  elementsActions: {persist: (fieldsToUpdate?: any) => any};
} & Omit<EditableCellProps["fieldComp"], "key" | "label">;

export type EditableCellComponentsRenders = {
  simpleDisplayCellRender?: (record: any) => EditableCellProps["simpleDisplayCell"];
  editCellRender?: (record: any) => EditableCellProps["editCell"];
};

export const editableCellColumn = (
  {
    title,
    dataIndex,
    editableValueTitle = title,
    name = dataIndex,
    beforeChange,
    elementsActions,
    ...fieldComp
  }: EditableCellColumnOptions,
  {
    simpleDisplayCellRender, // The cell in not-editable mode
    editCellRender, // The cell in editable mode
  }: EditableCellComponentsRenders = {}
): Pick<ColumnType<any>, "title" | "dataIndex" | "render"> => ({
  title,
  dataIndex,
  render: (text, record) => {
    return (
      <EditableCell
        value={record[name]}
        fieldComp={{...fieldComp, key: name, label: editableValueTitle}}
        onChange={async (value) => {
          if (beforeChange) {
            const fullPayload = {...record, [name]: value};
            await beforeChange?.(fullPayload, value, record);
          }

          return store
            .dispatch(
              elementsActions.persist({
                _id: record._id,
                [name]: value,
              })
            )
            .catch(() => {
              /* do nothing */
            });
        }}
        simpleDisplayCell={simpleDisplayCellRender?.(record)}
        editCell={editCellRender?.(record)}
      />
    );
  },
});
