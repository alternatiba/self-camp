import {Pie} from "@ant-design/plots";
import {ChartContainer} from "./ChartContainer";

export default function PieChart({config}) {
  return <ChartContainer chart={Pie} config={config} />;
}
