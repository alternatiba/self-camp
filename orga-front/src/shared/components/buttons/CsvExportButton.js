import React, {useRef} from "react";
import {Parser} from "@json2csv/plainjs";
import {Button, Tooltip} from "antd";
import {ExportOutlined} from "@ant-design/icons";
import {normalize} from "@shared/utils/stringUtilities";
import {dayjsBase} from "@shared/services/dayjs";

export default function CsvExportButton({
  dataExportFunction,
  withFields,
  children,
  tooltip,
  getExportName,
  withCurrentDate,
  ...props
}) {
  const downloadButton = useRef();

  const exportData = async () => {
    const dateString = withCurrentDate ? ` - ${dayjsBase().format("L LT")}` : "";

    const exportName = normalize(getExportName() + dateString, true).replace(/[^\w ]/g, "-");

    try {
      const exportedData = await dataExportFunction();
      const fields = withFields ? exportedData[0] : undefined;
      const parser = new Parser({fields});
      const csv = parser.parse(withFields ? exportedData.slice(1) : exportedData);

      let file = new File([csv], exportName, {
        type: "text/csv",
      });
      let exportUrl = URL.createObjectURL(file);

      downloadButton.current.setAttribute("href", exportUrl);
      downloadButton.current.setAttribute("download", exportName);
      downloadButton.current.click();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <Tooltip title={tooltip}>
        <Button
          onClick={(e) => {
            exportData();
          }}
          icon={<ExportOutlined />}
          type="link"
          {...props}>
          {children}
        </Button>
      </Tooltip>
      <a ref={downloadButton} style={{display: "none"}} />
    </>
  );
}
