import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {Button, Tooltip} from "antd";
import {SettingOutlined} from "@ant-design/icons";
import {Drawer} from "@shared/components/Drawer";
import {CardElement} from "@shared/components/CardElement";
import {FormElement} from "@shared/inputs/FormElement";
import {SelectInput} from "@shared/inputs/SelectInput";

export const ListSettingsDrawerButton = ({
  settingsDrawerContent,
  localStorageDisplaySize: [displaySize, setDisplaySize],
}) => {
  const [settingsDrawerOpen, setSettingsDrawerOpen] = useState(false);
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();

  return (
    <>
      <Tooltip title={t("common:listPage.displaySettings.tooltip")} placement="topRight">
        <Button
          icon={<SettingOutlined />}
          style={{
            transform:
              `translateY(${isMobileView ? -7 : displaySize === "small" ? 4 : 8}px) ` +
              `translateX(${isMobileView ? 8 : 0}px)`,
          }}
          shape="circle"
          size={isMobileView ? "small" : "default"}
          className="shadow"
          onClick={() => setSettingsDrawerOpen(true)}
        />
      </Tooltip>

      <Drawer
        title={t("common:settings")}
        width={isMobileView ? undefined : "700px"}
        open={settingsDrawerOpen}
        setOpen={setSettingsDrawerOpen}>
        <div style={{padding: 16}}>
          <p style={{color: "gray", marginBottom: 26}}>
            {t("common:listPage.settings.savedOnYourDeviceOnly")}
          </p>
          {settingsDrawerContent}
          <CardElement title={t("common:listPage.settings.visualComfort")}>
            <FormElement>
              <SelectInput
                label={t("common:listPage.settings.displayDensity.label")}
                onChange={setDisplaySize}
                defaultValue={displaySize}
                options={[
                  {
                    value: "small",
                    label: t("common:listPage.settings.displayDensity.options.small"),
                  },
                  {
                    value: "middle",
                    label: t("common:listPage.settings.displayDensity.options.comfort"),
                  },
                ]}
              />
            </FormElement>
          </CardElement>
        </div>
      </Drawer>
    </>
  );
};
