import {FieldCompInputType, FieldCompLayoutType} from "../fieldCompCustomProps/FieldCompInputType";

export type Conditional = {show: "show" | "doNotShow"; when: string; eq: string};

type FieldCompBase = {
  isNew?: boolean;
  key: string;
  conditional?: Conditional;
  meta: any;
};

export type FieldCompInput = FieldCompBase & {
  input: true;
  label: string;
  type: FieldCompInputType;
  description?: string;
  placeholder?: string;
  displayName?: string;
  displayOptions: Array<string>;
  disabled?: boolean;
  required?: boolean;
  selectedCount?: {
    min?: number;
    max?: number;
  };
  minMaxNumber?: {
    min?: number;
    max?: number;
  };
  options?: Array<{label: string; value: string}>;
};

export type FieldCompLayout = FieldCompBase & {
  input: false;
  label?: string;
  type: FieldCompLayoutType;
  content?: string;
  components?: Array<FieldComp>;
};

export type FieldComp = FieldCompInput | FieldCompLayout;

export type FieldInputComps = Array<FieldCompInput>;

export const initialInputFieldCompValues: Partial<FieldCompInput> = {
  type: "text",
  key: "", // We should put this to enable the autofill key feature
  displayOptions: [], // We should initialize the array to prevent displayName reset in DisplayNameInput
  input: true, // Needed to tell this is not a layout component, but an input component
  isNew: true,
};

export const initialContentFieldCompValues: Partial<FieldCompLayout> = {
  type: "content",
  key: "", // We should put this to enable the autofill key feature
  input: false, // Needed to tell this is not a layout component, but an input component
  isNew: true,
};

export const initialPanelFieldCompValues: Partial<FieldCompLayout> = {
  type: "panel",
  key: "", // We should put this to enable the autofill key feature
  input: false, // Needed to tell this is not a layout component, but an input component
  isNew: true,
};
