import {SwitchInputInline} from "@shared/inputs/SwitchInput";
import {TextInput} from "@shared/inputs/TextInput";
import {Form} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import {useTranslation} from "react-i18next";
import {useFieldCompsFormInstance} from "../atoms/useFieldCompsFormInstance";
import {
  CustomPropsGroup,
  FieldComponentCustomProps,
} from "../fieldCompCustomProps/fieldCompCustomPropsConfig";
import {FieldsBuilderConfig} from "../FieldsBuilder";

const DisplayNameInput: CustomPropsGroup = ({rootName, name, config}) => {
  const form = useFieldCompsFormInstance();
  const displayOptions = Form.useWatch([...rootName, name, "displayOptions"], form);
  const {t} = useTranslation();

  // Only show the display name input if some dependencies are activated
  const showDisplayNameInput = displayOptions && displayOptions?.length > 0;

  return showDisplayNameInput ? (
    <TextInput
      label={t("fieldsBuilder:schema.displayName.label")}
      placeholder={t("fieldsBuilder:schema.displayName.placeholder")}
      required
      name={[name, "displayName"]}
    />
  ) : null;
};

export function GeneralFieldCompConfig({
  rootName,
  name,
  customProps: CustomProps,
  config,
}: {
  rootName: NamePath;
  name: number;
  customProps?: FieldComponentCustomProps["generalTab"];
  config: FieldsBuilderConfig;
}) {
  const {t} = useTranslation();

  const CustomDisplayConfig = config.customDisplayConfig;
  const CustomGeneralConfig = config.customGeneralConfig;

  return (
    <div className={"container-grid three-per-row"} style={{alignItems: "start"}}>
      {/* Left column */}
      <div className={"container-grid"}>
        {CustomProps && <CustomProps name={name} rootName={rootName} config={config} />}
        <TextInput
          label={t("fieldsBuilder:schema.description.label")}
          placeholder={t("fieldsBuilder:schema.description.placeholder")}
          name={[name, "description"]}
        />
      </div>

      {/* Middle column */}
      <div className={"container-grid"} style={{gap: 16}}>
        {CustomDisplayConfig && (
          <CustomDisplayConfig name={name} rootName={rootName} config={config} />
        )}

        <DisplayNameInput name={name} rootName={rootName} config={config} />
      </div>

      {/* Right column */}
      <div className={"container-grid"} style={{gap: 16}}>
        <SwitchInputInline
          label={t("fieldsBuilder:schema.required.label")}
          name={[name, "required"]}
        />
        {CustomGeneralConfig && (
          <CustomGeneralConfig name={name} rootName={rootName} config={config} />
        )}
      </div>
    </div>
  );
}
