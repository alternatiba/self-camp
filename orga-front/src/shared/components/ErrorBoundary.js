import {lazyWithRetry} from "@shared/utils/lazyWithRetry";
import {ErrorBoundary as SentryErrorBoundary} from "@sentry/react";
import React, {Suspense} from "react";

const ErrorPageContent = lazyWithRetry(() =>
  import(
    /* webpackPrefetch: true */
    /* webpackFetchPriority: "low" */
    "@shared/layout/ErrorPageContent"
  )
);

export const ErrorBoundary = ({children, onError, extra}) => (
  <SentryErrorBoundary
    key={window.location.pathname}
    onError={onError}
    fallback={() => (
      <Suspense fallback={null}>
        <ErrorPageContent extra={extra} />
      </Suspense>
    )}>
    {children}
  </SentryErrorBoundary>
);
