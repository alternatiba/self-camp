import {useNavigate} from "react-router-dom";
import {Divider, Menu as AntMenu} from "antd";
import React from "react";

export const SidebarDivider = ({color, style}) => (
  <Divider
    style={{
      borderTop: "1px solid " + color,
      margin: "15pt 25%",
      minWidth: "unset",
      width: "unset",
      opacity: 0.6,
      ...style,
    }}
  />
);

export const MenuLayout = ({
  disabled,
  disabledMessage,
  title,
  selectedItem,
  children,
  divider = false,
  items,
  rootNavUrl = "",
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const navigate = useNavigate();
  const color = disabled ? "rgba(255,255, 255, 0.3)" : "rgba(255, 255, 255, 0.8)";

  // If there are some items, then add the navigation. If the url is explicitely set to false,
  // and there is no already defined onClick action, then don't add the onClick function
  items?.forEach((item) => {
    if (item && item.url !== false && item.onClick === undefined)
      item.onClick = ({key}) => {
        item.url ? window.location.replace(item.url) : navigate(rootNavUrl + key);
      };
    else delete item?.url;
  });

  return (
    <div title={disabled ? disabledMessage : undefined}>
      {divider && <SidebarDivider color={color} />}

      {title && (
        <p className="text-center" style={{color: color, paddingTop: "5px"}}>
          {title.toUpperCase()}
        </p>
      )}

      {items ? <AntMenu theme="dark" selectedKeys={[selectedItem]} items={items} /> : children}
    </div>
  );
};
