import {t} from "i18next";

// Custom inputs
// The divider plugin
import divider from "@react-page/plugins-divider";

// The spacer plugin
import spacer from "@react-page/plugins-spacer";
import "@react-page/plugins-spacer/lib/index.css";

// The video plugin
import video from "@react-page/plugins-video";
import "@react-page/plugins-video/lib/index.css";

// Customized plugins
import {backgroundP, imageP} from "./customPlugins";
import {colorPickerComponent, numberComponent, switchComponent} from "./inputComponents";
import {quillEditorP} from "./quillEditorP";
import {slateP} from "./slateP";

const plugins = [quillEditorP, slateP, backgroundP, spacer, divider, imageP, video];

const addStylesAndMarginDefaultControl = (plugins) =>
  plugins.map((plugin) => ({
    ...plugin,
    cellStyle: (data) => {
      return {
        ...(data.advancedPadding
          ? {
              paddingLeft: data.paddingLeft,
              paddingRight: data.paddingRight,
              paddingTop: data.paddingTop,
              paddingBottom: data.paddingBottom,
            }
          : {padding: data.padding}),
        ...(data.advancedMargin
          ? {
              marginLeft: data.marginLeft,
              marginRight: data.marginRight,
              marginTop: data.marginTop,
              marginBottom: data.marginBottom,
              height: `calc(100% - ${data.marginTop + data.marginBottom}px)`,
            }
          : {margin: data.margin, height: `calc(100% - ${data.margin * 2}px)`}),
        backgroundColor: data.backgroundColor,
        border:
          data.borderWidth > 0 ? `${data.borderWidth}px solid ${data.borderColor}` : undefined,
        borderRadius: data.borderRadius,
        overflow: data.borderRadius > 0 && "hidden", // So the border-radius can have an effect
      };
    },
    cellSpacing: (data) => ({x: data.cellSpacingX, y: data.cellSpacingY}),
    controls: [
      {title: t("common:reactPage.controlsTitle"), controls: plugin.controls},
      {
        title: t("common:reactPage.marginsTitle"),
        controls: {
          type: "autoform",
          columnCount: 2,
          schema: {
            properties: {
              advancedPadding: {
                title: t("common:reactPage.paddings.advanced"),
                default: false,
                uniforms: {component: switchComponent},
              },
              padding: {
                title: t("common:reactPage.paddings.main"),
                uniforms: {showIf: (data) => !data.advancedPadding, component: numberComponent},
              },
              paddingTop: {
                title: t("common:reactPage.paddings.top"),
                uniforms: {showIf: (data) => data.advancedPadding, component: numberComponent},
              },
              paddingBottom: {
                title: t("common:reactPage.paddings.bottom"),
                uniforms: {showIf: (data) => data.advancedPadding, component: numberComponent},
              },
              paddingLeft: {
                title: t("common:reactPage.paddings.left"),
                uniforms: {showIf: (data) => data.advancedPadding, component: numberComponent},
              },
              paddingRight: {
                title: t("common:reactPage.paddings.right"),
                uniforms: {showIf: (data) => data.advancedPadding, component: numberComponent},
              },

              advancedMargin: {
                title: t("common:reactPage.margins.advanced"),
                default: false,
                uniforms: {component: switchComponent},
              },
              margin: {
                title: t("common:reactPage.margins.main"),
                uniforms: {showIf: (data) => !data.advancedMargin, component: numberComponent},
              },
              marginTop: {
                title: t("common:reactPage.margins.top"),
                uniforms: {showIf: (data) => data.advancedMargin, component: numberComponent},
              },
              marginBottom: {
                title: t("common:reactPage.margins.bottom"),
                uniforms: {showIf: (data) => data.advancedMargin, component: numberComponent},
              },
              marginLeft: {
                title: t("common:reactPage.margins.left"),
                uniforms: {showIf: (data) => data.advancedMargin, component: numberComponent},
              },
              marginRight: {
                title: t("common:reactPage.margins.right"),
                uniforms: {showIf: (data) => data.advancedMargin, component: numberComponent},
              },
            },
          },
        },
      },
      {
        title: t("common:reactPage.stylesTitle"),
        controls: {
          type: "autoform",
          columnCount: 2,
          schema: {
            properties: {
              borderRadius: {
                title: t("common:reactPage.styles.borderRadius"),
                uniforms: {component: numberComponent},
              },
              borderWidth: {
                title: t("common:reactPage.styles.borderWidth"),
                uniforms: {component: numberComponent},
              },
              borderColor: {
                title: t("common:reactPage.styles.borderColor"),
                uniforms: {component: colorPickerComponent},
              },
              backgroundColor: {
                title: t("common:reactPage.styles.backgroundColor"),
                uniforms: {component: colorPickerComponent},
              },
              cellSpacingX: {
                title: t("common:reactPage.styles.horizontalCellSpacing"),
                uniforms: {component: numberComponent},
              },
              cellSpacingY: {
                title: t("common:reactPage.styles.verticalCellSpacing"),
                uniforms: {component: numberComponent},
              },
            },
          },
        },
      },
    ],
    bottomToolbar: {dark: true},
  }));

export default addStylesAndMarginDefaultControl(plugins);
