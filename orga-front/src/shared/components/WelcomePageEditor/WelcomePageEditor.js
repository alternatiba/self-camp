import Editor from "@react-page/editor";
import "@react-page/editor/lib/index.css";
import "@styles/react-page.less";
import editorPlugins from "./plugins";
import React from "react";

const WelcomePageEditor = ({
  value,
  style = {height: "100%"},
  readOnly = false,
  onChange,
  roundedBorders,
  className,
}) => (
  <div
    style={{
      paddingTop: roundedBorders ? 5 : 0,
      paddingLeft: roundedBorders ? 8 : 0,
      paddingRight: roundedBorders ? 5 : 0,
      paddingBottom: roundedBorders ? 8 : 90,
    }}>
    <div
      className={`containerV ${className}`}
      style={{
        borderRadius: roundedBorders ? 20 : 0,
        overflow: "hidden",
        background: "white",
      }}>
      {value && (
        <Editor
          onChange={onChange}
          cellPlugins={editorPlugins}
          value={value}
          style={style}
          readOnly={readOnly}
        />
      )}
    </div>
  </div>
);

export default WelcomePageEditor;
