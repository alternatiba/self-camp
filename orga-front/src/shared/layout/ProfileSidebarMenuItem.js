import {useTranslation} from "react-i18next";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import React from "react";
import {Avatar, Badge} from "antd";
import {EditOutlined, LoginOutlined, SettingOutlined, UserOutlined} from "@ant-design/icons";
import {personName} from "@shared/utils/utilities";
import {MenuLayout} from "../components/Menu";

export default function ProfileSidebarMenuItem({user, collapsedSidebar, setShowProfile}) {
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();
  const collapsedName = !isMobileView && collapsedSidebar;

  return user?._id ? (
    <div
      onClick={() => setShowProfile(true)}
      style={{
        cursor: "pointer",
        paddingTop: 7,
        paddingBottom: 10,
        paddingLeft: !collapsedName && 16,
        textAlign: collapsedName && "center",
        opacity: 0.9,
        overflowX: "hidden",
        whiteSpace: "nowrap",
      }}>
      <Badge
        style={{marginTop: 27, marginRight: 2}}
        count={
          <SettingOutlined
            style={{backgroundColor: "white", borderRadius: "50%", padding: 2, fontSize: 12}}
          />
        }>
        <Avatar
          style={{display: "inline-block", outline: "2px solid white"}}
          className="bg-noe-gradient"
          icon={<UserOutlined />}
        />
      </Badge>

      {!collapsedName && (
        <div style={{display: "inline-block", fontWeight: "bold", color: "white", paddingLeft: 14}}>
          {personName(user)}
        </div>
      )}
    </div>
  ) : (
    <MenuLayout
      rootNavUrl={"/"}
      items={[
        {label: t("common:login"), key: "login", icon: <LoginOutlined />},
        {label: t("common:createAccount"), key: "signup", icon: <EditOutlined />},
      ]}
    />
  );
}
