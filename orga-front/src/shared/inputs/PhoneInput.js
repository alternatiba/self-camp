import type {FormItemProps} from "./FormItem";
import {FormItem} from "./FormItem";
import React, {useEffect, useState} from "react";
import type {InputProps} from "antd";
import {Input} from "antd";
import unknownFlag from "@images/unknownFlag.png";
import {AsYouType, formatIncompletePhoneNumber} from "libphonenumber-js";
import {phoneValidator} from "@shared/utils/phoneValidator";

// Flag icons thanks to the free service https://flagpedia.net/
export const getFlagIcon = (country2Code: string) => {
  const src =
    !country2Code || country2Code.length !== 2
      ? unknownFlag
      : `https://flagcdn.com/16x12/${country2Code.toLowerCase()}.png`;

  return <img src={src} width="16" height="12" alt={"Phone flag " + country2Code} />;
};

export const PhoneInputComponent = (props: InputProps) => {
  const [currentCountryCode, setCurrentCountryCode] = useState();

  const getPhoneCountry = async (value): Promise<string | undefined> => {
    if (!value) return;
    const asYouType = new AsYouType();
    asYouType.input(value);
    return asYouType.getCountry();
  };

  useEffect(() => {
    getPhoneCountry(props.value).then(setCurrentCountryCode);
  }, [props.value]);

  return (
    <Input
      bordered={false}
      type="tel"
      {...props}
      prefix={getFlagIcon(currentCountryCode)}
      onChange={({target: {value}}) => props.onChange?.(formatIncompletePhoneNumber(value))}
    />
  );
};

export const PhoneInput = (props: FormItemProps<InputProps>) => {
  return (
    <FormItem
      {...{
        ...props,
        autoComplete: "phone",
        rules: [...(props.rules || []), {validator: phoneValidator}],
      }}>
      <PhoneInputComponent />
    </FormItem>
  );
};
