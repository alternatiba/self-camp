import dayjs from "@shared/services/dayjs";
import {TimePicker, TimePickerProps} from "antd";
import {FormItem, FormItemProps} from "./FormItem";

export const TimeInput = (props: FormItemProps<TimePickerProps>) => (
  <FormItem {...props}>
    <TimeInputComponent />
  </FormItem>
);

export const TimeInputComponent = (props: TimePickerProps) => (
  <TimePicker
    minuteStep={5}
    bordered={false}
    showSecond={false}
    format={(date) => dayjs(date).format("LT")}
    {...props}
  />
);
