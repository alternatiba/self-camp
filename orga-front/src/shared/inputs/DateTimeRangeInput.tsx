import {Dayjs} from "@shared/services/dayjs";
import {DatePicker} from "antd";
import {RangePickerProps} from "antd/es/date-picker";
import type {DateTimeInputProps} from "./DateTimeInput";
import {DatePickerComponent} from "./DateTimeInput";
import {FormItem, FormItemProps} from "./FormItem";

export type DateTimeRangeInputProps = Omit<RangePickerProps & DateTimeInputProps, "value"> & {
  value?: {start: string | Dayjs; end: string | Dayjs};
};

/**
 * Range Datetime input
 */
export const DateTimeRangeInput = (props: FormItemProps<DateTimeRangeInputProps>) => (
  <FormItem {...props}>
    <DatePickerComponent isRange DatePickerComponent={DatePicker.RangePicker} />
  </FormItem>
);
