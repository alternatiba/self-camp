import {EyeInvisibleOutlined, EyeOutlined} from "@ant-design/icons";
import {Input, InputProps} from "antd";
import React from "react";
import {FormItem, FormItemProps} from "./FormItem";

/**
 * Basic Text input
 */
export const TextInput = (props: FormItemProps<InputProps>) => (
  <FormItem {...props}>
    <Input bordered={false} />
  </FormItem>
);

/**
 * Email Text input
 */
export const TextInputEmail = (props: FormItemProps<InputProps>) => (
  <FormItem
    {...{
      ...props,
      rules: [...(props.rules || []), {type: "email"}],
      normalize: (value) =>
        // Set lowercase and exclude all characters not included in \w\d!#$%&’*+-/=?^_`{|}~@
        value ? value.toLowerCase().replace(/[^\w\d!#$%&’*+-/=?^_`{|}~@]/, "") : value,
    }}>
    <Input bordered={false} autoComplete={"email"} />
  </FormItem>
);

/**
 * Password Text input
 */
export const TextInputPassword = (props: FormItemProps<InputProps>) => (
  <FormItem {...props}>
    <Input.Password
      bordered={false}
      iconRender={(visible) => (visible ? <EyeOutlined /> : <EyeInvisibleOutlined />)}
    />
  </FormItem>
);
