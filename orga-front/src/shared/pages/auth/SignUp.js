import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {Button, Form} from "antd";
import {AuthPage} from "../AuthPage";
import {fieldToData} from "@shared/utils/tableUtilities";
import {viewSelectors} from "@features/view";
import {currentProjectSelectors} from "@features/currentProject";
import {Trans, useTranslation} from "react-i18next";
import {safeValidateFields} from "../../inputs/FormElement";
import {TextInput, TextInputEmail, TextInputPassword} from "../../inputs/TextInput";
import {Pending} from "../../components/Pending";
import {fetchWithMessages} from "@shared/utils/api/fetchWithMessages";
import {useNavigate, useParams} from "react-router-dom";
import {validatePassword} from "@shared/utils/passwordValidators";

export default function SignUp({footer}) {
  const navigate = useNavigate();
  const {projectId} = useParams();
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const [invitedRegistration, setInvitedRegistration] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    if (searchParamsInfo.invitationToken && currentProject._id) {
      fetchWithMessages(
        `projects/${currentProject._id}/registrations/byInvitationToken/${searchParamsInfo.invitationToken}`,
        {noAuthNeeded: true, method: "GET"},
        {},
        false
      )
        .then((invitedRegistration) => {
          setInvitedRegistration(invitedRegistration);
          form.setFieldsValue(invitedRegistration.user);
        })
        .catch(() =>
          dispatch(
            currentUserActions.changeConnectionError(
              t("common:connectionPage.invitationTokenInvalid")
            )
          )
        );
    }
  }, [currentProject._id]);

  // Don't render if there is an invitation token and we haven't fetched the invited registration yet
  const pending = searchParamsInfo.invitationToken && !invitedRegistration;

  const onChange = (changedFields, allFields) =>
    dispatch(currentUserActions.changeUser(fieldToData(allFields)));

  const signUp = () =>
    safeValidateFields(form, () => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.signUp()).then((success) => success && navigate("./../login"));
    });

  const goToLogInPage = () => {
    dispatch(currentUserActions.changeConnectionError(undefined));
    navigate("./../login");
  };

  const roleLabel =
    invitedRegistration?.role &&
    t(`registrations:schema.role.options.${invitedRegistration?.role}.label`);
  const greetingName =
    invitedRegistration?.user.firstName || invitedRegistration?.steward?.firstName;

  return (
    <AuthPage
      form={form}
      dataFormType={"register"}
      projectId={projectId}
      footer={footer}
      subtitle={
        !pending && invitedRegistration ? (
          <>
            {greetingName && <p>{t("welcome:hiFirstName", {firstName: greetingName})}</p>}
            <p>
              {roleLabel ? (
                invitedRegistration?.steward ? (
                  <Trans
                    ns="common"
                    i18nKey="connectionPage.invitations.asStewardAndRole"
                    values={{role: roleLabel}}
                  />
                ) : (
                  <Trans
                    ns="common"
                    i18nKey="connectionPage.invitations.asRole"
                    values={{role: roleLabel}}
                  />
                )
              ) : (
                <Trans ns="common" i18nKey="connectionPage.invitations.asSteward" />
              )}{" "}
              <Trans
                ns="common"
                i18nKey="connectionPage.invitations.onTheEvent"
                values={{projectName: currentProject.name}}
              />
              .
            </p>
            {t("common:connectionPage.invitations.fillYourInformationToCreateAccount")}
          </>
        ) : (
          t("common:connectionPage.newAccount")
        )
      }
      infoText={
        <div style={{textAlign: "center", color: "grey"}}>
          <div>{t("common:connectionPage.moreInfoOnYourData.message")}</div>
          {process.env.REACT_APP_DATA_POLICY_URL && (
            <a href={process.env.REACT_APP_DATA_POLICY_URL} target={"_blank"} rel="noreferrer">
              {t("common:connectionPage.moreInfoOnYourData.buttonTitle")}
            </a>
          )}
        </div>
      }
      initialValues={{
        firstName: currentUser.firstName || searchParamsInfo.firstName,
        lastName: currentUser.lastName || searchParamsInfo.lastName,
        email: currentUser.email || searchParamsInfo.email,
        password: currentUser.password,
      }}
      onFieldsChange={onChange}
      onValidate={signUp}
      buttons={
        !pending && (
          <>
            <Button type="primary" onClick={signUp} htmlType="submit">
              {t("common:createAccount")}
            </Button>
            <Button onClick={goToLogInPage}>{t("common:iAlreadyHaveAnAccount")}</Button>
          </>
        )
      }>
      {pending ? (
        <Pending minHeight={500} />
      ) : (
        <>
          <TextInput
            name="firstName"
            i18nNs="users"
            autoComplete="given-name"
            data-form-type="name,first"
            required
            bordered
          />
          <TextInput
            name="lastName"
            i18nNs="users"
            autoComplete="family-name"
            data-form-type="name,last"
            required
            bordered
          />
          <TextInputEmail
            name="email"
            i18nNs="users"
            autoComplete="email"
            data-form-type="email,new"
            disabled={invitedRegistration}
            required
            bordered
          />
          <TextInputPassword
            name="password"
            i18nNs="users"
            autoComplete="new-password"
            data-form-type="password,new"
            rules={[{validator: validatePassword(form)}, {min: 8}]}
            required
            bordered
          />
        </>
      )}
    </AuthPage>
  );
}
