import {Alert, Card} from "antd";
import React, {useEffect} from "react";
import {currentUserSelectors} from "@features/currentUser";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectActions, currentProjectSelectors} from "@features/currentProject";
import {FormElement} from "../inputs/FormElement";
import {connectionPageBackground, connectionPageLogo} from "@app/configuration";
import {DynamicProjectThemeProvider} from "@shared/layout/DynamicProjectThemeProvider";
import {useNavigate} from "react-router-dom";
import {H1Title} from "@shared/layout/typography";

export const AuthPage = ({
  projectId,
  subtitle,
  infoText,
  children,
  buttons,
  className,
  footer,
  dataFormType, // See https://dashlane.github.io/SAWF/ for the data-form-type values
  ...otherProps
}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const connectionError = useSelector(currentUserSelectors.selectConnectionError);
  const connectionNotice = useSelector(currentUserSelectors.selectConnectionNotice);

  const currentProject = useSelector(currentProjectSelectors.selectProject);

  useEffect(() => {
    projectId
      ? dispatch(currentProjectActions.load(projectId)).catch(() => navigate("/login"))
      : dispatch(currentProjectActions.cleanProject());
  }, [projectId]);

  // On unmount (ie. connection), fill the body element with the default background color, for a smooth transition, then remove the background color
  useEffect(() => () => {
    document.body.style.setProperty("background", connectionPageBackground);
    setTimeout(() => document.body.style.removeProperty("background"), 700);
  });

  // Render the page if there is no projectId, or if the project is actually loaded
  return !projectId || currentProject._id ? (
    <DynamicProjectThemeProvider>
      <div
        style={{
          minHeight: "100vh",
          justifyContent: "center",
          background: connectionPageBackground,
          transition: "1s background",
        }}
        className={`containerH ${className} fade-in`}>
        <div
          style={{
            width: "min(95vw, 500px)",
            flexGrow: 0,
            padding: "40px 0",
            justifyContent: "center",
            alignItems: "stretch",
          }}
          className="containerV">
          <img
            src={connectionPageLogo}
            height={45}
            alt={"NOÉ logo"}
            style={{marginBottom: 40, marginRight: "auto", marginLeft: "auto"}}
          />
          <Card style={{padding: 5}}>
            <div style={{textAlign: "center", paddingBottom: 12}}>
              {currentProject?.name && (
                <H1Title className="fade-in">{currentProject.name.toUpperCase()}</H1Title>
              )}
              {subtitle && <div style={{fontSize: 16, paddingBottom: 12}}>{subtitle}</div>}
              {infoText && (
                <div
                  style={{
                    marginBottom: 6,
                    marginTop: 6,
                    marginRight: 27,
                    marginLeft: 27,
                    fontSize: 13,
                  }}>
                  {infoText}
                </div>
              )}
            </div>
            {connectionError && (
              <Alert style={{marginBottom: "10pt"}} type="error" message={connectionError} />
            )}
            {connectionNotice && (
              <Alert style={{marginBottom: "10pt"}} type="success" message={connectionNotice} />
            )}

            <FormElement
              data-form-type={dataFormType}
              className="container-grid auth-page-container"
              style={{paddingTop: "10pt", paddingBottom: "8pt", alignItems: "stretch"}}
              {...otherProps}>
              {children}
            </FormElement>

            <div
              className="containerV buttons-container"
              style={{paddingTop: "5pt", alignItems: "stretch"}}>
              {buttons}
            </div>
          </Card>

          {footer}
        </div>
      </div>
    </DynamicProjectThemeProvider>
  ) : null;
};
