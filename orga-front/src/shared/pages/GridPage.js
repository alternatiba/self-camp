import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {Input, List} from "antd";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {paginationPageSizes} from "@features/view";
import {useTranslation} from "react-i18next";
import {PageHeading} from "../components/PageHeading";
import {useDebounce} from "../hooks/useDebounce";
import {searchInObjectsList} from "@shared/utils/searchInObjectsList";
import {useSavedPagination} from "../hooks/useSavedPagination";
import {useStickyShadow} from "../hooks/useStickyShadow";

const {Search} = Input;

export function GridPage({
  title,
  icon,
  subtitle,
  customButtons,
  renderItem,
  header = true,
  fullPage = true,
  searchInFields,
  dataSource,
  customControls,
}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [pagination, setPagination] = useSavedPagination(dispatch, title);
  const {isMobileView} = useWindowDimensions();
  const [searchText, setSearchText] = useState("");

  const filteredDataSource = searchInObjectsList(searchText, dataSource, searchInFields);

  const debouncedSearch = useDebounce(setSearchText, 500);

  useStickyShadow();

  return (
    <>
      <div
        className={`containerV sticky-element ${fullPage ? "full-width-content" : ""}`}
        style={{position: "sticky", top: 0, zIndex: 4}}>
        {header && (
          <PageHeading
            className="list-page-header"
            icon={icon}
            title={title}
            customButtons={customButtons}
          />
        )}
        {(subtitle || searchInFields) && (
          <div className="subtitle with-margins" style={{marginBottom: 20}}>
            {subtitle}
            <div className="containerH buttons-container">
              {searchInFields && (
                <Search
                  placeholder={t("common:search")}
                  style={{minWidth: 200, flexBasis: "40%"}}
                  onChange={(e) => debouncedSearch(e.target.value)}
                  enterButton
                />
              )}
              {customControls}
            </div>
          </div>
        )}
      </div>
      <List
        style={{paddingTop: 10, marginBottom: 40}}
        grid={{gutter: 16, xs: 1, sm: 1, md: 2, lg: 2, xl: 3, xxl: 4}}
        dataSource={filteredDataSource}
        renderItem={renderItem}
        pagination={{
          position: "bottom",
          style: {textAlign: "center"},
          size: isMobileView && "small",
          showSizeChanger: true,
          onChange: setPagination,
          current: pagination.current,
          pageSize: pagination.pageSize,
          pageSizeOptions: paginationPageSizes,
        }}
      />
    </>
  );
}
