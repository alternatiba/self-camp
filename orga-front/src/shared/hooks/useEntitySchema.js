import {useEffect, useState} from "react";
import {useSelector} from "react-redux";

import {useMatch} from "react-router-dom";
import {currentProjectSelectors} from "@features/currentProject";
import {fetchWithMessages} from "@shared/utils/api/fetchWithMessages";

export const useEntitySchema = ({filterSchema, forceEndpoint, forceSchema}) => {
  const [entitySchema, setEntitySchema] = useState([]);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const {params} = useMatch("/:projectId/:endpoint");
  const endpoint = forceEndpoint || params.endpoint;

  useEffect(() => {
    if (forceSchema) {
      setEntitySchema(forceSchema);
    } else {
      fetchWithMessages(`projects/${currentProject._id}/${endpoint}/schema`, {
        method: "GET",
      }).then((schema) => setEntitySchema(schema.filter(filterSchema || (() => true))));
    }
  }, [currentProject._id, endpoint, filterSchema, forceSchema]);

  return [entitySchema, params.endpoint];
};
