import {instanceName} from "@app/configuration";
import {useTranslation} from "react-i18next";

export const useNOEInstanceName = (mode: "inscriptionFront" | "orgaFront") => {
  const {t} = useTranslation();
  const orgaSuffix = mode === "orgaFront" ? ` ${t("common:orga").toUpperCase()}` : "";
  return `${instanceName || t("common:noe")}${orgaSuffix}`;
};
