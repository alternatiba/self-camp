import {sorter} from "@shared/utils/sorters";
import {useTranslation} from "react-i18next";

export const useStewardsColumns = () => {
  const {t} = useTranslation();
  return [
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      sorter: (a, b) => sorter.text(a.firstName, b.firstName),
      defaultSortOrder: "ascend",
      searchable: true,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      sorter: (a, b) => sorter.text(a.lastName, b.lastName),
      searchable: true,
    },
  ];
};
