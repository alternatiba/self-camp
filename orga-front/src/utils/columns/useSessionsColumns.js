import {Link} from "react-router-dom";
import {Tag} from "antd";
import {editableTagsColumn} from "./editableTagsColumn";
import {displayInconsistencies, sessionsActions, sessionsSelectors} from "@features/sessions";
import {SessionFilling} from "@routes/sessions/atoms/SessionFilling";
import {getSessionSubscription} from "../registrationsUtilities";
import {getCategoryTitle} from "../features/getCategoryTitle";
import {GroupSmsButtons, usePhoneFormCompsInfo} from "@shared/components/buttons/GroupSmsButtons";
import {currentProjectSelectors} from "@features/currentProject";
import {useSelector} from "react-redux";
import {sorter} from "@shared/utils/sorters";
import {editableCellColumn} from "@shared/components/EditableCell";
import {useTranslation} from "react-i18next";
import {editableEntitiesColumn} from "./editableEntitiesColumn";
import {personName} from "@shared/utils/utilities";
import {stewardsActions, stewardsSelectors} from "@features/stewards";
import StewardEdit from "@routes/stewards/StewardEdit";
import {placesActions, placesSelectors} from "@features/places";
import PlaceEdit from "@routes/places/PlaceEdit";
import {teamsActions, teamsSelectors} from "@features/teams";
import TeamEdit from "@routes/teams/TeamEdit";
import {activitiesActions, activitiesSelectors} from "@features/activities";
import ActivityEdit from "@routes/activities/ActivityEdit";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";

import {dateFormatter} from "@shared/utils/formatters";

export const useSessionsColumns = (
  path,
  showPlaces,
  showTeams,
  byDate = false,
  simpleMode = false,
  registrations,
  showSms = false
) => {
  const {t, i18n} = useTranslation();
  const phoneFormComps = usePhoneFormCompsInfo();
  const currentProjectId = useSelector((state) => currentProjectSelectors.selectProject(state)._id);

  return [
    {
      title: t("categories:label"),
      dataIndex: "category",
      sorter: (a, b) => sorter.text(getCategoryTitle(a), getCategoryTitle(b)),
      render: (text, record) => (
        <Link to={`${path}/categories/${record.activity?.category?._id}`}>
          <Tag
            style={{
              cursor: "pointer",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
              maxWidth: 125,
            }}
            color={record.activity?.category?.color}>
            {getCategoryTitle(record)}
          </Tag>
        </Link>
      ),
      searchable: true,
      width: 140,
      ellipsis: true,
      searchText: getCategoryTitle,
    },
    editableEntitiesColumn({
      title: t("activities:label"),
      dataIndex: "activity",
      endpoint: "activities",
      getEntityLabel: (activity) => activity.name,
      elementsActions: sessionsActions,
      entityElementsActions: activitiesActions,
      entityElementsSelectors: activitiesSelectors,
      ElementEdit: ActivityEdit,
    }),
    {
      ...editableCellColumn({
        title: t("sessions:schema.name.label"),
        dataIndex: "name",
        type: "text",
        placeholder: t("common:schema.name.placeholder"),
        elementsActions: sessionsActions,
      }),
      sorter: (a, b) => sorter.text(a.name, b.name),
      searchable: true,
    },
    {
      title: t("common:availability.startEnd"),
      dataIndex: "start",
      sorter: (a, b) => sorter.date(a.start, b.start),
      render: (text, record) =>
        dateFormatter.longDateTimeRange(record.start, record.end, {short: true}),
      defaultSortOrder: byDate && "ascend",
      width: i18n.language === "fr" ? 200 : 225,
      searchable: true,
      searchText: (record) =>
        dateFormatter.longDateTimeRange(record.start, record.end, {short: true}),
    },
    editableEntitiesColumn({
      title: t("stewards:label_other"),
      dataIndex: "stewards",
      elementsActions: sessionsActions,
      mode: "multiple",
      getEntityLabel: personName,
      beforeChange: (newRecord) => displayInconsistencies(newRecord, currentProjectId),
      entityElementsActions: stewardsActions,
      entityElementsSelectors: stewardsSelectors,
      ElementEdit: StewardEdit,
      simpleMode,
      path,
    }),
    showPlaces &&
      editableEntitiesColumn({
        title: t("places:label_other"),
        dataIndex: "places",
        elementsActions: sessionsActions,
        mode: "multiple",
        getEntityLabel: (el) => el.name,
        beforeChange: (newRecord) => displayInconsistencies(newRecord, currentProjectId),
        entityElementsActions: placesActions,
        entityElementsSelectors: placesSelectors,
        ElementEdit: PlaceEdit,
        simpleMode,
        path,
      }),
    showTeams &&
      editableEntitiesColumn({
        title: t("teams:label"),
        dataIndex: "team",
        endpoint: "teams",
        elementsActions: sessionsActions,
        getEntityLabel: (el) => el.name,
        entityElementsActions: teamsActions,
        entityElementsSelectors: teamsSelectors,
        ElementEdit: TeamEdit,
        simpleMode,
        path,
      }),
    editableTagsColumn({
      title: t("common:schema.tags.label"),
      dataIndex: "tags",
      elementsActions: sessionsActions,
      elementsSelectors: sessionsSelectors,
    }),
    ...useCustomFieldsColumns({
      endpoint: "sessions",
      elementsActions: sessionsActions,
    }),
    {
      ...editableCellColumn({
        title: t("activities:schema.maxNumberOfParticipants.label_short"),
        dataIndex: "maxNumberOfParticipants",
        type: "number",
        placeholder: t("activities:schema.maxNumberOfParticipants.placeholder"),
        elementsActions: sessionsActions,
      }),
      sorter: (a, b) => sorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
      width: 165,
    },
    {
      ...editableCellColumn({
        title: t("activities:schema.volunteeringCoefficient.label"),
        dataIndex: "volunteeringCoefficient",
        type: "number",
        placeholder: t("activities:schema.volunteeringCoefficient.placeholder"),
        elementsActions: sessionsActions,
      }),
      sorter: (a, b) => sorter.number(a.volunteeringCoefficient, b.volunteeringCoefficient),
      width: 200,
    },
    {
      ...editableCellColumn(
        {
          title: t("sessions:filling.label"),
          editableValueTitle: t("sessions:schema.maxNumberOfParticipants.labelPrecise"),
          dataIndex: "filling",
          name: "maxNumberOfParticipants",
          type: "number",
          placeholder: "max px.",
          minMaxNumber: {min: 0},
          elementsActions: sessionsActions,
        },
        {
          simpleDisplayCellRender: (record) =>
            record.everybodyIsSubscribed ? (
              t("sessions:filling.everybodyIsSubscribedOrga")
            ) : (
              <SessionFilling
                computedMaxNumberOfParticipants={record.computedMaxNumberOfParticipants}
                numberOfParticipants={record.numberParticipants}
                showLabel={false}
              />
            ),
        }
      ),
      sorter: (a, b) => {
        const maxA = a.computedMaxNumberOfParticipants;
        const maxB = b.computedMaxNumberOfParticipants;
        // Hierarchy: everybodyIsSubscribed > free registration > given number of participants > no participants at all
        return sorter.number(
          !b.everybodyIsSubscribed && maxA !== undefined && maxA !== 0
            ? a.numberParticipants / maxA
            : -1,
          !a.everybodyIsSubscribed && maxB !== undefined && maxB !== 0
            ? b.numberParticipants / maxB
            : -1
        );
      },
      width: 150,
    },
    registrations && {
      title: t("sessions:arrivals.label"),
      dataIndex: "allArrived",
      render: (text, record) => {
        const numberOfArrivedParticipants = record.everybodyIsSubscribed
          ? registrations.filter((r) => r.hasCheckedIn).length
          : registrations.filter((r) => r.hasCheckedIn && getSessionSubscription(r, record))
              ?.length;
        const numberOfParticipants = record.everybodyIsSubscribed
          ? registrations.length
          : record.numberParticipants;
        const color = record.everybodyIsSubscribed
          ? undefined
          : numberOfArrivedParticipants >= numberOfParticipants
          ? "green"
          : "red";
        const string = numberOfParticipants
          ? `${numberOfArrivedParticipants}/${numberOfParticipants}`
          : numberOfArrivedParticipants;

        return numberOfParticipants > 0 ? (
          <span
            title={t("sessions:arrivals.explanation", {
              numberOfParticipants,
              numberOfArrivedParticipants,
            })}
            style={{color}}>
            {string}
          </span>
        ) : (
          ""
        );
      },
      width: 95,
    },
    showSms &&
      phoneFormComps.length > 0 && {
        dataIndex: "sms",
        width: 45,
        render: (text, record) => {
          const registrationsForSession = registrations.filter((r) =>
            getSessionSubscription(r, record)
          );
          return <GroupSmsButtons registrations={registrationsForSession} session={record} />;
        },
      },
  ].filter((el) => !!el);
};
