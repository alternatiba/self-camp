import {EntitySelectors as ReduxEntitySelectors} from "@reduxjs/toolkit";
import {DefaultRootState} from "react-redux";

export type EntityBase = {_id: string};

export type LoadListParams = {forceLoad?: boolean; silent?: boolean};

export type EntitiesSelectors<T extends EntityBase, TLight extends EntityBase> = {
  selectEditing: (state: DefaultRootState) => T;
  selectList: ReduxEntitySelectors<TLight, DefaultRootState>["selectAll"];
  selectById: ReduxEntitySelectors<TLight, DefaultRootState>["selectEntities"];
  selectIsLoaded: (state: DefaultRootState) => boolean;
};
