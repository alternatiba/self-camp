module.exports = {
  async up(db, client) {
    await db.collection("projects").updateMany({locale: {$exists: false}}, {$set: {locale: "fr"}});
  },

  async down(db, client) {},
};
