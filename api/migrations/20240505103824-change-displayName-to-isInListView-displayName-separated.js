const mapAllComponents = (components, mapFn) =>
  components.map((comp) => {
    comp = mapFn(comp);
    if (comp.components?.length > 0) {
      comp.components = mapAllComponents(comp.components, mapFn);
    }
    return comp;
  });

module.exports = {
  async up(db, client) {
    const upFunction = (comp) => {
      const {displayName, inListView, ...restComp} = comp;
      if (displayName && displayName !== "") {
        return {displayName, inListView: true, ...restComp};
      } else {
        return {displayName, ...restComp};
      }
    };

    const projects = await db
      .collection("projects")
      .find({formComponents: {$exists: true}})
      .toArray();
    console.log(`${projects.length} projects`);

    for (const project of projects) {
      console.log(project.formComponents);
      const newComps = mapAllComponents(project.formComponents, upFunction);
      await db.collection("projects").updateOne(
        {_id: project._id},
        {
          $set: {formComponents: newComps},
        }
      );
    }
  },

  async down(db, client) {
    const downFunction = (comp) => {
      const {displayName, inListView, ...restComp} = comp;
      if (displayName && displayName !== "") {
        return {displayName, ...restComp};
      } else {
        return comp;
      }
    };

    const projects = await db
      .collection("projects")
      .find({formComponents: {$exists: true}})
      .toArray();
    console.log(`${projects.length} projects`);

    for (const project of projects) {
      const newComps = mapAllComponents(project.formComponents, downFunction);
      await db.collection("projects").updateOne(
        {_id: project._id},
        {
          $set: {formComponents: newComps},
        }
      );
    }
  },
};
