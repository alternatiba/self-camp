import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "@config/mongoose";

import {lightSelection} from "@utils/lightSelection";
import {SchemaDescription} from "@utils/routeUtilities";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {ActivityD} from "./activity.model";
import {ProjectD} from "./project.model";
import {RegistrationD} from "./registration.model";
import {SessionD} from "./session.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export type TeamD = Document & {
  name?: string;
  summary?: string;
  description?: string;
  notes?: string;
  activity?: ActivityD;

  customFields?: Record<string, any>;

  project: ProjectD;
  deletedAt?: Date;
  importedId?: string;
};

export type LightTeamD = Pick<TeamD, keyof typeof lightSelection.team>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const TeamSchema = new Schema<TeamD>(
  {
    name: String,
    summary: String,
    description: String,
    notes: String,
    activity: {type: SchemaTypes.ObjectId, ref: "Activity"},

    customFields: Object,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
TeamSchema.plugin(softDelete);
TeamSchema.plugin(addDiffHistory);
TeamSchema.plugin(indexByIdAndProject);
TeamSchema.plugin(preventDuplicatesInProject, [{name: 1, activity: 1}]); // We can have same names but not if the linked activity is the same

/**
 * MODEL
 */
export const Team = model<TeamD>("Team", TeamSchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription<
  TeamD & {sessions: Array<SessionD>; registrations: Array<RegistrationD>}
> = [
  {key: "name", label: "Nom de l'équipe"},
  {key: "activity", label: "Activité liée"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Informations"},
  {key: "description", label: "Description détaillée"},
  {key: "notes", label: "Notes privées pour les orgas"},

  // Those are not proper arguments, but can be given and will modify the appropriate objects accordingly
  {key: "sessions", label: "Sessions", isArray: true},
  {key: "registrations", label: "Participant⋅es", isArray: true},
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = schemaDescription.map((field) => field.key);
