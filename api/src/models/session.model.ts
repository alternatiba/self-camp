import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "@config/mongoose";
import {SchemaDescription} from "@utils/routeUtilities";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {ActivityD} from "./activity.model";
import {PlaceD} from "./place.model";
import {ProjectD} from "./project.model";
import {Registration, RegistrationD} from "./registration.model";
import {SlotD} from "./slot.model";
import {StewardD} from "./steward.model";
import {TeamD} from "./team.model";
import {log} from "console";

/**************************************************************
 *            TYPES
 **************************************************************/

/**
 * DEFINITION
 */
export type SessionD = Document & {
  activity: ActivityD;
  slots: Array<SlotD>;
  start: Date;
  end: Date;

  name?: string;

  notes?: string;
  tags?: Array<string>;
  everybodyIsSubscribed?: boolean;
  maxNumberOfParticipants?: number;
  volunteeringCoefficient?: number;
  stewards?: Array<StewardD>;
  places?: Array<PlaceD>;
  team?: TeamD;

  isScheduleFrozen?: boolean;

  customFields?: Record<string, any>;

  //*** Fields calculated on demand: they do not always exist and are not stored in database ***//

  participants?: Array<RegistrationD>;
  numberParticipants?: number;

  computedMaxNumberOfParticipants?: number;

  sameTimeSessions?: Array<SessionD>;

  project: ProjectD;
  deletedAt?: Date;
  importedId?: string;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const SessionSchema = new Schema<SessionD>(
  {
    name: String,
    activity: {type: SchemaTypes.ObjectId, ref: "Activity", required: true},
    start: {type: Date, required: true},
    end: {type: Date, required: true},
    slots: {type: [{type: SchemaTypes.ObjectId, ref: "Slot"}], required: true},
    notes: String,
    tags: [String],
    everybodyIsSubscribed: Boolean,
    maxNumberOfParticipants: Number,
    volunteeringCoefficient: {type: Number, required: false},
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
    isScheduleFrozen: {type: Boolean, default: false},

    customFields: Object,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true, toObject: {virtuals: true}, toJSON: {virtuals: true}}
);

/**
 * POPULATED VIRTUALS
 */
// The max number of participants computed based on the places, the activity and the session data
SessionSchema.virtual("computedMaxNumberOfParticipants").get(function (): number {
  return sessionMethods.getMaxParticipants(this);
});

/**
 * INSTANCE METHODS
 */

// This function is duplicated in orga-front/src/helpers/sessionUtilities.js
function getMaxParticipantsBasedOnPlaces(session: SessionD): number {
  return session.places?.length > 0
    ? session?.places?.reduce((acc, place) => acc + (place.maxNumberOfParticipants ?? Infinity), 0)
    : Infinity;
}

// Warning: Infinity is transmitted as "null" in JSON, so if it's "null" in the frontends, it's like infinity
function getMaxParticipants(session: SessionD): number | undefined {
  // Compute the jauge based on th session places
  const sessionMax = session.maxNumberOfParticipants;
  const activityMax = session.activity?.maxNumberOfParticipants;

  const maxParticipantsBasedOnPlaces = sessionMethods.getMaxParticipantsBasedOnPlaces(session);
  if (sessionMax !== undefined && sessionMax !== null) {
    return maxParticipantsBasedOnPlaces !== undefined
      ? Math.min(sessionMax, maxParticipantsBasedOnPlaces)
      : sessionMax;
  } else if (activityMax !== undefined && activityMax !== null) {
    return maxParticipantsBasedOnPlaces !== undefined
      ? Math.min(activityMax, maxParticipantsBasedOnPlaces)
      : activityMax;
  } else {
    return maxParticipantsBasedOnPlaces;
  }
}

function getSessionVolunteeringCoefficient(session: SessionD): number {
  return session.volunteeringCoefficient || session.volunteeringCoefficient === 0
    ? session.volunteeringCoefficient
    : session.activity.volunteeringCoefficient || 0;
}

async function getAllSessionsSlotsInProject(projectId: string) {
  const allSessions = await Session.find({project: {_id: projectId}})
    .select({_id: 1, stewards: 1, start: 1, end: 1})
    .populate([
      {path: "slots", select: {start: 1, end: 1}},
      {path: "activity", select: {name: 1, allowSameTime: 1}},
    ])
    .lean();
  return allSessions
    .map((session) =>
      session.slots.map((slot) => ({
        session,
        ...slot,
        slots: undefined, // Hide slots, it's not needed
      }))
    )
    .flat();
}

/**
 * Populates the session with its subscribed users, and adds also the number of participants
 * @param session the session to work on
 * @param includeParticipants if true, include the participants, if false don't, and just count the
 * number of participants without fetching the data.
 */
async function populateParticipants(session: SessionD, includeParticipants = true): Promise<void> {
  if (includeParticipants) {
    // If we include participants, fetch them entirely
    session.participants = await Registration.find(
      {sessionsSubscriptions: {$elemMatch: {session: session._id}}},
      "tags teamsSubscriptions sessionsSubscriptions formAnswers numberOfDaysOfPresence"
    ).populate([
      {path: "user", select: "firstName lastName email"},
      {path: "teamsSubscriptions.team", select: "name"},
    ]);
    session.numberParticipants = session.participants.length;
  } else {
    // Else, just count the project registrations, that's enough
    session.numberParticipants = await Registration.countDocuments({
      sessionsSubscriptions: {$elemMatch: {session: session._id}},
    });
  }
}

/**
 * Computes the sessions that occur in the same time asa the current session. It will be useful, so we can then
 * calculate in the frontends which session is potentially in conflict with another
 * @param session the session to work on
 * @param registration the current registration, containing a list of subscribed sessions by a user
 * @param allSessionsSlots the project slots. They are given for performance improvement reasons, but can
 * also be calculated if not given.
 */
async function computeSameTimeSessions(
  session: SessionD,
  registration?: RegistrationD,
  allSessionsSlots?: Awaited<ReturnType<typeof sessionMethods.getAllSessionsSlotsInProject>>
): Promise<void> {
  // If not given, calculate the project slots
  if (!allSessionsSlots) {
    const projectId = session.project?._id || session.project;
    allSessionsSlots = await sessionMethods.getAllSessionsSlotsInProject(projectId.toString());
  }

  // DEPRECATED: will be removed in the next versions
  const registeredSessionIds =
    registration?.sessionsSubscriptions.map((ss) => ss.session.toString()) || [];
  // DEPRECATED: end

  const sameTimeSlots = session.slots.flatMap((currentSlot) => {
    const sameTimeSlots = allSessionsSlots.filter(
      (slot) =>
        // Check if the activity of the slot allows same time sessions
        slot.session.activity.allowSameTime !== true &&
        // Check if the slots overlap
        slot.start < currentSlot.end &&
        slot.end > currentSlot.start &&
        // Check if the session is not the current session
        slot.session._id.toString() !== session._id.toString() &&
        // Check if the slot is not the current slot
        slot._id != currentSlot._id
    );
    return sameTimeSlots;
  });

  session.sameTimeSessions = sameTimeSlots.reduce((acc, sameTimeSlot) => {
    const sameTimeSession = sameTimeSlot.session;

    // Check if the same time session already exists in the accumulator.
    // If yes, add the slot to the existing same time session
    // Else, create a new same time session in the accumulator
    const existingSameTimeSession = acc.find(
      (session) => session.sameTimeSession._id == sameTimeSession._id
    );
    if (!existingSameTimeSession) {
      acc.push({
        sameTimeSession: {
          _id: sameTimeSession._id,
          activity: {name: sameTimeSession.activity.name},
          start: sameTimeSession.start,
          end: sameTimeSession.end,
        },
        // DEPRECATED: will be removed in the next versions
        sameTimeSessionRegistered:
          registeredSessionIds.includes(sameTimeSession._id.toString()) || undefined,
        // DEPRECATED: end
        sameTimeSessionSteward: registration?.steward
          ? sameTimeSession.stewards.some(
              (steward) => steward.toString() === registration.steward._id.toString()
            )
          : undefined,
      });
    }
    return acc;
  }, []);
}

export const sessionMethods = {
  getMaxParticipants,
  getMaxParticipantsBasedOnPlaces,
  getSessionVolunteeringCoefficient,
  getAllSessionsSlotsInProject,
  populateParticipants,
  computeSameTimeSessions,
} as const;

/**
 * PLUGINS
 */
SessionSchema.plugin(softDelete);
SessionSchema.plugin(addDiffHistory);
SessionSchema.plugin(indexByIdAndProject);
SessionSchema.plugin(preventDuplicatesInProject);

/**
 * MODEL
 */
export const Session = model<SessionD>("Session", SessionSchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription<
  SessionD & {registrations?: Array<RegistrationD>}
> = [
  {key: "name", label: "Nom de la session"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags", isArray: true},
  {key: "everybodyIsSubscribed", label: "Inscrire tout le monde"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de participant⋅es"},
  {key: "volunteeringCoefficient", label: "Coefficient de bénévolat"},
  {key: "stewards", label: "Encadrant⋅es", isArray: true},
  {key: "places", label: "Espaces", usePlaces: true, isArray: true},
  {key: "activity", label: "Activité"},
  {key: "team", label: "Équipe", useTeams: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "slots", label: "Plages horaires", isArray: true},
  {key: "registrations", label: "Participant⋅es", isArray: true},
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = schemaDescription.map((field) => field.key);
