import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "@config/mongoose";
import {lightSelection} from "@utils/lightSelection";
import {SchemaDescription} from "@utils/routeUtilities";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import {ProjectD} from "./project.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export type PlaceD = Document & {
  name: string;
  summary?: string;
  notes?: string;
  availabilitySlots?: Array<AvailabilitySlotD>;
  maxNumberOfParticipants?: number;

  customFields?: Record<string, any>;

  project: ProjectD;
  deletedAt?: Date;
  importedId?: string;
};

export type LightPlaceD = Pick<PlaceD, keyof typeof lightSelection.place>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const PlaceSchema = new Schema<PlaceD>(
  {
    name: {type: String, required: true},
    summary: String,
    notes: String,
    availabilitySlots: [AvailabilitySlotSchema],
    maxNumberOfParticipants: Number,

    customFields: Object,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
PlaceSchema.plugin(softDelete);
PlaceSchema.plugin(addDiffHistory);
PlaceSchema.plugin(indexByIdAndProject);
PlaceSchema.plugin(preventDuplicatesInProject, [{name: 1}]);

/**
 * MODEL
 */
export const Place = model<PlaceD>("Place", PlaceSchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription<PlaceD> = [
  {key: "name", label: "Nom de l'espace", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Informations"},
  {key: "maxNumberOfParticipants", label: "Nombre maximum de personnes"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "availabilitySlots", label: "Disponibilités"},
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = schemaDescription.map((field) => field.key);
