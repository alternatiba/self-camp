import {addDiffHistory, Lean, preventDuplicatesInProject, softDelete} from "@config/mongoose";
import {SchemaDescription} from "@utils/routeUtilities";
import moment from "moment";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import {Project, ProjectD} from "./project.model";
import {Session, SessionD, sessionMethods} from "./session.model";
import {StewardD} from "./steward.model";
import {TeamD} from "./team.model";
import {HelloAssoTicket, Ticket} from "./ticketing.model";
import {UserD} from "./user.model";

/**************************************************************
 *            TYPES
 **************************************************************/

type DaysOfPresence = {
  start: string;
  end: string;
  breakfast: boolean;
  lunch: boolean;
  dinner: boolean;
  projectDay: boolean;
};

export type VolunteeringTime = {real: number; weighted: number};

export type SessionSubscriptionD = Document & {
  subscribedBy: UserD;
  session: SessionD;
  hasNotShownUp?: boolean | UserD;
  team?: TeamD; // If filled, it means that the session subscription has been created because of the team registration
};
const SessionSubscriptionSchema = new Schema<SessionSubscriptionD>(
  {
    subscribedBy: {type: SchemaTypes.ObjectId, ref: "User"},
    session: {type: SchemaTypes.ObjectId, ref: "Session", required: true},
    hasNotShownUp: {type: SchemaTypes.ObjectId, ref: "User"}, // Save the ref of the user who checked the person as checkedIn
    // Redundant info to tell that the session subscription was subscribed
    // either on its own (no team), or subscribed via a team subscription.
    team: {type: SchemaTypes.ObjectId, ref: "Team"},
  },
  {timestamps: true}
);

type TeamSubscriptionD = Document & {
  subscribedBy: UserD;
  team: TeamD;
};
const TeamSubscriptionSchema = new Schema<TeamSubscriptionD>(
  {
    subscribedBy: {type: SchemaTypes.ObjectId, ref: "User"},
    team: {type: SchemaTypes.ObjectId, ref: "Team", required: true},
  },
  {timestamps: true}
);

/**
 * DEFINITION
 */
export type RegistrationD = Document & {
  // Entities
  user: UserD;
  steward?: StewardD;

  // Project role
  role?: string;

  // Invitation data
  invitationToken?: string;

  // Registration
  booked?: boolean;
  formAnswers?: Record<string, unknown>;
  sessionsSubscriptions?: Array<SessionSubscriptionD>;
  teamsSubscriptions?: Array<TeamSubscriptionD>;
  availabilitySlots?: Array<AvailabilitySlotD>;
  hasCheckedIn?: boolean;

  // Fields for the orga team
  notes?: string;
  tags?: Array<string>;

  // Ticketing
  helloAssoTickets?: Array<HelloAssoTicket>;
  customTicketingTickets?: Array<Ticket>;

  // Tells if the registration is hidden from public, and is only synced to the project
  hidden?: boolean;

  // Computed stuff
  numberOfDaysOfPresence?: number;
  daysOfPresence?: Array<DaysOfPresence>;

  customFields?: Record<string, any>;

  project: ProjectD;
  deletedAt?: Date;
  importedId?: string;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const RegistrationSchema = new Schema<RegistrationD>(
  {
    user: {type: SchemaTypes.ObjectId, ref: "User", required: true},
    steward: {type: SchemaTypes.ObjectId, ref: "Steward"},

    role: String,

    invitationToken: String,

    booked: Boolean,
    formAnswers: Object,
    sessionsSubscriptions: [SessionSubscriptionSchema],
    teamsSubscriptions: [TeamSubscriptionSchema],
    availabilitySlots: [AvailabilitySlotSchema],
    hasCheckedIn: {type: Boolean, default: false},

    notes: String,
    tags: [String],

    helloAssoTickets: [Object],
    customTicketingTickets: [Object],

    hidden: Boolean,

    numberOfDaysOfPresence: Number,
    daysOfPresence: [Object],

    customFields: Object,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    deletedAt: Date,
  },
  {
    timestamps: true,
  }
);

/**
 * HOOKS
 */

// When saving or updating a registration, we need to update the days of presence
RegistrationSchema.pre(["save", "updateOne"], async function (next) {
  const registration = this as RegistrationD;
  await registrationMethods.computeDaysOfPresence(registration);
  next();
});

/**
 * INSTANCE METHODS
 */
export const registrationMethods = {
  /**
   * Calculates volunteering indicators for a registration.
   *
   * @param registration - The registration for which to calculate volunteering indicators.
   * @param prefetchedSessions - Optional array of pre-fetched sessions to optimize performance.
   *
   * @returns A promise that resolves to an object containing:
   *   - The original registration data
   *   - voluntaryCounter: A legacy indicator, calculated as weighted volunteering time divided by number of days of presence
   *   - volunteering: An object containing:
   *     - real: Total duration of all sessions the user is involved in (as participant or steward)
   *     - weighted: Weighted duration, taking into account volunteering coefficients
   *
   * Calculation process:
   * 1. Retrieve all relevant sessions (where user is participant or steward)
   * 2. For each session:
   *    a. Determine if the user is a steward
   *    b. Calculate volunteering coefficient based on steward status and session/activity properties
   *    c. Sum up real duration and weighted duration (duration * coefficient)
   * 3. Compute days of presence if not already available
   * 4. Calculate legacy voluntaryCounter (weighted time / days of presence)
   *
   * Note: The function handles edge cases such as division by zero for voluntaryCounter.
   */
  async withVoluntaryCounter(
    registration: RegistrationD,
    prefetchedSessions?: Array<SessionD>
  ): Promise<
    Lean<RegistrationD> & {
      voluntaryCounter: number; // TODO legacy: old volunteering counter
      volunteering: VolunteeringTime;
    }
  > {
    // The volunteeringCounter is an async calculus that cannot be done in a
    // Mongoose getter function (cause getters can't be async).

    // The steward can be undefined
    const registrationStewardId = registration.steward?._id.toString();
    const subscribedSessionsIds = registration.sessionsSubscriptions.map((ss) =>
      ss.session?.toString()
    );

    // TODO use the slots instead cause you can be steward only on one slot of a session
    const sessionsSubscriptions = prefetchedSessions
      ? prefetchedSessions.filter(
          (session) =>
            subscribedSessionsIds.includes(session._id) ||
            session.stewards?.includes(registrationStewardId)
        )
      : ((await Session.find(
          {$or: [{_id: {$in: subscribedSessionsIds}}, {stewards: registrationStewardId}]},
          "stewards activity slots volunteeringCoefficient"
        )
          .populate([
            {path: "activity", select: "stewardVolunteeringCoefficient volunteeringCoefficient"},
            {path: "slots", select: "duration"},
          ])
          .lean()) as Array<SessionD>);

    // TODO this doesn't take in account the case of un-synchronized slot stewards
    // Compute the weighted volunteering time in all session for the current user
    const volunteeringTime: VolunteeringTime = sessionsSubscriptions.reduce(
      (sumVolunteering, session) => {
        const isStewardForThisSession = session.stewards.includes(registrationStewardId);
        const volunteeringCoefficient = isStewardForThisSession
          ? session.activity.stewardVolunteeringCoefficient || 0
          : sessionMethods.getSessionVolunteeringCoefficient(session) || 0;
        const duration =
          session.slots.reduce((durationSum, slot) => durationSum + slot.duration, 0) || 0;

        return {
          real: sumVolunteering.real + duration,
          weighted: sumVolunteering.weighted + volunteeringCoefficient * duration,
        };
      },
      {real: 0, weighted: 0}
    );

    // Compute numberOfDaysOfPresence if not computed already
    if (
      (!registration.daysOfPresence || registration.daysOfPresence.length === 0) &&
      registration.availabilitySlots?.length > 0
    ) {
      await registrationMethods.computeDaysOfPresence(registration);
    }

    // To avoid division by 0, make the check before
    return {
      ...registration.toObject(),
      volunteering: volunteeringTime,
    };
  },

  /**
   * Calculates the days of presence for a given registration.
   *
   * The calculation is done as follows:
   * 1. If no availability slots are defined, the number of days of presence is 0.
   * 2. For each day between the first and last availability slot:
   *    a. Check if the user has slots on that day.
   *    b. If yes, determine if the user is present for breakfast, lunch, and dinner.
   *    c. Also check if this day corresponds to a project day.
   * 3. A day is counted as a day of presence if:
   *    - The user is present for lunch or dinner.
   *    - AND it's a project day.
   *
   * @param registration - The registration for which to calculate the days of presence
   * @returns A promise that resolves when the calculation is complete
   */
  async computeDaysOfPresence(registration: RegistrationD): Promise<void> {
    // If there are no availability slots, everything is equal to zero
    if (!registration.availabilitySlots || registration.availabilitySlots.length === 0) {
      registration.numberOfDaysOfPresence = 0;
      registration.daysOfPresence = [];
      return;
    }

    // Get the project
    // TODO this should be externalized out of the function
    const project = await Project.findByIdOrSlug(registration.project?._id || registration.project);

    // Get meals times from project
    const {breakfastTime, lunchTime, dinnerTime} = project;

    // Convert project dates to Moment dates
    const projectStart = moment(project.start);
    const projectEnd = moment(project.end);
    const projectSlots = project.availabilitySlots.map((as) => ({
      start: moment(as.start),
      end: moment(as.end),
    }));
    // TODO until here

    // If the project is a one-day event, adapt some behaviors
    const projectIsOneDayEvent = projectStart.isSame(projectEnd, "day");

    // Convert the registration slots to moment dates
    const registrationSlots = registration.availabilitySlots.map((as) => ({
      start: moment(as.start),
      end: moment(as.end),
    }));

    // Get the earliest and latest presence time
    const registrationStart: moment.Moment = registrationSlots.reduce(
      (acc: moment.Moment, slot) => (acc !== undefined ? moment.min(acc, slot.start) : slot.start),
      undefined
    );
    const registrationEnd: moment.Moment = registrationSlots.reduce(
      (acc: moment.Moment, slot) => (acc !== undefined ? moment.max(acc, slot.end) : slot.end),
      undefined
    );

    // Total days of presence of the user, no matter the project dates
    const totalDaysOfPresenceForRegistration =
      registrationEnd.clone().startOf("day").diff(registrationStart.clone().startOf("day"), "day") +
      1;

    // Compute the real days of presence on site: iterate through each day and get data
    const daysOfPresenceOnSite = [];
    for (let dayIndex = 0; dayIndex < totalDaysOfPresenceForRegistration; dayIndex++) {
      // Create the current day object
      const currentDayStart = registrationStart.clone().startOf("day");
      currentDayStart.add(dayIndex, "day");
      const currentDayEnd = currentDayStart.clone().endOf("day");

      // Get user availabilitySlots for this day (even if they are on multiple days, if the current days is inside, get it)
      let registrationSlotsThisDay = registrationSlots.filter(
        (slot) =>
          currentDayStart.diff(slot.start.clone().startOf("day"), "day") >= 0 &&
          currentDayStart.diff(slot.end.clone().startOf("day"), "day") <= 0
      );

      // If no slots on this day, don't add this day to the days of presence, and skip it
      // If there are slots on this day, compute the breakfastTime, lunchTime and dinnerTime and all the rest
      if (registrationSlotsThisDay.length > 0) {
        // Truncate multi-days slots with the begging and the end of the current day
        registrationSlotsThisDay = registrationSlotsThisDay.map((slot) => ({
          start: moment.max(currentDayStart.clone(), slot.start),
          end: moment.min(currentDayEnd.clone(), slot.end),
        }));

        const [userEatsForBreakfastThisDay, userEatsForLunchThisDay, userEatsForDinnerThisDay] = [
          breakfastTime,
          lunchTime,
          dinnerTime,
        ].map((mealTime) => {
          // Build the real lunchTime and dinnerTime dates for this current day
          const mealTimeThisDay = moment(mealTime);
          mealTimeThisDay.year(currentDayStart.year());
          mealTimeThisDay.month(currentDayStart.month());
          mealTimeThisDay.date(currentDayStart.date());

          // If the project ends before or start after the meal times, then it means the user will not eat this meal
          // TODO can ben externalized out of the map loop
          const projectEndsBeforeOrStartsAfterMeal =
            projectEnd <= mealTimeThisDay || projectStart >= mealTimeThisDay;

          // Tell if the user eats for lunch or dinner this current day
          return (
            // Don't rely on projectEndsBeforeOrStartsAfterMeal for one-day events, as this can lead to side-effects with projects having zero "open" days
            (!projectEndsBeforeOrStartsAfterMeal || projectIsOneDayEvent) &&
            !!registrationSlotsThisDay.find(
              (slot) => slot.start <= mealTimeThisDay && slot.end > mealTimeThisDay
            )
          );
        });

        const thereIsAProjectSlotOnThisDay = !!projectSlots.find(
          (slot) =>
            currentDayStart.diff(slot.start.startOf("day"), "day") >= 0 &&
            currentDayStart.diff(slot.end.startOf("day"), "day") <= 0
        );

        daysOfPresenceOnSite.push({
          start: registrationSlotsThisDay.reduce(
            (acc: moment.Moment, slot) => (acc ? moment.min(acc, slot.start) : slot.start),
            undefined
          ),
          end: registrationSlotsThisDay.reduce(
            (acc: moment.Moment, slot) => (acc ? moment.max(acc, slot.end) : slot.end),
            undefined
          ),
          breakfast: userEatsForBreakfastThisDay,
          lunch: userEatsForLunchThisDay,
          dinner: userEatsForDinnerThisDay,
          projectDay: thereIsAProjectSlotOnThisDay,
        });
      }
    }

    // A day of presence is a day when the participant eats at least for lunch or for dinner during a project day.
    // A day of presence out of the project days is not counted
    registration.numberOfDaysOfPresence = daysOfPresenceOnSite.filter(
      (p) => (p.lunch || p.dinner) && p.projectDay
    ).length;
    registration.daysOfPresence = daysOfPresenceOnSite.map((d) => ({
      ...d,
      start: d.start?.toISOString(),
      end: d.end?.toISOString(),
    }));
  },

  async unregister(registration: RegistrationD): Promise<void> {
    registration.set({
      booked: false,
      availabilitySlots: [],
      formAnswers: {},
      steward: undefined,
      sessionsSubscriptions: [],
      teamsSubscriptions: [],
      helloAssoTickets: [],
      customTicketingTickets: [],
    });
    await registration.save();
  },
};

/**
 * PLUGINS
 */
RegistrationSchema.plugin(softDelete);
RegistrationSchema.plugin(addDiffHistory, ["user"]);
RegistrationSchema.plugin(preventDuplicatesInProject, [{user: 1}]);

/**
 * MODEL
 */
export const Registration = model<RegistrationD>("Registration", RegistrationSchema);

/**
 * SCHEMA DESCRIPTIONS
 */
const baseSchemaDescription: SchemaDescription<RegistrationD> = [
  {key: "user", label: "Utilisateur·ice", noGroupEditing: true},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "steward", label: "Encadrant·e"},
  {key: "role", label: "Rôle", noGroupEditing: true},
  {key: "booked", label: "Inscription commencée", noGroupEditing: true},
  {key: "formAnswers", label: "Formulaire d'inscription"},
  {
    key: "sessionsSubscriptions",
    label: "Inscriptions aux sessions",
    noGroupEditing: true,
    isArray: true,
  },
  {
    key: "teamsSubscriptions",
    label: "Inscriptions aux équipes",
    noGroupEditing: true,
    isArray: true,
  },
  {key: "availabilitySlots", label: "Plages de disponibilité", noGroupEditing: true, isArray: true},
  {key: "hasCheckedIn", label: "Arrivé·e"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "tags", label: "Tags", isArray: true},
  {key: "hidden", label: "Caché·e", noGroupEditing: true},
  {key: "helloAssoTickets", label: "Tickets HelloAsso", noGroupEditing: true, isArray: true},
  {
    key: "customTicketingTickets",
    label: "Tickets billetterie",
    noGroupEditing: true,
    isArray: true,
  },
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = baseSchemaDescription.map((field) => field.key);

export const schemaDescription: SchemaDescription<RegistrationD> = [
  ...baseSchemaDescription,
  // Add the fields that are calculated on the fly
  {key: "numberOfDaysOfPresence", label: "Nombre de jours de présence", noGroupEditing: true},
];
