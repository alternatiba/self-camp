import {
  addDiffHistory,
  indexByIdAndProject,
  preventDuplicatesInProject,
  softDelete,
} from "@config/mongoose";
import {lightSelection} from "@utils/lightSelection";
import {SchemaDescription} from "@utils/routeUtilities";
import {Document, model, Schema, SchemaTypes} from "mongoose";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import {ProjectD} from "./project.model";

/**************************************************************
 *            TYPES
 **************************************************************/

export type StewardD = Document & {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  summary: string;
  notes: string;
  project: ProjectD;
  availabilitySlots: Array<AvailabilitySlotD>;

  customFields?: Record<string, any>;

  deletedAt?: Date;
  importedId?: string;
};

export type LightStewardD = Pick<StewardD, keyof typeof lightSelection.steward>;

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const StewardSchema = new Schema<StewardD>(
  {
    firstName: {type: String, required: true},
    lastName: String,
    phoneNumber: String,
    summary: String,
    notes: String,

    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    availabilitySlots: [AvailabilitySlotSchema],

    customFields: Object,

    deletedAt: Date,
    importedId: String,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
StewardSchema.plugin(softDelete);
StewardSchema.plugin(addDiffHistory);
StewardSchema.plugin(indexByIdAndProject);
StewardSchema.plugin(preventDuplicatesInProject, [{firstName: 1, lastName: 1}]); // Can't have two stewards with same name and last name in the same project

/**
 * MODEL
 */
export const Steward = model<StewardD>("Steward", StewardSchema);

/**
 * SCHEMA DESCRIPTION
 */
export const schemaDescription: SchemaDescription<StewardD> = [
  {key: "firstName", label: "Prénom", noGroupEditing: true},
  {key: "lastName", label: "Nom", noGroupEditing: true},
  {key: "phoneNumber", label: "Téléphone"},
  {key: "project", label: "Événement", noGroupEditing: true},
  {key: "summary", label: "Résumé"},
  {key: "notes", label: "Notes privées pour les orgas"},
  {key: "availabilitySlots", label: "Disponibilités", isArray: true},
  {key: "customFields", label: "Champs personnalisés", noGroupEditing: true},
];
export const allowedBodyArgs = schemaDescription.map((field) => field.key);
