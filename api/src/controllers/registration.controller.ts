import {isValidObjectId, Lean} from "@config/mongoose";
import {withAuthentication} from "@config/passport";
import {Project} from "@models/project.model";
import {
  allowedBodyArgs,
  Registration,
  RegistrationD,
  registrationMethods,
  schemaDescription,
} from "@models/registration.model";
import {Session, SessionD} from "@models/session.model";
import {LightStewardD} from "@models/steward.model";
import {LightTeamD} from "@models/team.model";
import {User, UserD} from "@models/user.model";
import {createEntity} from "@utils/controllers/actions/createEntity";
import {deleteEntity} from "@utils/controllers/actions/deleteEntity";
import {readEntity} from "@utils/controllers/actions/readEntity";
import {updateEntity} from "@utils/controllers/actions/updateEntity";
import {getDependenciesList} from "@utils/controllers/getDependenciesList";
import {GenericQueryParams, parseQueryParams} from "@utils/controllers/parseQueryParams";
import {filterRequestBody} from "@utils/controllers/requestValidation/filterRequestBody";
import {
  permit,
  projectAdmins,
  ROLES_LABELS,
  UserPermission,
} from "@utils/controllers/requestValidation/permit";
import {removeNestedObjectsContent} from "@utils/controllers/requestValidation/removeNestedObjectsContent";
import {
  isEmail,
  isMongoId,
  isString,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {lightSelection} from "@utils/lightSelection";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
  TypedRequestHandler,
} from "@utils/routeUtilities";
import {personName} from "@utils/utilities";
import {randomBytes} from "crypto";
import {Request, Response, Router} from "express";
import {body} from "express-validator/check";
import config from "../config/config";
import Mailer from "../services/mailer";

// If the user has no sufficient rights, clean the stuff that is not supposed to be visible to participants
const cleanOrgaStuffIfNeeded = (registration: Partial<RegistrationD>, authenticatedUser: UserD) => {
  if (!authenticatedUser?.registration?.role) {
    registration.notes = registration.tags = undefined;
  }
};

const userIsTheOnlyAdminOnCurrentProject = async (req: Request) =>
  (await Registration.countDocuments({
    project: req.params.projectId,
    role: "admin",
    user: {$ne: req.user._id},
  })) === 0;

// Custom permission to let proprietaries of the registration have full rights, but also permit admins or contributors if necessary
const registrationProprietaryOr = (permission: UserPermission): UserPermission => ({
  challenge: (user, request) =>
    // Either satisfy the permission, or being the proprietary of the project registration
    permission.challenge(user) || user.registration._id === request.params.id,
  errorReason: permission.errorReason,
});

export const RegistrationRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
RegistrationRouter.get(...MODIFICATIONS_HISTORY("Registration"));
RegistrationRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

type RegistrationPopulation = {
  user: UserD;
  steward: LightStewardD;
  teamsSubscriptions: Array<{team: LightTeamD}>;
};
const populateRegistrationOptions = [
  {path: "user", select: lightSelection.user, options: {lean: true}},
  {path: "steward", select: lightSelection.steward, options: {lean: true}},
  {path: "teamsSubscriptions.team", model: "Team", select: lightSelection.team},
];

type PopulatedRegistration = RegistrationD & RegistrationPopulation;

/**
 * List all the registrations in the project
 * GET /                                   (when you want to load everything no matter what)
 * POST /selectiveLoad  /!\ deprecated !!! (when you want to load only a part of the elements)
 *
 *   Body params:
 *     - alreadyLoaded: list of registration ids separated with a comma
 */
const LIST_REGISTRATIONS: TypedRequestHandler<
  ProjectId,
  {alreadyLoaded?: Array<string>},
  Array<PopulatedRegistration>,
  GenericQueryParams
> = async (req: Request, res: Response) => {
  const alreadyLoaded: Array<string> = req.body?.alreadyLoaded;
  const registrations = await Registration.find(
    {
      project: req.params.projectId,
      _id: {$not: {$in: alreadyLoaded}},
      ...parseQueryParams(req.query),
    },
    "-updatedAt -availabilitySlots._id -project -sessionsSubscriptions.updatedAt"
  )
    .populate([
      {path: "user", select: lightSelection.user, options: {lean: true}},
      {path: "steward", select: lightSelection.steward, options: {lean: true}},
      {
        path: "teamsSubscriptions.team",
        model: "Team",
        select: lightSelection.team,
        options: {lean: true},
      },
    ])
    .sort({updatedAt: "desc"});

  const projectSessions = await Session.find(
    {project: req.params.projectId},
    "stewards activity slots volunteeringCoefficient"
  )
    .populate([
      {path: "activity", select: "stewardVolunteeringCoefficient volunteeringCoefficient"},
      {path: "slots", select: "duration"},
    ])
    .lean();

  const preparedSessions = projectSessions.map((s) => ({
    stewards: s.stewards.map((steward) => steward._id.toString()),
    _id: s._id.toString(),
    activity: s.activity,
    slots: s.slots,
  }));

  const registrationsWithVoluntaryCounter = await Promise.all(
    registrations.map((r) =>
      registrationMethods.withVoluntaryCounter(r, preparedSessions as Array<SessionD>)
    )
  );

  return res.status(200).send(registrationsWithVoluntaryCounter);
};
RegistrationRouter.get(...endpoint("/", [withAuthentication], LIST_REGISTRATIONS));
RegistrationRouter.post(...endpoint("/selectiveLoad", [withAuthentication], LIST_REGISTRATIONS));

/**
 * Create a registration
 * POST /
 */
const CREATE_REGISTRATION = endpoint<ProjectId, New<RegistrationD>, PopulatedRegistration>(
  "/",
  [
    withAuthentication,
    filterRequestBody(allowedBodyArgs),
    removeNestedObjectsContent("project", "steward"),
    validateAndSanitizeRequest(
      // User can either be a plain user (when creating invitations)
      // or an id (when creating a registration for an existing user)
      body("user").exists(),
      isEmail("user.email").optional(),
      isString(["user.firstName", "user.lastName", "role"]).optional(),
      isMongoId("steward").optional(),
      isMongoId("project")
    ),
  ],
  async function (req, res) {
    // If a user full object is submitted:
    // - check if the email matches a known user.
    //   - If yes, replace the user with its id.
    //   - If not, then the user is not properly formatted.
    // - else, just pass
    if (typeof req.body.user !== "string") {
      if (req.body.user?.email) {
        const user = await User.findOne({email: req.body.user.email}, "_id").lean();
        if (user?._id) {
          req.body.user = user._id;
        }
      } else {
        return res.sendStatus(400);
      }
    }

    return await createEntity(Registration, req.body, res, async (registration) => {
      await registration.populate(populateRegistrationOptions);

      // If the role of a person is updated by another person, then the person whose role is updated receives an email.
      if (
        registration.role &&
        req.authenticatedUser._id.toString() !== registration.user._id.toString()
      ) {
        const {name: projectName, slug} = await Project.findByIdOrSlug(
          req.params.projectId,
          "name slug"
        ).lean();
        await Mailer.sendMail(
          "invitation.send_updated_role_notification",
          registration.user.email,
          {
            lng: registration.user.locale,
            fromPersonName: personName(req.authenticatedUser),
            projectName,
            role: ROLES_LABELS[registration.role].toLowerCase(),
            link: `${process.env.REACT_APP_ORGA_FRONT_URL}/${slug || req.params.projectId}`,
          }
        );
      }

      cleanOrgaStuffIfNeeded(registration, req.authenticatedUser);
      return await registrationMethods.withVoluntaryCounter(registration);
    });
  }
);
RegistrationRouter.post(...CREATE_REGISTRATION);

/**
 * Get a registration
 * GET /:id
 */
const GET_REGISTRATION = endpoint<ProjectIdAndId, undefined, PopulatedRegistration>(
  "/:id",
  [
    withAuthentication,
    // TODO check if that works
    // permit(registrationProprietaryOr(projectContributors)),
  ],

  async function (req, res) {
    // Replace particular cases with real ID
    if (req.params.id === "current") req.params.id = req.user.registration?._id.toString();

    return await readEntity(
      req.params.id,
      req.params.projectId,
      res,
      async (id) => Registration.findById(id).populate(populateRegistrationOptions),
      async (registration: RegistrationD) => {
        cleanOrgaStuffIfNeeded(registration, req.authenticatedUser);
        return await registrationMethods.withVoluntaryCounter(registration);
      }
    );
  }
);
RegistrationRouter.get(...GET_REGISTRATION);

/**
 * Get a registration by invitationToken
 * GET /:id
 */
const GET_REGISTRATION_BY_INVITATION_TOKEN = endpoint<
  ProjectId & {invitationToken: string},
  undefined,
  Lean<RegistrationD>
>("/byInvitationToken/:invitationToken", [], async function (req, res) {
  // Replace particular cases with real ID
  const registration = await Registration.findOne(
    {invitationToken: req.params.invitationToken, project: req.params.projectId},
    "role"
  )
    .populate(populateRegistrationOptions)
    .lean();
  return res.status(200).json(registration);
});
RegistrationRouter.get(...GET_REGISTRATION_BY_INVITATION_TOKEN);

/**
 * Modify a registration
 * PATCH /:id
 */
const MODIFY_REGISTRATION = endpoint<
  ProjectIdAndId,
  Modified<RegistrationD & {steward: string; user: string; project: string}>,
  RegistrationD | ErrorString
>(
  "/:id",
  [
    withAuthentication,
    // TODO make this work !
    // permit(registrationProprietaryOr(projectContributors)),
    filterRequestBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project", "steward", "user"),
    validateAndSanitizeRequest(isMongoId("user").optional(), isMongoId("project").optional()),
  ],
  async function (req, res) {
    const {role, ...registrationBody} = req.body;

    // If the steward is given, and is not "null" or a valid mongoId, then return
    if (
      registrationBody.steward !== undefined &&
      registrationBody.steward !== null &&
      !isValidObjectId(registrationBody.steward)
    )
      return res.status(400).send("Invalid steward ID");

    cleanOrgaStuffIfNeeded(registrationBody, req.authenticatedUser);

    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Registration,
      registrationBody,
      req.authenticatedUser,
      res,
      [
        {path: "user", select: lightSelection.user, options: {lean: true}},
        {path: "steward", select: lightSelection.steward, options: {lean: true}},
        {path: "teamsSubscriptions.team", model: "Team", select: "name"},
      ],
      async (registration: RegistrationD) => {
        let needSave = false;

        // Clean DB if steward = null or role = null
        if (registration.steward === null) {
          registration.steward = undefined;
          needSave = true;
        }

        // if availability slots have been submitted, save the document to trigger the updates.
        if (registrationBody.availabilitySlots) needSave = true;

        // If the person is modifying the roles, make some checks
        if (role !== undefined && role !== registration.role) {
          // Prevent non admin or superAdmin users to change rights
          if (
            req.authenticatedUser.registration.role !== "admin" &&
            !req.authenticatedUser.superAdmin
          ) {
            return res
              .status(403)
              .send("Vous ne pouvez pas modifier de droits si vous n'êtes pas administrateur⋅ice.");
          }

          // If the person is admin and wants to not be admin anymore, check that
          // there are other admins on the project so there is always at least one admin on the project
          if (
            req.user.registration._id.toString() === req.params.id && // ...Admin user is modifying its own registration...
            req.body.role !== "admin" && // ...and wants to change its role to something else than "admin"...
            (await userIsTheOnlyAdminOnCurrentProject(req)) // ...but is the only admin on the project
          ) {
            return res
              .status(403)
              .send(
                "Vous êtes le⋅la seul⋅e administrateur⋅ice du projet. Désignez un⋅e autre" +
                  " administrateur⋅ice pour pouvoir modifier votre inscription."
              );
          }

          const {name: projectName, slug} = await Project.findByIdOrSlug(req.params.projectId);
          const userIsModifyingItsOwnRegistration =
            req.authenticatedUser.registration._id.toString() === registration._id.toString();

          // Finally, update the role. When new roles are given, send an email (except if the user is modifying its own registration role)
          if (role === null) {
            // If it's null, it means the role has been removed.
            const oldRole = registration.role;
            registration.role = undefined;

            !userIsModifyingItsOwnRegistration &&
              (await Mailer.sendMail(
                "invitation.send_deleted_role_notification",
                registration.user.email,
                {
                  lng: registration.user.locale,
                  fromPersonName: personName(req.authenticatedUser),
                  projectName,
                  oldRole: ROLES_LABELS[oldRole].toLowerCase(),
                  link: `${process.env.REACT_APP_INSCRIPTION_FRONT_URL}/${
                    slug || req.params.projectId
                  }`,
                }
              ));
          } else {
            registration.role = role;

            !userIsModifyingItsOwnRegistration &&
              (await Mailer.sendMail(
                "invitation.send_updated_role_notification",
                registration.user.email,
                {
                  lng: registration.user.locale,
                  fromPersonName: personName(req.authenticatedUser),
                  projectName,
                  role: ROLES_LABELS[registration.role].toLowerCase(),
                  link: `${process.env.REACT_APP_ORGA_FRONT_URL}/${slug || req.params.projectId}`,
                }
              ));
          }
          needSave = true;
        }

        if (needSave) await registration.save();

        return await registrationMethods.withVoluntaryCounter(registration);
      }
    );
  }
);
RegistrationRouter.patch(...MODIFY_REGISTRATION);

/**
 * Tell if a participant has not shown up to a session to a session
 * post /:id/noShow/:sessionId
 */
const NO_SHOW_TO_SESSION = endpoint<
  ProjectIdAndId & {sessionId: string},
  {hasNotShownUp: boolean},
  RegistrationD
>(
  "/:id/noShow/:sessionId",
  [withAuthentication, validateAndSanitizeRequest(body("hasNotShownUp").isBoolean())],
  async function (req, res) {
    const sessionId = req.params.sessionId;
    const session = await Session.findById(sessionId, "stewards").populate("slots").lean();
    const hasRole = req.authenticatedUser.registration.role;
    const isStewardOnSession = !!session?.stewards.find(
      (stewardId) =>
        (stewardId.toString() as unknown as string) ===
        (req.authenticatedUser.registration.steward?.toString() as unknown as string)
    );

    if (!(hasRole || isStewardOnSession)) return res.sendStatus(401);

    const registration = await Registration.findOne({
      project: req.params.projectId,
      _id: req.params.id,
    }).populate([
      {path: "user", select: lightSelection.user, options: {lean: true}},
      {path: "steward", select: lightSelection.steward, options: {lean: true}},
      {path: "teamsSubscriptions.team", model: "Team", select: "name"},
    ]);

    const sessionSubscription = registration.sessionsSubscriptions.find(
      (ss) => ss.session.toString() === sessionId
    );
    sessionSubscription.hasNotShownUp = req.body.hasNotShownUp
      ? req.authenticatedUser._id
      : undefined;
    registration.save({__reason: "update()", __user: req.authenticatedUser._id});

    return res.status(200).send(registration);
  }
);
RegistrationRouter.post(...NO_SHOW_TO_SESSION);

/**
 * Delete a registration
 * DELETE /:id
 */
const DELETE_REGISTRATION = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(registrationProprietaryOr(projectAdmins))], // All users are admins from there
  async function (req, res) {
    // If the person is admin and wants to not be admin anymore, check that
    // there are other admins on the project so there is always at least one admin on the project
    if (
      req.user.registration._id.toString() === req.params.id && // Admin user wants to delete its own registration...
      (await userIsTheOnlyAdminOnCurrentProject(req)) // ...but is the only admin on the project
    ) {
      return res.status(403); // Can't allow this
    }

    // If the registration is only an invitation for a person that is not yet on the platform, also delete the user
    // WARNING : REGISTRATIONS ARE DELETED FOR REAL
    return await deleteEntity(
      req.params.id,
      Registration,
      req.authenticatedUser,
      res,
      async (deletedRegistration: RegistrationD) => {
        // Also try to delete the user if this was only an invitation or if the registration is hidden
        if (deletedRegistration.invitationToken || deletedRegistration.hidden) {
          // Check this user is not referenced at all elsewhere (in that case, don't delete the user)
          const linkedRegistrations = await getDependenciesList(Registration, {
            user: deletedRegistration.user,
          });
          // Delete *for real* the user related to this registration if there is no other linked registration
          if (!linkedRegistrations) await User.deleteOne({_id: deletedRegistration.user});
        }
      },
      undefined,
      true
    );
  }
);
RegistrationRouter.delete(...DELETE_REGISTRATION);

/**
 * Send an invitation email to somebody to allow them to collaborate on the project
 * POST /sendInvitationEmail
 */
const SEND_INVITATION_EMAIL = endpoint<
  ProjectId,
  {user: UserD; role?: string; steward?: string},
  RegistrationD
>(
  "/sendInvitationEmail",
  [
    permit(projectAdmins),
    validateAndSanitizeRequest(
      isEmail("user.email"),
      isString(["user.firstName", "user.lastName", "role"]).optional(),
      isMongoId("steward").optional()
    ),
  ],
  async function (req, res) {
    const invitedUserData = req.body.user;

    // If we find an existing *real* user (that has a password), we cancel the call
    const existingInvitedUser = await User.findOne({
      email: invitedUserData.email,
      password: {$exists: true}, // If a password exists, it means it's a real user account. Else, it's just an invitation.
    });
    // If the user exists, it is not possible to send an invitation. We must directly modify the registration itself
    if (existingInvitedUser) return res.sendStatus(403);

    // Generate a random token and save it in the user with an expiration date
    const invitationToken = (await randomBytes(32)).toString("hex");
    const newInvitedRegistration = await Registration.create({
      role: req.body.role,
      steward: req.body.steward,
      invitationToken,
      user:
        (await User.findOne({email: invitedUserData.email})) || // Check if a simili user already exists, even if that's a fake user
        (await User.create(invitedUserData)),
      project: req.params.projectId,
    });

    const project = await Project.findByIdOrSlug(req.params.projectId);

    const roleLabel =
      newInvitedRegistration.role && ROLES_LABELS[newInvitedRegistration.role].toLowerCase();
    const rootLinkUrl = roleLabel ? config.urls.orgaFront : config.urls.inscriptionFront;

    // Send the reset password email
    await Mailer.sendMail(
      "invitation.send_new_orga_invitation",
      newInvitedRegistration.user.email,
      {
        lng: existingInvitedUser?.locale || project.locale,
        fromPersonName: `${req.user.firstName} ${req.user.lastName}`,
        as: roleLabel
          ? newInvitedRegistration.steward
            ? ` en tant qu'encadrant⋅e et ${roleLabel}`
            : ` en tant que ${roleLabel}`
          : newInvitedRegistration.steward && " en tant qu'encadrant⋅e",
        projectName: project.name,
        link:
          `${rootLinkUrl}/${project.slug || project._id}/signup?` +
          `invitationToken=${newInvitedRegistration.invitationToken}`,
      }
    );

    return res.status(200).send(newInvitedRegistration);
  }
);
RegistrationRouter.post(...SEND_INVITATION_EMAIL);
