import {withAuthentication} from "@config/passport";
import {RegistrationD} from "@models/registration.model";
import {User, UserD} from "@models/user.model";
import {
  isEmail,
  validateAndSanitizeRequest,
} from "@utils/controllers/requestValidation/validateAndSanitizeRequest";
import {endpoint, ErrorString} from "@utils/routeUtilities";
import {compare as bcryptCompare} from "bcryptjs";
import {randomBytes} from "crypto";
import {Request, Response, Router} from "express";
import {body} from "express-validator/check";
import jwt from "jsonwebtoken";
import config from "../config/config";
import {default as Mailer} from "../services/mailer";

export const AuthRouter = Router();

/**
 * Tell if the user can authenticate or not (used for log in)
 * POST /authenticate
 */
const AUTHENTICATE = endpoint<
  undefined,
  {email: string; password: string},
  {jwt_token: string} | {error: ErrorString}
>(
  "/authenticate",
  [validateAndSanitizeRequest(isEmail("email"), body("password").exists())],
  async function (req, res) {
    // Get the user and select the password with it, because we will need it
    const user = await User.findOne({email: req.body.email}).select("+password");

    if (!user) return res.sendStatus(401); // User not found (but no 404 because we don't want the user to know it

    // Check password. User can have no password if the user has a pending invitation
    const passwordsAreEqual =
      user.password && (await bcryptCompare(req.body.password, user.password));
    if (!passwordsAreEqual) return res.status(401).json({error: "Incorrect email or password"});

    // Create JWT token
    const payload = {id: user._id};
    const token = jwt.sign(payload, config.jwt.secret, {expiresIn: `${config.jwt.expiresIn} days`});
    const jsonResponse = {jwt_token: token};
    return res.status(200).json(jsonResponse);
  }
);
AuthRouter.post(...AUTHENTICATE);

/**
 * Refresh the user's XSRF and JWT authentication tokens,
 * and returns users with registrations populated
 * GET /refreshAuthTokens
 */
const REFRESH_AUTH_TOKENS = endpoint<
  undefined,
  undefined,
  {
    jwt_token: string;
    user: UserD;
    authenticatedUser: UserD & {registrations: Array<Pick<RegistrationD, "project" | "role">>};
  }
>("/refreshAuthTokens", [withAuthentication], async function (req, res) {
  const payload = {id: req.authenticatedUser._id};
  const token = jwt.sign(payload, config.jwt.secret, {expiresIn: `${config.jwt.expiresIn} days`});

  return res.status(200).json({
    jwt_token: token,
    authenticatedUser: await req.authenticatedUser.populate({
      path: "registrations",
      select: "project role",
      options: {lean: true},
    }),
    user: req.user,
  });
});
AuthRouter.get(...REFRESH_AUTH_TOKENS);

/**
 * Send a password forgotten email link to the user
 * POST /password/sendResetEmail
 */
const SEND_RESET_PASSWORD_EMAIL = endpoint(
  "/password/sendResetEmail",
  [validateAndSanitizeRequest(isEmail("email"), body("url").exists())],
  async function (req, res) {
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.sendStatus(200); // User not found (but 200 because we don't want to let know if we found the email or not, for security reasons

    // Generate a random token and save it in the user with an expiration date
    const buf = await randomBytes(32);
    user.passwordResetToken = buf.toString("hex");
    user.passwordResetExpires = new Date(Date.now() + 3600000); // 1 hour
    await user.save();

    // Send the reset password email
    await Mailer.sendMail("auth.send_forgot_password", user.email, {
      lng: user.locale,
      link: req.body.url + "/resetpassword?token=" + user.passwordResetToken,
    });

    return res.sendStatus(200);
  }
);
AuthRouter.post(...SEND_RESET_PASSWORD_EMAIL);

/**
 * Check the password reset token from the client, to tell if it is allowed to change password
 * GET /password/checkResetToken/:token
 */
const CHECK_RESET_PASSWORD_TOKEN = endpoint<{token: string}, undefined, undefined>(
  "/password/checkResetToken/:token",
  [],
  async function checkPasswordResetToken(req: Request, res: Response) {
    // Get the user with the given token
    const user = await User.findOne({passwordResetToken: req.params.token})
      .where("passwordResetExpires")
      .gt(Date.now());

    // If no user with this token is found, return error
    if (!user) return res.sendStatus(401);

    // Else, token is valid
    return res.sendStatus(200);
  }
);
AuthRouter.get(...CHECK_RESET_PASSWORD_TOKEN);

/**
 * Validate the password reset
 * POST /password/reset
 */
const RESET_PASSWORD = endpoint<undefined, {token: string; password: string}, undefined>(
  "/password/reset",
  [validateAndSanitizeRequest(body("token").exists(), body("password").isLength({min: 8}))],
  async function (req, res) {
    // Get the user with the given token
    const user = await User.findOne({passwordResetToken: req.body.token})
      .where("passwordResetExpires")
      .gt(Date.now());

    // If no user with this token is found, return error
    if (!user) return res.sendStatus(401); // User not found (but no 404 because we don't want the user to know it

    // If valid, reset the token, and save the new password
    user.passwordResetToken = undefined;
    user.password = req.body.password;
    await user.save();

    // Send a password changed confirmation email
    await Mailer.sendMail("auth.send_reset_password", user.email, {
      lng: user.locale,
      email: user.email,
    });

    return res.sendStatus(200);
  }
);
AuthRouter.post(...RESET_PASSWORD);
