import {UserD} from "@models/user.model";
import {Response} from "express";
import {Document, Model} from "mongoose";
import {broadcastUpdate} from "../broadcastUpdate";

export const updateEntity = async <EntityD extends Document>(
  id: string,
  projectId: string,
  Entity: Model<EntityD>,
  args: any,
  updatedByUser: UserD,
  res: Response,
  populateOptions?: any,
  additionalActions?: (entity: EntityD) => Promise<void | any>
): Promise<Response> => {
  const query = Entity.findOneAndUpdate({_id: id, project: projectId}, args, {
    new: true,
    __user: updatedByUser._id,
  });
  const entity = await (populateOptions ? query.populate(populateOptions) : query);

  let finalEntity: any;
  if (additionalActions) finalEntity = await additionalActions(entity as EntityD);
  else finalEntity = entity;

  broadcastUpdate(res, "update", finalEntity);
  return res.status(201).json(finalEntity);
};
