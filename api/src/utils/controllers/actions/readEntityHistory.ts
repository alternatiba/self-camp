import {Registration, RegistrationD} from "@models/registration.model";
import {UserD} from "@models/user.model";
import {Response} from "express";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const diffHistory = require("mongoose-diff-history/diffHistory");

export type HistoryFormatted = {
  date: string;
  diff: any;
  registration: Pick<RegistrationD, "_id"> & {user: Pick<UserD, "firstName" | "lastName">};
};

export const readEntityHistory = async (
  id: string,
  projectId: string,
  entityClassName: string,
  res: Response<Array<HistoryFormatted>>
): Promise<Response<Array<HistoryFormatted>>> => {
  const history = await diffHistory.getDiffs(entityClassName, id, {sort: "-createdAt"});
  const registrations = await Registration.find(
    {
      user: history.map((h: any) => h.user),
      project: entityClassName === "Project" ? id : projectId,
    },
    "user _id"
  )
    .populate("user", "firstName lastName")
    .lean();
  return res.send(
    history.map(
      (h: any) =>
        ({
          date: h.createdAt,
          diff: h.diff,
          registration: registrations.find((r) => r.user._id.toString() === h.user?.toString()),
        } as HistoryFormatted)
    )
  );
};
