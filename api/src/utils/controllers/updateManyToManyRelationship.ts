import {Model} from "mongoose";

export const updateManyToManyRelationship = async (
  elementToRelateTo: any,
  manyOtherElementsIds: string[],
  Entity: Model<any>,
  fieldName: string,
  nestedArrayName?: string,
  additionalFieldsToAdd?: any
): Promise<string[]> => {
  const queryField = nestedArrayName ? `${nestedArrayName}.${fieldName}` : fieldName;

  // Select and update elements to link with the element
  const entitiesToLinkConditions = {
    [queryField]: {$not: {$in: elementToRelateTo}},
    _id: manyOtherElementsIds,
  };
  // Select and update entities to unlink from the element
  const entitiesToUnlinkConditions = {
    [queryField]: elementToRelateTo,
    _id: {$not: {$in: manyOtherElementsIds}},
  };

  // -- Get all the ids of the entities to update (before they get updated, after the update we will not be able to find them anymore) --
  const entitiesToUpdate = await Entity.find(
    {$or: [entitiesToLinkConditions, entitiesToUnlinkConditions]},
    "_id"
  );
  const entitiesToUpdateIds = entitiesToUpdate.map((s) => s._id);

  // -- Make the updates --
  await Entity.updateMany(entitiesToLinkConditions, {
    $push: {[nestedArrayName]: [{[fieldName]: elementToRelateTo, ...additionalFieldsToAdd}]},
  });
  await Entity.updateMany(entitiesToUnlinkConditions, {
    $pull: {[nestedArrayName]: {[fieldName]: elementToRelateTo}},
  });

  return entitiesToUpdateIds.map((id) => id.toString());
};
