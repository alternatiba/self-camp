import {logger} from "@services/logger";
import {RequestHandler} from "express";

// Filter fields if they are not wanted
export function filterRequestBody(args: string[]): RequestHandler {
  const allowedArgs = [...args, "__v"];
  return (req, res, next) => {
    const notWanted = Object.keys(req.body).filter(
      // Exclude all the keys that are not either like "myAllowedKey" or "myAllowedKey.subKey"
      (key) => !allowedArgs.includes(key) && !allowedArgs.includes(key.split(".")[0])
    );
    notWanted.forEach((key) => delete req.body[key]);
    notWanted.length && logger.debug(`Fields not wanted : ${notWanted.join(", ")}`);
    next();
  };
}
