import crypto from "crypto";
import config from "../config/config";

const algorithm = "aes-256-ctr";
const secretKey = config.encryptionKey;

type HashedString = {iv: string; content: string};

export const encrypt = (text: string): HashedString => {
  if (!(text?.length > 0)) return undefined;

  const iv = crypto.randomBytes(16);

  const cipher = crypto.createCipheriv(algorithm, secretKey, iv);

  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

  return {iv: iv.toString("hex"), content: encrypted.toString("hex")};
};

export const decrypt = (hash: HashedString): string => {
  if (!hash?.content || !hash?.iv) return undefined;

  const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, "hex"));

  const decrypted = Buffer.concat([
    decipher.update(Buffer.from(hash.content, "hex")),
    decipher.final(),
  ]);

  return decrypted.toString();
};

export const MongoEncrypted = {
  type: Object,
  set: encrypt,
  get: decrypt,
};
