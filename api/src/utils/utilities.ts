export const personName = (person: {firstName?: string; lastName?: string}): string =>
  person?.firstName?.length > 0
    ? person?.lastName?.length > 0
      ? `${person.firstName} ${person.lastName}`
      : person.firstName
    : "";
