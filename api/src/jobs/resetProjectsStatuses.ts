import {Job} from "./index";
import {Project} from "../models/project.model";
import moment from "moment";
import {logger} from "@services/logger";
import Mailer from "@services/mailer";
import config from "@config/config";
import {Registration} from "@models/registration.model";

/**
 * Job that resets projects statuses.
 * If a project is in pendingDonation status it is reset to demo
 * if pendingDonationSince is greater than 2 weeks
 * Runs every day at 00:00
 */
export default {
  name: "Reset project status to demo if no donation after a certain delay",
  run: async () => {
    if (!config.pendingDonationTimeInDays) return;

    const projects = await Project.find({});

    // Reset status for each project if it is in pendingDonation mode since more than config.pendingDonationTimeInDays
    for (const project of projects) {
      if (
        project.status === "pendingDonation" &&
        project.pendingDonationSince &&
        moment(project.pendingDonationSince).isBefore(
          moment().subtract(config.pendingDonationTimeInDays, "days")
        )
      ) {
        project.status = "demo";
        await project.save();
        logger.info(`[JOB] Project ${project.name} status was reset to demo`);

        // Get all registrations for this project and populate the users to get their emails
        const adminRegistrations = await Registration.find({
          project: project._id,
          role: "admin",
        }).populate("user");
        const adminEmails = adminRegistrations.map((registration) => registration.user.email);

        // Send email to all users
        for (const email of new Set([...adminEmails, project.contactEmail])) {
          await Mailer.sendMail("projects.projectStatusResetToDemo", email, {
            projectName: project.name,
            projectLink: `${config.urls.orgaFront}/${project.slug}`,
            contactLink: `${config.urls.website}/community/contact`,
            donateLink: config.urls.donate,
            locale: project.locale,
          });
        }
      }
    }
  },
  cronSchedule: "0 0 * * *", // Runs every day at 00:00
} satisfies Job;
