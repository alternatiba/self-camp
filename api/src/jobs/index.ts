import resetProjectsStatuses from "./resetProjectsStatuses";

/**
 * A job is a function that runs at a specific time and performs a task.
 * It is defined by a name, a function that runs the task, and a cron schedule.
 *
 * The cron schedule is a string that follows the Cron format (see https://crontab.guru/).
 * For example, "10 6 * * *" means the job will run every day at 06:10.
 * Timezone is always Europe/Paris.
 */
export type Job = {
  name: string;
  run: () => Promise<void>;
  cronSchedule: string;
};

/**
 * List of all jobs to be scheduled.
 */
export const JOBS: Array<Job> = [resetProjectsStatuses];
