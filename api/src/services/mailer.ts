import {t, TOptions} from "i18next";
import {createTransport} from "nodemailer";
import config from "../config/config";
import {compileHtmlTemplate} from "./html-and-pdf";
import {logger} from "./logger";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const md = require("markdown").markdown;

const isSmtpConfigured =
  config.smtp.host && config.smtp.port && config.smtp.user && config.smtp.pass;

const transporter =
  isSmtpConfigured &&
  createTransport({
    pool: true,
    host: config.smtp.host,
    port: config.smtp.port,
    auth: {
      user: config.smtp.user,
      pass: config.smtp.pass,
    },
  });

try {
  if (isSmtpConfigured) {
    transporter.verify().then((success) => {
      if (success) {
        logger.info("SMTP connection is ready");
      } else {
        logger.error("SMTP connection failed !");
      }
    });
  } else if (config.env === "production") {
    logger.warn("No valid SMTP config is given. Emails will not be sent.");
  }
} catch (e) {
  logger.error("SMTP connection failed !", e);
}

const headerImageUrl = `${config.urls.api}/assets/images/logo-colors-with-blue-text.png`;

class Mailer {
  async sendMail(emailKey: string, to: string, i18nOptions: TOptions): Promise<void> {
    // Generate the body
    const text = t(`emails:${emailKey}.body`, i18nOptions);
    const html = await compileHtmlTemplate("emails", {
      headerImageUrl,
      emailBody: md.toHTML(text),
    });

    // Assemble parameters
    const emailParams = {
      subject: t(`emails:${emailKey}.subject`, i18nOptions),
      to,
      from: `${config.instanceName} <${config.smtp.address || "fake@smtp.test"}>`, // Fallback on fake@smtp.test if there is no proper config
      ...i18nOptions,
    };

    // Send the email
    if (isSmtpConfigured && config.env === "production") {
      // If we are not in test mode, and that we have a correct SMTP config, use the real SMTP provider
      try {
        await transporter.sendMail({...emailParams, text, html});
        logger.info(`Email '${emailKey}' sent:`, emailParams);
      } catch (e) {
        logger.error(e);
        const errorEmailMessage = "FAILED TO DELIVER EMAIL to email " + to;
        // Send an email error to the email account.
        await transporter.sendMail({
          ...emailParams,
          text: errorEmailMessage,
          html: errorEmailMessage,
          to: config.smtp.address,
        });
      }
    } else {
      // If we are in test mode, or if we don't have a correct SMTP config, use the fake one
      logger.info(`[FAKE SMTP] Fake email '${emailKey}' sent:`, {
        ...emailParams,
        text,
        html,
      });
      // If in production with no correct config, throw a warning
      config.env === "production" &&
        logger.warn("[FAKE SMTP] No valid SMTP config is given, so emails are not sent for real");
    }
  }
}

export default new Mailer();
