import * as Sentry from "@sentry/node";
import {logger} from "./logger";
import {Job, JOBS} from "../jobs";
import cron from "node-cron";

const startRecurringJob = (job: Job) => {
  const safeRun = async () => {
    try {
      logger.info(`[JOB] Running "${job.name}" (${job.cronSchedule}) `);
      await job.run();
    } catch (error) {
      logger.error(`[JOB] Error running "${job.name}"`, error);
      Sentry.captureException(error);
    }
  };
  cron.schedule(job.cronSchedule, safeRun, {timezone: "Europe/Paris"});
};

JOBS[0].run();

export const runJobs = async () => {
  logger.info("Launching jobs:");
  for (const job of JOBS) {
    logger.info(` > (${job.cronSchedule}) ${job.name}`);
    startRecurringJob(job);
  }
};
