import {t} from "i18next";
import {inSidebar} from "@shared/utils/userTourUtilities";
import {Trans} from "react-i18next";
import {StepType} from "@reactour/tour";

export default (isMobileView, navigate): StepType[] => [
  {
    selector: ".userTourSubscribedSessionsFilterBar",
    content: <Trans i18nKey="userTour.subscribedSessions.sessionsFilters" ns="sessions" />,
    position: "bottom",
  },
  {
    selector: ".userTourSubscribedSessionsFilterBarAvailable",
    content: <Trans i18nKey="userTour.subscribedSessions.sessionFilterAvailable" ns="sessions" />,
    position: "bottom",
  },
  {
    selector: ".userTourSubscribedSessionsToggle",
    action: () => navigate("agenda"),
    actionAfter: () => navigate("./.."),
    content: <Trans i18nKey="userTour.subscribedSessions.toggleListOrAgenda" ns="sessions" />,
  },
  {
    ...inSidebar("span div .ant-menu"),
    content: <Trans i18nKey="userTour.subscribedSessions.myPlanningAndAllSessions" ns="sessions" />,
  },
  {
    selector: ".userTourSubscribedSessionsPdfExport",
    content: isMobileView ? (
      t("sessions:userTour.subscribedSessions.pdf")
    ) : (
      <>
        <p>{t("sessions:userTour.subscribedSessions.pdf")}</p>
        {t("sessions:userTour.subscribedSessions.phoneAlso")}
      </>
    ),
    position: "bottom",
  },
];
