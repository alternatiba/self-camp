import {t} from "i18next";
import {Trans} from "react-i18next";
import {inSidebar} from "@shared/utils/userTourUtilities";

export default () => [
  {
    selector: ".userTourBookmarkProject:first-of-type",
    content: t("projects:userTour.projectsLists.1"),
  },
  {
    ...inSidebar(".ant-menu:first-of-type"),
    content: <Trans i18nKey="userTour.projectsLists.2" ns="projects" />,
  },
];
