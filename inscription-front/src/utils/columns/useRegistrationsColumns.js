import {t} from "i18next";
import {RoleTag} from "@shared/components/RoleTag";
import {Tag} from "antd";
import {WaitingInvitationTag} from "@shared/components/WaitingInvitationTag";
import {sorter} from "@shared/utils/sorters";
import {useParticipantsFormAnswersColumns} from "./useParticipantsFormAnswersColumns";
import {useCustomFieldsColumns} from "@shared/utils/columns/useCustomFieldsColumns";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";

export const useRegistrationsColumns = ({start, middle, end} = {}) => {
  const useTeams = useSelector((s) => currentProjectSelectors.selectProject(s).useTeams);

  return [
    ...(start || []),
    {
      title: t("users:schema.firstName.label"),
      dataIndex: "firstName",
      render: (text, record) => {
        const sessionsNoShows = record.sessionsSubscriptions?.filter(
          (sessionSubscription) => sessionSubscription.hasNotShownUp
        );
        return (
          <>
            <RoleTag roleValue={record.role} small />
            {sessionsNoShows?.length > 0 && (
              <span title={`Ce·tte participant·e a fait défaut ${sessionsNoShows.length} fois.`}>
                {sessionsNoShows.map(() => "❗").join("")}{" "}
              </span>
            )}
            {record.user?.firstName}
          </>
        );
      },
      sorter: (a, b) => sorter.text(a.user?.firstName, b.user?.firstName),
      searchable: true,
      searchText: (record) => record.user?.firstName,
    },
    {
      title: t("users:schema.lastName.label"),
      dataIndex: "lastName",
      render: (text, record) => record.user?.lastName,
      sorter: (a, b) => sorter.text(a.user?.lastName, b.user?.lastName),
      defaultSortOrder: "ascend",
      searchable: true,
      searchText: (record) => record.user?.lastName,
    },
    {
      title: t("users:schema.email.label"),
      dataIndex: "email",
      render: (text, record) => (
        <>
          {record.invitationToken && <WaitingInvitationTag />}
          {record.user.email}
        </>
      ),
      sorter: (a, b) => sorter.text(a.user.email, b.user.email),
      searchable: true,
      searchText: (record) => record.user.email,
    },
    ...(middle || []),
    ...useParticipantsFormAnswersColumns(),
    ...useCustomFieldsColumns({endpoint: "registrations"}), // Not editable in inscription-front view
    {
      title: t("common:schema.tags.label"),
      dataIndex: "tags",
      sorter: (a, b) => sorter.text(a.tags?.join(" "), b.tags?.join(" ")),
      render: (text, record) => record.tags?.map((tagName) => <Tag>{tagName}</Tag>),
      searchable: true,
      searchText: (record) => record.tags?.join(" "),
    },
    useTeams && {
      title: t("teams:label"),
      dataIndex: "team",
      render: (text, record) =>
        record.teamsSubscriptions.map((teamSubscription) => teamSubscription.team.name).join(", "),
      sorter: (a, b) => sorter.text(a.teamsSubscriptionsNames, b.teamsSubscriptionsNames),
      searchable: true,
      searchText: (record) => record.teamsSubscriptionsNames,
    },
    {
      title: t("registrations:numberOfDaysOfPresence.label"),
      dataIndex: "numberOfDaysOfPresence",
      sorter: (a, b) => sorter.number(a.numberOfDaysOfPresence, b.numberOfDaysOfPresence),
      searchable: true,
      searchText: (record) => record.numberOfDaysOfPresence,
      width: 95,
    },
    ...(end || []),
  ].filter((el) => !!el);
};
