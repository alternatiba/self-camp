import {flattenFieldComponents} from "@shared/utils/flattenFieldComponents";
import {cleanAnswer} from "@shared/utils/columns/cleanAnswer";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {sorter} from "@shared/utils/sorters";
import {EditableCell} from "@shared/components/EditableCell";
import {EyeOutlined} from "@ant-design/icons";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {useTranslation} from "react-i18next";

type GetParticipantsFormAnswersColumnsParams = {
  nameSuffix?: string,
  getAllFormAnswers?: (record: any) => Array<any>,
};

// Get the columns generated from the participant custom data
export const useParticipantsFormAnswersColumns = ({
  nameSuffix,
  getAllFormAnswers = (record) => [record.formAnswers],
}: GetParticipantsFormAnswersColumnsParams = {}) => {
  const {t} = useTranslation();
  const formComponents = useSelector(
    (s) => currentProjectSelectors.selectProject(s).formComponents
  );

  const flatFormComponents = flattenFieldComponents(formComponents).filter(
    (comp) => comp.displayName
  );

  const renderAnswersList = (
    record,
    formComp,
    render = (formAnswers) => cleanAnswer(formAnswers?.[formComp.key], undefined, formComp)
  ) => listOfClickableElements(getAllFormAnswers(record), render);

  return (
    flatFormComponents?.map((formComp) => ({
      title: (
        <>
          <EyeOutlined title={t("projects:schema.formComponents.label")} style={{opacity: 0.8}} />{" "}
          {formComp.displayName + (nameSuffix ? ` ${nameSuffix}` : "")}
        </>
      ),
      dataIndex: formComp.displayName,
      render: (text, record) =>
        renderAnswersList(record, formComp, (formAnswers) => (
          <EditableCell value={formAnswers?.[formComp.key]} fieldComp={formComp} />
        )),
      sorter: (a, b) => sorter.text(renderAnswersList(a, formComp), renderAnswersList(b, formComp)),
      searchable: true,
      searchText: (record) => renderAnswersList(record, formComp),
    })) || []
  );
};
