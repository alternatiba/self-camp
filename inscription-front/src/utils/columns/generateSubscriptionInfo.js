import {t} from "i18next";
import {Link} from "react-router-dom";
import {dateFormatter} from "@shared/utils/formatters";
import {URLS} from "@app/configuration";

export const generateSubscriptionInfo = (registration, sessionSubscription, projectId) => {
  let res;
  const sessionSubscriptionTeam = registration.teamsSubscriptions
    .map((ts) => ts.team)
    ?.find((t) => t._id === sessionSubscription.team);

  if (sessionSubscriptionTeam) {
    res = (
      <>
        {t("sessions:schema.subscriptionInfo.viaTeam")}{" "}
        <Link to={`${URLS.ORGA_FRONT}/${projectId}/teams/${sessionSubscriptionTeam._id}`}>
          {sessionSubscriptionTeam.name}
        </Link>
      </>
    );
  }

  // Add date of subscription
  res = (
    <>
      {res && (
        <>
          {res} <br />
        </>
      )}
      <span style={{color: "gray"}}>
        {dateFormatter.longDateTime(sessionSubscription.createdAt, {short: true})}
      </span>
    </>
  );

  return res;
};
