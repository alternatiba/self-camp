import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {viewActions, viewSelectors} from "@features/view";
import React, {useEffect} from "react";
import RegistrationHelpButton from "../../registration/atoms/RegistrationHelpButton";
import {BarsOutlined, CalendarOutlined, QuestionOutlined} from "@ant-design/icons";
import {Segmented} from "antd";
import {useNavigate} from "react-router-dom";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";

export const ChangeSessionViewButtons = ({viewMode}) => {
  const navigate = useNavigate();
  const {isMobileView} = useWindowDimensions();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const sessionsViewMode = useSelector(viewSelectors.selectSessionsViewMode);
  const isListView = sessionsViewMode === "list";

  // Resynchronize viewMode if needed
  useEffect(() => {
    if (viewMode !== sessionsViewMode) {
      dispatch(viewActions.changeSessionsViewMode(viewMode));
    }
  }, [viewMode, sessionsViewMode]);

  const toggleView = () => navigate(isListView ? "agenda" : "./..");

  return (
    <div
      className={
        (viewMode === "agenda" && !isMobileView ? "with-margins" : "") +
        (viewMode === "list" ? " bounce-in-strong" : "")
      }
      style={{
        position: "fixed",
        bottom: isMobileView ? 10 : 20,
        right: isMobileView ? 10 : viewMode === "agenda" ? 10 : 25,
        zIndex: 1,
      }}>
      <div className={"containerH buttons-container"}>
        <RegistrationHelpButton
          type={"primary"}
          icon={<QuestionOutlined />}
          size={"large"}
          shape={"circle"}
          className={"shadow"}
        />

        <div
          id="view-mode-changer-select"
          className="bg-noe-gradient shadow userTourSubscribedSessionsToggle"
          style={{
            padding: 5,
            borderRadius: 10,
          }}>
          <div
            style={{background: "var(--colorBgContainer)", borderRadius: 10, overflow: "hidden"}}>
            <Segmented
              onChange={toggleView}
              options={[
                {
                  label: isMobileView
                    ? t("sessions:changeViewButton.list")
                    : t("sessions:changeViewButton.listView"),
                  value: "list",
                  icon: <BarsOutlined />,
                },
                {
                  label: isMobileView
                    ? t("sessions:changeViewButton.agenda")
                    : t("sessions:changeViewButton.agendaView"),
                  value: "agenda",
                  icon: <CalendarOutlined />,
                },
              ]}
              value={sessionsViewMode}
              style={{height: 40}}
              size="large"
            />
          </div>
        </div>
      </div>
    </div>
  );
};
