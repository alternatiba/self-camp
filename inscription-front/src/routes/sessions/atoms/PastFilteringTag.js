import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {useSessionsFilter} from "./useSessionsFilter";
import dayjs from "dayjs";
import {Tag, Tooltip} from "antd";

export const PastFilteringTag = ({agendaView}) => {
  const {t} = useTranslation();
  const currentProjectEnd = useSelector((s) => currentProjectSelectors.selectProject(s).end);

  const [{showPastSessionsFilter}, setSessionsFilter] = useSessionsFilter();

  return !agendaView && !dayjs().isAfter(dayjs(currentProjectEnd)) ? (
    <Tooltip title={t("sessions:searchControls.showPastSessionsFilter.tooltip")}>
      <Tag.CheckableTag
        checked={showPastSessionsFilter}
        onChange={(value) => setSessionsFilter({showPastSessionsFilter: value})}>
        {t("sessions:searchControls.showPastSessionsFilter.label")}
      </Tag.CheckableTag>
    </Tooltip>
  ) : null;
};
