import {cleanAnswer} from "@shared/utils/columns/cleanAnswer";
import {getEndpointInSession} from "./getEndpointInSession";
import {listOfClickableElements} from "@shared/utils/listOfClickableElements";
import {oneToManyRelationEndpoints} from "./getAllCustomFieldValuesForEndpoint";

export const getCustomFieldForSession = (customField, session) => {
  const cleanAnswerForObject = (object) =>
    cleanAnswer(object.customFields?.[customField.key], undefined, customField, "html");

  if (!session) return null;

  const endpointObject = getEndpointInSession(customField.meta.endpoint, session);
  if (!endpointObject) return null;

  if (oneToManyRelationEndpoints.includes(customField.meta.endpoint)) {
    return listOfClickableElements(endpointObject, cleanAnswerForObject, {lineBreaks: true});
  } else {
    return cleanAnswerForObject(endpointObject);
  }
};
