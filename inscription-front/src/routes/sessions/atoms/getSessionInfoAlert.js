import {t} from "i18next";
import React from "react";

export const getSessionInfoAlert = (
  sessionSubscription,
  isSteward,
  blockSubscriptions,
  registrationIncomplete,
  participantIsNotAvailable,
  sessionIsFull,
  alreadyStewardOnOtherSession,
  alreadySubscribedToOtherSession,
  inConflictWithRegistrationDates
) => {
  let message;
  if (isSteward) {
    message = {
      type: "info",
      message: t("sessions:alerts.isSteward"),
    };
  } else if (sessionSubscription?.team) {
    message = {
      type: "info",
      message: t("sessions:alerts.belongsToTeam"),
    };
  } else if (blockSubscriptions) {
    return; // Don't show anything if subscriptions are blocked for project
  } else if (registrationIncomplete) {
    message = {
      type: "info",
      message: t("sessions:alerts.mustCompleteRegistration"),
    };
  } else if (participantIsNotAvailable) {
    message = {
      type: "warning",
      message: t("sessions:alerts.participantIsNotAvailable"),
    };
  } else if (sessionIsFull) {
    message = {type: "info", message: t("sessions:alerts.sessionIsFull")};
  } else if (alreadyStewardOnOtherSession) {
    message = {
      type: "warning",
      message: t("sessions:alerts.alreadyStewardOnOtherSession"),
    };
  } else if (alreadySubscribedToOtherSession) {
    message = {
      type: "warning",
      message: t("sessions:alerts.alreadySubscribedToOtherSession"),
    };
  }

  // TODO this doesn't work anymore, looks like it is never shown
  if (inConflictWithRegistrationDates) {
    message = {
      message: (
        <>
          <p>{t("sessions:alerts.inConflictWithRegistrationDates")}</p>
          {message.message}
        </>
      ),
      ...(message || []),
    };
  }

  return message;
};
