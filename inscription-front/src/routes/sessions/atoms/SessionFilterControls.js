import {useSelector} from "react-redux";
import {sessionsSelectors} from "@features/sessions";
import React, {useEffect} from "react";
import {useUserTour} from "@shared/utils/userTourUtilities";
import subscribedSessionsUserTour from "@utils/userTours/subscribedSessionsUserTour";
import {useSessionsViewUrl} from "./useSessionsViewUrl";
import {useNavigate} from "react-router-dom";
import {CategoriesFilterSelect} from "./CategoriesFilterSelect";
import {SearchBox} from "./SearchBox";
import {FromDateFilterSelect} from "./FromDateFilterSelect";
import {useSessionsFilter} from "./useSessionsFilter";
import {CustomFieldsFilters} from "./CustomFieldsFilters";
import {AvailableFilteringTag} from "./AvailableFilteringTag";
import {ManagedFilteringTag} from "./ManagedFilteringTag";
import {PastFilteringTag} from "./PastFilteringTag";

export const SessionFilterControls = React.memo(
  ({agendaView = false, isMobileView, className, style, customButtons}) => {
    const navigate = useNavigate();

    const sessions = useSelector(sessionsSelectors.selectList);
    const [{registeredFilter, availableFilter, fromDateFilter}, setSessionsFilter] =
      useSessionsFilter();

    const viewUrl = useSessionsViewUrl();
    const viewOnlySubscribed = viewUrl === "subscribed";

    useUserTour("subscribingToSessions", subscribedSessionsUserTour(isMobileView, navigate), {
      shouldShowNow: () => !viewOnlySubscribed && !agendaView && sessions.length > 0,
      deps: [sessions.length],
      delayBeforeShow: 1000,
    });

    useEffect(() => {
      // Deactivate date filtering for agenda view
      if (agendaView && fromDateFilter) setSessionsFilter({fromDateFilter: undefined});

      if (viewOnlySubscribed) {
        // Deactivate the subscribed not subscribed select on subscribed viewUrl
        if (registeredFilter !== undefined) setSessionsFilter({registeredFilter: undefined});

        // Deactivate the available or not filter
        if (availableFilter) setSessionsFilter({availableFilter: false});
      }
    }, [agendaView, viewOnlySubscribed]);

    return (
      <div
        className={`containerH buttons-container ${className} userTourSubscribedSessionsFilterBar`}
        style={{alignItems: "center", marginBottom: 0, marginTop: 0, ...style}}>
        <SearchBox />

        <CategoriesFilterSelect />

        <CustomFieldsFilters mode={"inParticipantFilters"} />

        {customButtons}

        {!agendaView && <FromDateFilterSelect />}

        <div className={"userTourSubscribedSessionsFilterBarAvailable"} style={{flexGrow: 0}}>
          <PastFilteringTag agendaView={agendaView} />
          <AvailableFilteringTag />
          <ManagedFilteringTag />
        </div>
      </div>
    );
  },
  () => true
);
