import {useTranslation} from "react-i18next";
import {useSessionsFilter} from "./useSessionsFilter";
import {useSelector} from "react-redux";
import {registrationsSelectors} from "@features/registrations";
import {Tag, Tooltip} from "antd";

export const ManagedFilteringTag = () => {
  const {t} = useTranslation();
  const [{stewardFilter}, setSessionsFilter] = useSessionsFilter();

  const participantIsLinkedWithASteward = useSelector(
    (s) => !!registrationsSelectors.selectCurrent(s).steward
  );

  return participantIsLinkedWithASteward ? (
    <Tooltip title={t("sessions:searchControls.stewardFilter.tooltip")}>
      <Tag.CheckableTag
        checked={stewardFilter}
        onChange={(value) => setSessionsFilter({stewardFilter: value})}>
        {t("sessions:searchControls.stewardFilter.label")}
      </Tag.CheckableTag>
    </Tooltip>
  ) : null;
};
