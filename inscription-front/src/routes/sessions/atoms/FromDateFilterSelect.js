import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "@features/currentProject";
import {useSessionsFilter} from "./useSessionsFilter";
import {DatePickerComponent} from "@shared/inputs/DateTimeInput";
import {dayjsBase} from "@shared/services/dayjs";
import React from "react";

export const FromDateFilterSelect = () => {
  const {t} = useTranslation();
  const currentProjectStart = useSelector((s) => currentProjectSelectors.selectProject(s).start);
  const [{fromDateFilter}, setSessionsFilter] = useSessionsFilter();
  return (
    <DatePickerComponent
      bordered
      style={{flexBasis: 0, minWidth: 130}}
      value={fromDateFilter}
      defaultPickerValue={currentProjectStart}
      format={`ddd ${dayjsBase.localeData().longDateFormat("LL")}`}
      placeholder={t("sessions:searchControls.fromDateFilter.placeholder")}
      showTime={false}
      useMinutes={false}
      disableDatesIfOutOfProject
      disableDatesBeforeNow={false}
      onChange={(value) => setSessionsFilter({fromDateFilter: value})}
    />
  );
};
