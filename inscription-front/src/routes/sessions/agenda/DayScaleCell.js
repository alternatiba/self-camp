import {useAgendaParams} from "./useAgendaParams";
import {DayView} from "@devexpress/dx-react-scheduler-material-ui";
import dayjs from "@shared/services/dayjs";

export const DayScaleCell = (props) => {
  const [, setAgendaParams] = useAgendaParams();

  return (
    <DayView.DayScaleCell
      style={{cursor: "pointer"}}
      onDoubleClick={() =>
        setAgendaParams({
          currentAgendaDate: props.startDate.toISOString(),
          numberOfDaysDisplayed: 1,
        })
      }
      {...props}
      formatDate={(date, {weekday}) => dayjs(date).format(weekday ? "dddd" : "D")}
    />
  );
};
