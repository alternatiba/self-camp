import {App, Button, Card} from "antd";
import React from "react";
import {useSelector} from "react-redux";
import {viewSelectors} from "@features/view";
import {useTranslation} from "react-i18next";
import {useNavigate} from "react-router-dom";
import {registrationsSelectors} from "@features/registrations";
import {currentProjectSelectors} from "@features/currentProject";

const welcomeMessagePopupKey = "welcomeMessagePopup";
const useDestroyMessagePopup = () => {
  const {message} = App.useApp();
  return () => message.destroy(welcomeMessagePopupKey);
};

const BottomMessage = ({content, children}) => {
  return (
    <div
      className="ant-message ant-message-top"
      style={{
        left: "50%",
        transform: "translateX(-50%)",
        width: "100%",
        bottom: 10,
        position: "fixed",
        zIndex: 20,
      }}>
      <div>
        <div className={"bounce-in-strong"} style={{borderRadius: 10, animationDelay: "400ms"}}>
          <Card
            size={"small"}
            className={"shadow"}
            style={{
              width: "fit-content",
              maxWidth: "calc(100% - 20px)",
              margin: "auto",
              padding: "4px 10px",
            }}>
            <div className="containerH buttons-container">
              <p style={{textAlign: "center"}}>{content}</p>
              <div className="containerH buttons-container" style={{textAlign: "center"}}>
                {children}
              </div>
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
};

export const WelcomeMessagePopup = () => {
  const {t} = useTranslation();
  const searchParamsInfo = useSelector(viewSelectors.selectSearchParams);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  const projectIsNotOpened = currentProject.openingState === "notOpened";
  const destroyMessagePopup = useDestroyMessagePopup();
  const welcomeMessage = projectIsNotOpened
    ? t("welcome:eventIsNotOpen")
    : currentProject.full
    ? t("welcome:eventIsFull")
    : t("welcome:eventIsOpen");

  return (
    <BottomMessage
      content={
        <>
          {!currentProject.full &&
            searchParamsInfo.firstName &&
            t("welcome:hiFirstName", {
              firstName: searchParamsInfo.firstName,
            })}
          {welcomeMessage}
        </>
      }>
      {currentProject.full || projectIsNotOpened ? (
        <Button type="primary" href={"/signup"} onClick={destroyMessagePopup}>
          {t("welcome:registerToOtherEvents")}
        </Button>
      ) : (
        <Button type="primary" href={"signup"} onClick={destroyMessagePopup}>
          {t("welcome:register")}
        </Button>
      )}
      <Button style={{marginLeft: 8}} href={"login"} onClick={destroyMessagePopup}>
        {t("common:iAlreadyHaveAnAccount")}
      </Button>
    </BottomMessage>
  );
};

export const RegisterToProjectPopup = () => {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const destroyMessagePopup = useDestroyMessagePopup();

  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  // Do not display if the user is already registered
  if (currentRegistration._id !== "new" && currentRegistration.inDatabase.everythingIsOk)
    return null;

  return (
    <BottomMessage
      content={
        currentRegistration._id === "new"
          ? t("welcome:pleaseRegisterToTheEvent")
          : t("welcome:pleaseCompleteYourRegistration")
      }>
      <Button
        type="primary"
        onClick={() => {
          navigate("./registration");
          destroyMessagePopup();
        }}>
        {currentRegistration._id === "new"
          ? t("welcome:register")
          : t("welcome:completeMyRegistration")}
      </Button>
    </BottomMessage>
  );
};
