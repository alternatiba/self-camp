import React, {useCallback, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, App, Button, Divider, Space} from "antd";

import {TableElement} from "@shared/components/TableElement";
import Search from "antd/es/input/Search";

import {registrationsActions, registrationsSelectors} from "@features/registrations";
import {viewSelectors} from "@features/view";
import {personName} from "@shared/utils/utilities";
import {Trans, useTranslation} from "react-i18next";
import {CardElement} from "@shared/components/CardElement";
import {useWebSocketConnection} from "@shared/utils/api/webSocket";
import {currentUserSelectors} from "@features/currentUser";
import {t} from "i18next";
import {ArrowLeftOutlined} from "@ant-design/icons";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {currentProjectSelectors} from "@features/currentProject";
import RegistrationHelpButton from "./RegistrationHelpButton";
import {H3Title} from "@shared/layout/typography";
import {TransferTicketButton} from "./TransferTicketButton";
import {useRenderHtmlTicket} from "@shared/hooks/useRenderHtmlTicket";
import {ReactComponent as HelloAssoLogo} from "@images/hello-asso-logo.svg";

type TicketingProps = {
  IntegratedBookingComponent: ({
    project: any,
    saveData: (silent?: true) => void,
  }) => ReactNode,
  hasOptions: boolean,
  hasTicketName: boolean,
};

const scrollToAddManuallyInput = () => {
  const addManuallyInput = document.getElementById("addManuallyInput");
  window.scrollTo({
    top: addManuallyInput.getBoundingClientRect().top,
    behavior: "smooth",
  });
  setTimeout(() => addManuallyInput.focus(), 500);
};

const PaymentIframe = ({iframeUrl, style, title}) => {
  const {isMobileView} = useWindowDimensions();
  return (
    <CardElement
      className={"shadow-sm"}
      style={{overflow: "hidden", borderColor: "#ddd", backgroundImage: "var(--noe-gradient)"}}>
      {title && <div style={{marginBottom: 26}}>{title}</div>}
      <CardElement
        borderless
        style={{overflow: "hidden", margin: 0}}
        bodyStyle={{marginBottom: -7}}>
        <iframe
          src={iframeUrl}
          title="Payment iframe"
          style={{border: 0, height: isMobileView ? "120vh" : "85vh", width: "100%", ...style}}
        />
      </CardElement>
    </CardElement>
  );
};

const ticketingProps: Record<string, TicketingProps> = {
  helloAsso: {
    hasOptions: true,
    hasTicketName: true,
    IntegratedBookingComponent: ({project}) => {
      const ticketingUrl = `https://www.helloasso.com/associations/${project.helloAsso.organizationSlug}/evenements/${project.helloAsso.selectedEvent}`;
      const currentUser = useSelector(currentUserSelectors.selectUser);
      return (
        <div style={{gap: 26}} className={"containerV"}>
          <Alert
            showIcon
            type={"info"}
            message={
              <Trans
                i18nKey="ticketing.helloAssoAlert"
                ns="registrations"
                values={{email: currentUser.email}}
              />
            }
          />

          <PaymentIframe
            iframeUrl={`${ticketingUrl}/widget`}
            title={
              <div className={"containerH"} style={{gap: 16, flexWrap: "wrap"}}>
                <div style={{height: 40}}>
                  <HelloAssoLogo
                    style={{
                      height: "100%",
                      width: "auto",
                      background: "white",
                      padding: "8px 12px",
                      borderRadius: 10,
                    }}
                  />
                </div>
                <Button
                  onClick={scrollToAddManuallyInput}
                  ghost
                  size={"large"}
                  style={{alignSelf: "start"}}>
                  {t("registrations:ticketing.alreadyHaveTicket")}
                </Button>
              </div>
            }
            style={{background: "#f5f5f5", paddingTop: 20}}
          />
        </div>
      );
    },
  },
  tiBillet: {
    hasIntegratedPayment: false,
    IntegratedBookingComponent: ({project, saveData}) => {
      const ticketingUrl = `https://www.helloasso.com/associations/${project.helloAsso.organizationSlug}/evenements/${project.helloAsso.selectedEvent}`;
      return (
        <>
          <Alert
            type="success"
            style={{marginBottom: 26}}
            message={
              <Trans
                i18nKey="ticketing.thanksToTiBilletAlert"
                ns="registrations"
                components={{
                  linkToTiBillet: (
                    <a
                      href="https://tibillet.org"
                      onClick={saveData}
                      target="_blank"
                      rel="noreferrer"
                    />
                  ),
                }}
              />
            }
          />
          <PaymentIframe iframeUrl={ticketingUrl} />
        </>
      );
    },
  },
  customTicketing: {
    getTicketingUrl: ({customTicketing}) => customTicketing,
  },
};

export const useRegistrationTicketsColumns = () => {
  const renderHtmlTicket = useRenderHtmlTicket();
  const ticketingMode = useSelector((s) => currentProjectSelectors.selectProject(s).ticketingMode);

  return (tickets) =>
    [
      {
        title: t("registrations:ticketing.schema.id"),
        dataIndex: "id",
        render: (text, record) => renderHtmlTicket(record),
      },
      ticketingProps[ticketingMode].hasTicketName && {
        title: t("registrations:ticketing.schema.userName"),
        dataIndex: "username",
        render: (text, record) => personName(record.user),
      },
      {
        title: t("registrations:ticketing.schema.article"),
        dataIndex: "name",
      },
      {
        title: t("registrations:ticketing.schema.amount"),
        dataIndex: "amount",
        render: (text, record) => `${record?.amount} €`,
      },
      ticketingProps[ticketingMode].hasOptions &&
        tickets.find(({options}) => !!options) && {
          title: t("registrations:ticketing.schema.options"),
          dataIndex: "options",
          render: (text, record) =>
            record.options?.map((option) => `${option.name} (${option.amount} €)`).join(", "),
        },
    ].filter((el) => !!el);
};

export function RegistrationTicketing({saveData, paymentPageVisible, setPaymentPageVisible}) {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const ticketIdsString = useSelector(viewSelectors.selectSearchParams).ticketId;

  const currentUser = useSelector(currentUserSelectors.selectUser);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const currentRegistrationTickets = currentRegistration[`${currentProject.ticketingMode}Tickets`];
  const getTicketingColumns = useRegistrationTicketsColumns(currentRegistrationTickets);
  const IntegratedBookingComponent =
    ticketingProps[currentProject.ticketingMode]?.IntegratedBookingComponent;

  const shouldBeVisibleForPaymentOrTableRecap =
    currentProject.ticketingMode && (paymentPageVisible || currentRegistration.ticketingIsOk);

  const removeTicket = (record) => {
    // If there is only one ticket and user has booked, then we can't
    if (currentRegistration.inDatabase.ticketingIsOk && currentRegistrationTickets.length <= 1) {
      message.error(t("registrations:ticketing.messages.unregisterBeforeRemovingTicket"));
    } else {
      // Else if there are many or if the user is not booked, just delete it and update the modif button
      dispatch(registrationsActions.removeTicketFromRegistration(record.id));
    }
  };

  const addTicket = async (ticketId) => {
    saveData();

    // Make sure we don't add the ticket twice
    if (!currentRegistrationTickets?.find((ticket) => ticket.id === ticketId)) {
      const success = await dispatch(registrationsActions.addTicketToRegistration(ticketId));
      !!success && setPaymentPageVisible(false);
    } else {
      message.warning(t("registrations:ticketing.messages.alreadyUsedTicket"));
    }
  };

  const addTickets = async (ticketIds) => {
    for (const ticketId of ticketIds) {
      await addTicket(ticketId);
    }
  };

  // We memoize the onMessage function for the Websocket connection so that we don't recreate one each time the page re-renders.
  const memoizedAddTickets = useCallback(addTickets, [JSON.stringify(currentRegistrationTickets)]);

  // Websocket connection so we can push new tickets from the backend.
  useWebSocketConnection(
    !!IntegratedBookingComponent,
    {type: "ticketing", id: currentUser._id},
    memoizedAddTickets
  );

  // Automagically add the ticketIds if there is one in the URL and there is no ticket for the current registration
  useEffect(() => {
    if (ticketIdsString) {
      const ticketIdsToAdd = ticketIdsString.split(",");
      addTickets(ticketIdsToAdd);
    }
  }, [ticketIdsString]);

  return shouldBeVisibleForPaymentOrTableRecap ? (
    <div className="containerV" style={{marginBottom: 50}}>
      <Divider style={{marginBottom: 35}} />
      {paymentPageVisible && (
        <Button
          onClick={() => setPaymentPageVisible(false)}
          style={{marginBottom: 26, alignSelf: "start", paddingLeft: 0}}
          type={"link"}
          icon={<ArrowLeftOutlined />}
          size={"large"}>
          {t("registrations:backToForm")}
        </Button>
      )}
      <div className="list-element-header">
        <H3Title>
          {currentRegistration?.ticketingIsOk
            ? t("registrations:ticketing.labelMyTickets")
            : IntegratedBookingComponent
            ? t("registrations:ticketing.labelBookATicket")
            : t("registrations:ticketing.labelIHaveATicket")}
        </H3Title>
      </div>
      {currentRegistration?.ticketingIsOk && !paymentPageVisible ? (
        <>
          <Alert
            type={"success"}
            showIcon
            icon={"🎉"}
            message={t("registrations:ticketing.youHaveYourTicket")}
            style={{marginBottom: 26}}
          />
          <TableElement.Simple
            showHeader
            style={{marginBottom: 15}}
            columns={[
              ...getTicketingColumns(currentRegistrationTickets),
              {
                dataIndex: "transfer",
                width: 42,
                render: (text, record) => <TransferTicketButton ticketId={record.id} />,
              },
            ]}
            onDelete={removeTicket}
            rowKey="id"
            dataSource={currentRegistrationTickets}
          />
        </>
      ) : (
        IntegratedBookingComponent &&
        paymentPageVisible && (
          <IntegratedBookingComponent project={currentProject} saveData={saveData} />
        )
      )}

      <Space wrap>
        {IntegratedBookingComponent && !currentRegistration.ticketingIsOk ? (
          t("registrations:ticketing.addManually")
        ) : (
          <strong>
            {currentRegistration?.ticketingIsOk
              ? t("registrations:ticketing.addAnotherTicket")
              : t("registrations:ticketing.addYourTicketHere")}
          </strong>
        )}
        <Search
          allowClear
          id={"addManuallyInput"}
          style={{maxWidth: 250}}
          enterButton={t("registrations:ticketing.add")}
          onSearch={addTicket}
          placeholder={t("registrations:ticketing.ticketInputPlaceholder")}
        />
      </Space>
      {IntegratedBookingComponent && (
        <div style={{marginTop: 26, textAlign: "center"}}>
          <RegistrationHelpButton type={"link"}>
            {t("registrations:aProblemToRegister")}
          </RegistrationHelpButton>
        </div>
      )}
    </div>
  ) : null;
}
