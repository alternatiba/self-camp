import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions, currentUserSelectors} from "@features/currentUser";
import {Button, Form} from "antd";
import {AuthPage} from "../AuthPage";
import {useTranslation} from "react-i18next";
import {safeValidateFields} from "../../inputs/FormElement";
import {TextInputPassword} from "../../inputs/TextInput";
import {useLocation, useNavigate} from "react-router-dom";
import {checkPasswordsAreSame, validatePassword} from "@shared/utils/passwordValidators";

export default function ResetPassword({footer}) {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const [form] = Form.useForm();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const connectionError = useSelector(currentUserSelectors.selectConnectionError);
  const dispatch = useDispatch();
  let query = new URLSearchParams(useLocation().search);
  const token = query.get("token");

  useEffect(() => {
    dispatch(currentUserActions.checkPasswordResetToken(token));
  }, [token]);

  const onChange = (changedFields, allFields) => {
    dispatch(currentUserActions.changePassword(allFields[0].value));
  };

  const resetPassword = () => {
    safeValidateFields(form, () => {
      dispatch(currentUserActions.changeConnectionError(undefined));
      dispatch(currentUserActions.changeConnectionNotice(undefined));
      dispatch(currentUserActions.resetPassword(token)).then(
        (success) => success && navigate("./../login")
      );
    });
  };

  const goToForgotPasswordPage = () => {
    // Flush alerts
    dispatch(currentUserActions.changeConnectionError(undefined));
    dispatch(currentUserActions.changeConnectionNotice(undefined));
    navigate("/forgotpassword");
  };

  return (
    <AuthPage
      form={form}
      dataFormType={"change_password"}
      footer={footer}
      subtitle={t("users:schema.password.labelNew")}
      fields={[{name: "password", value: currentUser.password}]}
      onFieldsChange={onChange}
      onValidate={resetPassword}
      buttons={
        connectionError ? (
          <Button onClick={goToForgotPasswordPage}>
            {t("common:connectionPage.sendPasswordRecoveryEmailAgain")}
          </Button>
        ) : (
          <Button type="primary" onClick={resetPassword} htmlType="submit">
            {t("common:connectionPage.validatePasswordChange")}
          </Button>
        )
      }>
      <TextInputPassword
        label={t("users:schema.password.labelNew")}
        name="password"
        placeholder={t("users:schema.password.placeholder")}
        autoComplete="new-password"
        data-form-type={"password,new"}
        rules={[{validator: validatePassword(form)}, {min: 8}]}
        required
        bordered
      />
      <TextInputPassword
        label={t("users:schema.password.labelConfirmNew")}
        name="confirmPassword"
        placeholder={t("users:schema.password.placeholder")}
        autoComplete="new-password"
        data-form-type={"password,confirmation"}
        dependencies={["password"]}
        rules={[{validator: checkPasswordsAreSame(form)}]}
        required
        bordered
      />
    </AuthPage>
  );
}
