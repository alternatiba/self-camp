import {Badge, Button} from "antd";
import React from "react";
import Sider from "antd/es/layout/Sider";
import {useLocalStorageState} from "@shared/utils/localStorageUtilities";
import {ArrowUpOutlined} from "@ant-design/icons";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {H3Title} from "./typography";
import {useSelector} from "react-redux";
import {viewSelectors} from "@features/view";

export default function Sidebar({title, children, forceCollapsed, ribbon}) {
  const [userCollapsed, setUserCollapsed] = useLocalStorageState("sidebar-collapsed", false);
  const {isMobileView} = useWindowDimensions();
  const darkMode = useSelector(viewSelectors.selectDarkMode);
  const isCollapsed = forceCollapsed || userCollapsed;

  const boxShadow = "3px 3px 15px 0px rgba(0,0,0,0.65)";
  const sider = (
    <div
      style={{
        overflow: "hidden",
        margin: "5px 0 5px 5px",
        borderRadius: 20,
        boxShadow,
        WebkitBoxShadow: boxShadow,
      }}>
      <Sider
        collapsed={isCollapsed}
        breakpoint="md"
        width={210}
        style={{
          overflow: "auto",
          borderRadius: 20,
          height: "calc(100vh - 10px)",
          background: darkMode ? "var(--colorBgElevated)" : "var(--noe-bg)",
          border: darkMode ? "1px solid var(--colorBorder)" : undefined,
        }}>
        <div className="sidebar-container">
          {title && (
            <H3Title
              className="text-white text-center fade-in"
              style={{
                padding: "5pt",
                paddingTop: "15pt",
                flexGrow: 0,
              }}>
              {isCollapsed
                ? title
                    ?.split(" ")
                    .map((word) => word.charAt(0))
                    .join("")
                : title}
            </H3Title>
          )}
          {children}
        </div>
      </Sider>
    </div>
  );

  return (
    <div style={{position: "sticky", top: 0, height: "100vh", zIndex: 100}}>
      {ribbon ? (
        <Badge.Ribbon text={ribbon} color="#6cdac5">
          {sider}
        </Badge.Ribbon>
      ) : (
        sider
      )}
      {!forceCollapsed && !isMobileView && (
        <Button
          shape={"circle"}
          size={"small"}
          type={"default"}
          style={{
            position: "absolute",
            bottom: "20%",
            right: -10,
            zIndex: 1000,
            transition: "transform 0.6s",
            transitionDelay: "0.4s",
            transform: userCollapsed ? "rotate(90deg)" : "rotate(-90deg)",
          }}
          className={"shadow"}
          onClick={() => setUserCollapsed(!userCollapsed)}
          icon={<ArrowUpOutlined />}
        />
      )}
    </div>
  );
}
