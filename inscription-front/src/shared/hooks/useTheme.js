import {useSelector} from "react-redux";
import {viewSelectors} from "@features/view";
import {DARK_THEME, LIGHT_THEME} from "@config/theme";

export const useTheme = () => {
  const darkMode = useSelector(viewSelectors.selectDarkMode);
  return darkMode ? DARK_THEME : LIGHT_THEME;
};
