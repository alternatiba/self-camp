import {useDispatch, useSelector} from "react-redux";
import {defaultPagination, viewActions, viewSelectors} from "@features/view";

export const useSavedPagination = () => {
  const dispatch = useDispatch();
  const pagination = useSelector(viewSelectors.selectPagination) || defaultPagination;
  const setPagination = (current, pageSize) =>
    dispatch(viewActions.changePagination({current, pageSize}));
  return [pagination, setPagination];
};
