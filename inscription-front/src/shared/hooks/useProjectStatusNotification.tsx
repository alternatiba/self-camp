import {Trans, useTranslation} from "react-i18next";
import {useEffect} from "react";
import {displayNotification} from "@shared/utils/displayNotification";
import {currentProjectSelectors} from "@features/currentProject";
import {useSelector} from "react-redux";
import {App} from "antd";
import {URLS} from "@app/configuration";
import {DONATE_URL} from "@config/sharedConfig";
import dayjs from "@shared/services/dayjs";
import {useThemeToken} from "./useThemeToken";

/**
 * Demo notif when the project is in DEMO mode
 */
export const useProjectStatusNotification = () => {
  const {t} = useTranslation();
  const {notification} = App.useApp();
  const projectStatus = useSelector((s) => currentProjectSelectors.selectProject(s)?.status);
  const pendingDonationSince = useSelector(
    (s) => currentProjectSelectors.selectProject(s)?.pendingDonationSince
  );

  const infoFooter = (
    <Trans
      i18nKey="projectStatusNotification.infoFooter"
      ns="common"
      components={{
        linkToContact: (
          <a href={`${URLS.WEBSITE}/community/contact`} target={"_blank"} rel="noreferrer" />
        ),
        linkToDonation: <a href={DONATE_URL} target={"_blank"} rel="noreferrer" />,
      }}
    />
  );

  const pendingDonationTimeInDays = process.env.REACT_APP_PENDING_DONATION_TIME_IN_DAYS
    ? Number(process.env.REACT_APP_PENDING_DONATION_TIME_IN_DAYS)
    : 14;

  const colorWarning = useThemeToken().colorWarningText;

  useEffect(() => {
    if (
      projectStatus === "demo" || // Display everywhere if demo mode
      (projectStatus === "pendingDonation" && URLS.CURRENT === URLS.ORGA_FRONT) // Display only on orga-front if pending donation
    ) {
      displayNotification("warning", "projectStatusNotification", {
        message: (
          <Trans i18nKey={`projectStatusNotification.${projectStatus}.message`} ns="common" />
        ),
        description: (
          <>
            <Trans
              i18nKey={`projectStatusNotification.${projectStatus}.description`}
              ns="common"
              values={{
                count: dayjs()
                  .add(pendingDonationTimeInDays, "days")
                  .diff(dayjs(pendingDonationSince), "days"),
              }}
              components={{
                warningText: <span style={{color: colorWarning}} />,
              }}
            />
            {infoFooter}
          </>
        ),
      });
    }

    return () => notification.destroy("projectStatusNotification");
  }, [projectStatus]);
};
