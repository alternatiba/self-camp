import {useState} from "react";

/**
 * useMultipleSelection
 * @param onDeleteElement what to do to delete one element selected based on its id
 */
export const useMultipleSelection = (onDeleteElement: (elementId: string) => void) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [enableMultipleSelection, setEnableMultipleSelection] = useState(false);

  const rowSelection = enableMultipleSelection
    ? {
        selectedRowKeys,
        onChange: (newSelectedRowKeys) => setSelectedRowKeys(newSelectedRowKeys),
      }
    : undefined;

  const onBatchDelete = async () => {
    const notDeleted = [];
    for (const id of selectedRowKeys) {
      try {
        await onDeleteElement(id);
      } catch {
        notDeleted.push(id);
      }
    }
    setSelectedRowKeys(notDeleted);
  };

  return {
    /**
     * State to enable or disable multiple selection mode.
     */
    enableMultipleSelection,
    setEnableMultipleSelection,

    /**
     * State to set and get manually the selected keys
     */
    selectedRowKeys,
    setSelectedRowKeys,

    /**
     * Put this rowSelection object on a Table
     */
    rowSelection,

    /**
     * The function to call when you want to batch delete all the selection
     */
    onBatchDelete,
  };
};
