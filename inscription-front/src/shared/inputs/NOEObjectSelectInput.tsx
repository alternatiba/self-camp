import {SelectProps} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";
import {FormItemProps} from "./FormItem";
import {SelectInput} from "./SelectInput";

const noeObjectsEndpoints = [
  "categories",
  "places",
  "stewards",
  "activities",
  "teams",
  "sessions",
] as const;
export type NOEEndpoint = (typeof noeObjectsEndpoints)[number];

export const noeObjectsEndpointsExtended = [...noeObjectsEndpoints, "registrations"] as const;
export type NOEEndpointExtended = (typeof noeObjectsEndpointsExtended)[number];

export const useNOEObjectsWithLabels = (extended = false) => {
  const {t} = useTranslation();
  return (extended ? noeObjectsEndpointsExtended : noeObjectsEndpoints).map((endpoint) => ({
    label: t(`${endpoint}:label_other`),
    value: endpoint,
  }));
};

export const NOEObjectSelectInput = ({
  extended,
  ...props
}: Omit<FormItemProps<SelectProps>, "options"> & {
  extended?: boolean;
}) => {
  const options = useNOEObjectsWithLabels(extended);
  return <SelectInput options={options} allowClear {...props} />;
};
