import {PlusOutlined} from "@ant-design/icons";
import {EntitiesSelectors, EntityBase} from "@utils/features/types";
import {Button, Empty, SelectProps} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {useLoadList} from "../hooks/useLoadList";
import {useNewElementModal} from "../hooks/useNewElementModal";
import {FormItem, FormItemProps} from "./FormItem";
import {SelectComponent} from "./SelectInput";

export type EntitiesSelectComponentProps = Omit<SelectProps, "value"> & {
  elementsActions: any;
  elementsSelectors: EntitiesSelectors<any, any>;
  getEntityLabel: (entity: any) => string; // How to render the label
  ElementEdit?: React.FC; // If given, it will allow to display a Create button if user searches for some entity taht doesn't exist.
} & (
    | {
        mode: "multiple";
        value?: Array<EntityBase>; // If multiple mode, then value is an array of entities
        deselectEntityLabel: never;
      }
    | {
        mode: undefined;
        value?: EntityBase; // If single mode, then value is an entity
        deselectEntityLabel?: string; // The label to deselect the field
      }
  );

/**
 * This component takes entities as values and options, instead of simple strings.
 */
export const EntitiesSelectComponent = ({
  value: rawValue,
  mode,
  onChange,
  elementsActions,
  elementsSelectors,
  getEntityLabel,
  deselectEntityLabel,
  ElementEdit,
  ...props
}: EntitiesSelectComponentProps) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [setShowNewElementModal, NewElementModal] = useNewElementModal(ElementEdit);

  const entities = useSelector(elementsSelectors.selectList);
  const entitiesById = useSelector(elementsSelectors.selectById);
  const entitiesLoaded = useSelector(elementsSelectors.selectIsLoaded);

  useLoadList(() => {
    dispatch(elementsActions.loadList());
  });

  const value = mode === "multiple" ? rawValue?.map((s) => s._id) : rawValue?._id;

  // Imitate the default onChange method, but with entities instead of values
  const onChangeSelect: SelectProps["onChange"] = (value, option) =>
    onChange?.(
      mode === "multiple"
        ? value.map((id: string) => entitiesById[id])
        : value === null // If the value is null, leave it as it is. Else, select the corresponding entity associated to it
        ? null
        : entitiesById[value],
      option
    );

  return (
    <>
      <SelectComponent
        disabled={!entitiesLoaded}
        value={value}
        mode={mode}
        {...props}
        options={[
          ...(!mode && deselectEntityLabel
            ? [
                {
                  value: null,
                  label: (
                    <span style={{fontStyle: "italic", color: "gray"}}>
                      - {deselectEntityLabel} -
                    </span>
                  ),
                },
              ]
            : []), // Only display the "no something" in single mode
          ...entities.map((entity) => ({value: entity._id, label: getEntityLabel(entity)})), // Get entities options
        ]}
        onChange={onChangeSelect}
        showSearch
        notFoundContent={
          // If no search is successul, offer the user to create its own entity
          ElementEdit ? (
            <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}>
              <Button
                type={"link"}
                icon={<PlusOutlined />}
                onClick={() => setShowNewElementModal(true)}>
                {t("common:editPage.buttonTitle.create")}
              </Button>
            </Empty>
          ) : undefined
        }
      />

      <NewElementModal />
    </>
  );
};

export const EntitiesSelectInput = (props: FormItemProps<EntitiesSelectComponentProps>) => (
  <FormItem {...props}>
    {/* @ts-ignore because the FormItem cares about the props passing */}
    <EntitiesSelectComponent />
  </FormItem>
);
