import {Progress, ProgressProps} from "antd";
import React from "react";
import {FormItem, FormItemProps} from "./FormItem";

type FillingComponentProps = {text?: string} & ProgressProps;

const FillingComponent = ({text, ...props}: FillingComponentProps) => (
  <div className="containerH buttons-container">
    <div style={{flexGrow: 100}}>
      <Progress size="small" showInfo={false} {...props} />
    </div>
    <div>{text}</div>
  </div>
);

export const FillingInput = (props: FormItemProps<FillingComponentProps>) => (
  <FormItem {...props}>
    <FillingComponent />
  </FormItem>
);
