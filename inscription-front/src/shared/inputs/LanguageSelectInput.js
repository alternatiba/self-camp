import {FormItemProps, SelectProps} from "antd";
import {SelectInput} from "./SelectInput";
import {useTranslation} from "react-i18next";

export const LanguageSelectInput = (props: Omit<FormItemProps<SelectProps>, "options">) => {
  const {t} = useTranslation();

  return (
    <SelectInput
      options={[
        {value: "fr", label: t("users:schema.locale.options.french")},
        {value: "en", label: t("users:schema.locale.options.english")},
      ]}
      {...props}
    />
  );
};
