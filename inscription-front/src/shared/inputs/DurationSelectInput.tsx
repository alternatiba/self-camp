import {timeFormatter} from "@shared/utils/formatters";
import {SelectProps} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";
import {FormItemProps} from "./FormItem";
import {SelectInput} from "./SelectInput";

export const DurationSelectInput = (
  props: FormItemProps<Omit<SelectProps, "options" | "showSearch">>
) => {
  const {t} = useTranslation();
  return (
    <SelectInput
      label={t("common:schema.duration.label")}
      placeholder={t("common:schema.duration.placeholder")}
      options={Array.from(Array(287), (_, i) => ({
        value: (i + 1) * 5,
        label: timeFormatter.duration((i + 1) * 5),
      }))}
      showSearch
      {...props}
    />
  );
};
