// Returns the navbar height, or zero if the navbar isn't present
export const navbarHeight = () =>
  document.querySelector(".mobile-navbar-container")?.offsetHeight || 0;

export const getFullHeightWithMargins = (element) =>
  element
    ? element.offsetHeight + // Height
      parseInt(window.getComputedStyle(element).getPropertyValue("margin-top")) +
      parseInt(window.getComputedStyle(element).getPropertyValue("margin-bottom"))
    : 0;
