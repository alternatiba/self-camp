import {dayjsBase} from "@shared/services/dayjs"; // Needed to import all dayjs plugins
import i18next from "i18next";
import {dateFormatter, timeFormatter} from "./formatters";

i18next.init({
  lng: "fr",
  resources: {
    fr: {
      common: {
        time: {
          xMin: "{{count}}min",
          hoursDurationFormat: "H[h]mm",
          xDaysAndHours: "{{days}} jours et {{hours}}",
        },
      },
    },
  },
});
dayjsBase.locale("fr");

describe("timeFormatter", () => {
  describe("time", () => {
    it("should correctly format the time", () => {
      const date = "2023-07-15T14:30:00Z";
      expect(timeFormatter.time(date)).toBe("16:30");
    });

    it("should return an empty string for an invalid date", () => {
      expect(timeFormatter.time("")).toBe("");
      // @ts-expect-error: testing an undefined date
      expect(timeFormatter.time(undefined)).toBe("");
    });
  });

  describe("timeRange", () => {
    it("should correctly format a time range", () => {
      const start = "2023-07-15T14:30:00Z";
      const end = "2023-07-15T16:00:00Z";
      expect(timeFormatter.timeRange(start, end)).toBe("16:30 – 18:00");
    });

    it("should return an empty string if start or end is missing", () => {
      expect(timeFormatter.timeRange("", "2023-07-15T16:00:00Z")).toBe("");
      // @ts-expect-error: testing an undefined date
      expect(timeFormatter.timeRange("2023-07-15T14:30:00Z", undefined)).toBe("");
      // @ts-expect-error: testing an undefined date
      expect(timeFormatter.timeRange(undefined, undefined)).toBe("");
    });
  });

  describe("duration", () => {
    it("should correctly format a short duration", () => {
      expect(timeFormatter.duration(30)).toBe("30min");
    });

    it("should correctly format a long duration", () => {
      expect(timeFormatter.duration(3150)).toBe("2 jours et 4h30 (3150min)");
    });

    it("should correctly format a long duration in short format", () => {
      expect(timeFormatter.duration(3150, {short: true})).toBe("2 jours et 4h30");
    });
  });
});

describe("dateFormatter", () => {
  describe("shortDate", () => {
    it("should correctly format a short date", () => {
      const date = "2023-07-15T14:30:00Z";
      expect(dateFormatter.shortDate(date)).toBe("15/07");
    });

    it("should return an empty string for an invalid date", () => {
      expect(dateFormatter.shortDate("")).toBe("");
      // @ts-expect-error: testing an undefined date
      expect(dateFormatter.shortDate(undefined)).toBe("");
    });
  });

  describe("longDate", () => {
    it("should correctly format a long date", () => {
      const date = "2023-07-15T14:30:00Z";
      expect(dateFormatter.longDate(date)).toBe("samedi 15 juillet");
    });

    it("should correctly format a long date with year", () => {
      const date = "2023-07-15T14:30:00Z";
      expect(dateFormatter.longDate(date, {withYear: true})).toBe("samedi 15 juillet 2023");
    });

    it("should correctly format a short date", () => {
      const date = "2023-07-15T14:30:00Z";
      expect(dateFormatter.longDate(date, {short: true})).toBe("sam. 15 juil.");
    });
  });

  describe("longDateTime", () => {
    it("should correctly format a long date and time", () => {
      const date = "2023-07-15T14:30:00Z";
      expect(dateFormatter.longDateTime(date)).toBe("samedi 15 juillet 16:30");
    });
  });

  describe("longDateRange", () => {
    it("should correctly format a date range", () => {
      const start = "2023-07-15T14:30:00Z";
      const end = "2023-07-16T16:00:00Z";
      expect(dateFormatter.longDateRange(start, end)).toBe(
        "samedi 15 juillet – dimanche 16 juillet"
      );
      expect(dateFormatter.longDateRange(start, end, {withYear: true})).toBe(
        "samedi 15 juillet – dimanche 16 juillet 2023"
      );
    });
  });

  describe("longDateTimeRange", () => {
    it("should correctly format a date and time range on the same day", () => {
      const start = "2023-07-15T14:30:00Z";
      const end = "2023-07-15T16:00:00Z";
      expect(dateFormatter.longDateTimeRange(start, end)).toBe("samedi 15 juillet 16:30 – 18:00");
    });

    it("should correctly format a date and time range on different days", () => {
      const start = "2023-07-15T14:30:00Z";
      const end = "2023-07-16T16:00:00Z";
      expect(dateFormatter.longDateTimeRange(start, end)).toBe(
        "samedi 15 juillet 16:30 – dimanche 16 juillet 18:00"
      );
    });
  });
});
