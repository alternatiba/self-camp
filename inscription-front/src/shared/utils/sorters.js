import {dayjsBase} from "@shared/services/dayjs";
import {normalize} from "./stringUtilities";

const compare = (a, b) => {
  if (a === b) {
    return 0;
  } else {
    const aDefined = a !== undefined && a !== null;
    const bDefined = b !== undefined && b !== null;
    if (aDefined && bDefined) return a > b ? 1 : -1;
    else if (aDefined) return 1;
    else if (bDefined) return -1;
    else return 0;
  }
};

export const sorter = {
  date: (a, b) =>
    (a ? dayjsBase(a).valueOf() : -Infinity) - (b ? dayjsBase(b).valueOf() : -Infinity),

  text: (a, b) => compare(normalize(a).replace(/ /g, ""), normalize(b).replace(/ /g, "")),

  number: compare,
};
