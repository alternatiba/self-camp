import React, {useState} from "react";
import {Alert, Button, Dropdown} from "antd";
import {PictureOutlined} from "@ant-design/icons";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {useTranslation} from "react-i18next";

export const ExportAgendaViewButton = ({small = false, style, extraMenus = []}) => {
  const {t} = useTranslation();
  const {isMobileView} = useWindowDimensions();
  const [loading, setLoading] = useState(false);

  // Don't display the feature in mobile view
  if (isMobileView) return null;

  const onPrintAgendaView = async ({key: exportFormat}: {key: "jpg" | "svg" | "png"}) => {
    setLoading(true);

    // Get the main scheduler wrapper
    const agendaSpace =
      document.getElementsByClassName("scheduler-wrapper")[0].firstChild.lastChild;

    const exportFormatMapping = {
      jpg: {functionName: "toJpeg", options: {quality: 0.95}},
      png: {functionName: "toPng"},
      svg: {functionName: "toSvg"},
    };

    const {functionName, options} = exportFormatMapping[exportFormat];

    // Remove the scrolling and hidden content
    agendaSpace.style.overflowY = "visible";

    // Generate the image
    const htmlToImage = await require("html-to-image");
    const dataUrl = await htmlToImage[functionName](
      document.getElementsByClassName("MainLayout-container")[0],
      {
        ...options,
        backgroundColor: "white",
        pixelRatio: 5,
      }
    );

    // Download it
    const link = document.createElement("a");
    link.download = `${t("sessions:agenda.menuDrawer.exportAgendaView.fileName")}.${exportFormat}`;
    link.href = dataUrl;
    link.click();

    // Re-establish scrolling view
    agendaSpace.style.overflowY = "auto";
    setLoading(false);
  };

  return (
    <Dropdown
      menu={{
        items: [
          {
            label: t("sessions:agenda.menuDrawer.exportAgendaView.formatOption", {
              fileFormat: "JPG",
            }),
            key: "jpg",
            onClick: onPrintAgendaView,
          },
          {
            label: t("sessions:agenda.menuDrawer.exportAgendaView.formatOption", {
              fileFormat: "PNG",
            }),
            key: "png",
            onClick: onPrintAgendaView,
          },
          {
            label: t("sessions:agenda.menuDrawer.exportAgendaView.formatOption", {
              fileFormat: "SVG",
            }),
            key: "svg",
            onClick: onPrintAgendaView,
          },
          {type: "divider"},
          {
            label: (
              <Alert
                style={{maxWidth: 200}}
                message={t("sessions:agenda.menuDrawer.exportAgendaView.warningMessage")}
              />
            ),
            style: {cursor: "auto"},
          },
        ].concat(extraMenus),
      }}
      trigger={["click"]}>
      <Button
        icon={<PictureOutlined />}
        type={small ? "link" : "primary"}
        loading={loading}
        style={style}>
        {!small && t("sessions:agenda.menuDrawer.exportAgendaView.buttonTitle")}
      </Button>
    </Dropdown>
  );
};
