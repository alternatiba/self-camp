import React from "react";
import Icon, {GitlabOutlined, MailOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {ReactComponent as DiscordLogo} from "@images/discord-outlined.svg";
import {ReactComponent as NoeLogo} from "@images/logo-base.svg";
import {LinkContainer} from "./LinkContainer";
import {URLS} from "@app/configuration";
import {DISCORD_INVITE_LINK, GITLAB_URL} from "@config/sharedConfig";

export const DiscordLink = (props) => (
  <LinkContainer href={DISCORD_INVITE_LINK} icon={<Icon component={DiscordLogo} />} {...props}>
    Discord{" "}
  </LinkContainer>
);

export const GitlabLink = (props) => (
  <LinkContainer
    href={`${GITLAB_URL}/-/blob/master/CONTRIBUTING.md`}
    icon={<GitlabOutlined />}
    {...props}>
    Gitlab
  </LinkContainer>
);

export const ContactNOELink = ({children, ...props}) => {
  const {t} = useTranslation();
  console.log("children", children);
  return (
    <LinkContainer
      href={`${URLS.WEBSITE}/community/contact`}
      target=""
      icon={<MailOutlined />}
      {...props}>
      {children || t("common:contactUs")}
    </LinkContainer>
  );
};

export default function NOESocialIcons({expanded = false, linkClassname}) {
  const {t} = useTranslation();

  const iconStyle = {fontSize: 18};

  return (
    <div className="containerV" style={{alignItems: "center", gap: 8}}>
      {/* Website link */}
      <LinkContainer
        showTitle={false}
        href={URLS.WEBSITE}
        icon={
          <div
            className={"containerH"}
            style={{flexWrap: "wrap", justifyContent: "center", textAlign: "center", gap: 8}}>
            <Icon component={NoeLogo} />
            {new URL(URLS.WEBSITE).hostname}
          </div>
        }
        className={linkClassname}>
        {t("common:website")}
      </LinkContainer>

      {/* Social icons */}
      <div
        className={"containerH"}
        style={{gap: 16, justifyContent: "center", marginTop: 4, flexWrap: "wrap"}}>
        <DiscordLink style={iconStyle} className={linkClassname} showTitle={expanded} />
        <GitlabLink style={iconStyle} className={linkClassname} showTitle={expanded} />
        <ContactNOELink style={iconStyle} className={linkClassname} showTitle={expanded} />
      </div>
    </div>
  );
}
