import {ArrowUpOutlined, MinusCircleOutlined} from "@ant-design/icons";
import {SelectInput} from "@shared/inputs/SelectInput";
import {TextAreaInput} from "@shared/inputs/TextAreaInput";
import {TextInput} from "@shared/inputs/TextInput";
import {Stack} from "@shared/layout/Stack";
import {camelCaseify, generateRandomId} from "@shared/utils/stringUtilities";
import {Button, Collapse, Form, FormListOperation, Tabs, Tooltip} from "antd";
import {NamePath} from "rc-field-form/es/interface";
import React, {CSSProperties, useMemo} from "react";
import {useTranslation} from "react-i18next";
import {RequiredMark} from "../../RequiredMark";
import {useFieldCompsFormInstance} from "../atoms/useFieldCompsFormInstance";
import {useIsNewElement} from "../atoms/useIsNewElement";
import {useSafeAutoModifField} from "../atoms/useSafeAutoModifField";
import {fieldCompIcons} from "../fieldCompCustomProps/fieldCompIcons";
import {
  FieldCompInputType,
  fieldCompInputTypes,
  FieldCompType,
} from "../fieldCompCustomProps/FieldCompInputType";
import {FieldsBuilderConfig, FieldsBuilderI18n} from "../FieldsBuilder";
import {ConditionalFieldCompConfig} from "./ConditionalFieldCompConfig";
import {GeneralFieldCompConfig} from "./GeneralFieldCompConfig";

// Each array represents a list of types that are compatible with each other
// in the way they are stored in the databases.
const INTERCOMPATIBLE_FORM_COMP_TYPE_GROUPS: Array<Array<FieldCompInputType>> = [
  ["text", "longText", "url", "email"],
  ["radioGroup", "select"],
  ["checkboxGroup", "multiSelect"],
  ["datetime", "day"],
];

// Find an intercompatibility group that contains both typeA and typeB
const areInterCompatible = (typeA: FieldCompInputType, typeB: FieldCompInputType) =>
  INTERCOMPATIBLE_FORM_COMP_TYPE_GROUPS.find(
    (intercompatibleGroup) =>
      intercompatibleGroup.includes(typeA) && intercompatibleGroup.includes(typeB)
  );

export const createDatabaseKey = (inputString: string) => {
  if (!inputString) return "";

  // CamelCaseify the string
  const camelCaseString = camelCaseify(inputString);

  // Ensure the first character is a letter (database key requirements)
  const key = camelCaseString.charAt(0).match(/[a-zA-Z]/) ? camelCaseString : `_${camelCaseString}`;

  // Truncate the string if too long and add a nonce for uniqueness
  return `${key.slice(0, 30)}_${generateRandomId(3)}`;
};

export const FieldCompConfig = ({
  rootName,
  name,
  remove,
  moveDown,
  moveUp,
  config,
}: {
  rootName: NamePath;
  name: number;
  remove: FormListOperation["remove"];
  moveUp?: () => void;
  moveDown?: () => void;
  config: Omit<FieldsBuilderConfig, "i18nOverrides"> & {i18n: FieldsBuilderI18n};
}) => {
  const thisCompRootName = [...rootName, name];
  const {t} = useTranslation();
  const form = useFieldCompsFormInstance();
  const {isNewElement, hasFirstReRendered} = useIsNewElement(thisCompRootName);

  const keyNamePath: NamePath = [...thisCompRootName, "key"];
  const keyVal = Form.useWatch(keyNamePath, form);
  const modifKeyBasedOnValue = useSafeAutoModifField(thisCompRootName, "key", isNewElement);

  const typeVal = Form.useWatch([...thisCompRootName, "type"], form);
  const isInput = fieldCompInputTypes.includes(typeVal);
  const isPanel = typeVal === "panel";

  const customProps = config.customPropsConfig[typeVal as FieldCompInputType];
  const MainControls = customProps?.MainControls;

  const RemoveButton = () => (
    <Button onClick={() => remove(name)} danger type={"link"} icon={<MinusCircleOutlined />} />
  );

  const panelTitleLinesSkeleton = useMemo(() => {
    if (typeVal === "panel") {
      return (
        <Stack gap={1} mt={2}>
          {Array.from({length: 3}, () => (
            <div
              style={{
                minWidth: 100,
                width: Math.ceil(Math.random() * 10) + 40 + "%",
                height: 10,
                borderRadius: 4,
                background: "var(--text)",
              }}
            />
          ))}
        </Stack>
      );
    } else return null;
  }, [typeVal]);

  const FieldCompTitleExtras = config.FieldCompTitleExtras;

  const FieldCompTitle = () => {
    const labelVal = Form.useWatch([...thisCompRootName, "label"], form);
    const requiredVal = Form.useWatch([...thisCompRootName, "required"], form);

    return (
      <>
        {!keyVal ? (
          // New input title
          <span style={{color: "gray"}}>
            {isInput
              ? config.i18n.newFieldInputComp
              : isPanel
              ? config.i18n.newFieldPanelComp
              : t("fieldsBuilder:newFieldContentComp")}
          </span>
        ) : (
          // Existing input title
          <Stack row>
            <Stack row sx={{whiteSpace: "pre-wrap"}} gap={2}>
              <div title={t(`fieldsBuilder:types.${typeVal}`)}>
                {fieldCompIcons[typeVal as FieldCompType]}
              </div>
              {isInput ? (
                <span>
                  {requiredVal && <RequiredMark />}
                  {labelVal}
                </span>
              ) : isPanel ? (
                <strong>{labelVal}</strong>
              ) : (
                t("fieldsBuilder:types.content")
              )}
            </Stack>
            {FieldCompTitleExtras && <FieldCompTitleExtras componentRootName={thisCompRootName} />}
          </Stack>
        )}
        <div style={{opacity: keyVal ? 0.15 : 0.05}}>{panelTitleLinesSkeleton}</div>
      </>
    );
  };

  // Main component styles
  // - Make dashed border if new question with no key yet
  // - Make border bigger if panel component
  const collapseStyle: CSSProperties = {
    borderStyle: keyVal ? undefined : "dashed",
    borderWidth: isPanel ? "2px 2px 0 2px" : undefined, // Except bottom border, which is managed by the panel component
  };
  // Do the same but only for bottom border, the rest is managed by collapse component
  const collapsePanelStyle: CSSProperties = {
    borderBottomStyle: keyVal ? undefined : "dashed",
    borderBottomWidth: isPanel ? 2 : undefined,
  };

  return hasFirstReRendered ? (
    // Open the collapse if it is a newly created question, otherwise first display it as closed
    <Collapse
      defaultActiveKey={isNewElement ? "mainPanel" : undefined}
      size={config.nested ? "middle" : "large"} // If the component is nested in a panel component, make its size smaller
      style={collapseStyle}>
      <Collapse.Panel
        header={<FieldCompTitle />}
        style={collapsePanelStyle}
        extra={
          <Stack
            row
            alignCenter
            justifyCenter
            gap={2}
            my={"-4px"}
            onClick={(e) => e.stopPropagation()}
            height={"100%"}>
            {moveUp && (
              <Tooltip title={t("fieldsBuilder:move.up")}>
                <Button type={"text"} size={"small"} icon={<ArrowUpOutlined />} onClick={moveUp} />
              </Tooltip>
            )}
            {moveDown && (
              <Tooltip title={t("fieldsBuilder:move.down")}>
                <Button
                  type={"text"}
                  size={"small"}
                  icon={<ArrowUpOutlined style={{transform: "rotate(180deg)"}} />}
                  onClick={moveDown}
                />
              </Tooltip>
            )}
            <RemoveButton />
          </Stack>
        }
        key={"mainPanel"}>
        {isInput && (
          <div className="container-grid two-per-row">
            <TextAreaInput
              label={config.i18n.labelLabel}
              placeholder={config.i18n.labelPlaceholder}
              autoSize={{minRows: 1}}
              name={[name, "label"]}
              required
              autoFocus={isNewElement} // Focus if new
              onChange={(event) => {
                // If the key is not defined in the component value, it means it's a new component.
                // So pre-fill it unless the user touches the field
                modifKeyBasedOnValue(event.target.value);
              }}
            />

            <SelectInput
              name={[name, "type"]}
              label={config.i18n.typeLabel}
              placeholder={config.i18n.typePlaceholder}
              options={fieldCompInputTypes.map((type) => {
                // Disable option if the question is not new and if types are not compatible
                const disabled = !isNewElement && !areInterCompatible(typeVal, type);
                return {
                  label: (
                    <Stack row alignCenter>
                      <div style={{width: 18, textAlign: "center", marginRight: 8}}>
                        {fieldCompIcons[type as FieldCompType]}
                      </div>
                      {t(`fieldsBuilder:types.${type}`)}
                    </Stack>
                  ),
                  value: type,
                  disabled,
                  title: disabled ? t("fieldsBuilder:schema.type.typesAreIncompatible") : undefined,
                };
              })}
              popupMatchSelectWidth={false}
              required
            />
          </div>
        )}

        {MainControls && (
          <MainControls
            name={name}
            modifKeyBasedOnValue={modifKeyBasedOnValue}
            rootName={rootName}
            isNewElement={isNewElement}
            config={config}
          />
        )}

        <Tabs
          items={[
            ...(isInput
              ? [
                  {
                    label: t("fieldsBuilder:tabs.general"),
                    key: "general",
                    forceRender: true, // Force render so the key can be initialized to something
                    children: (
                      <GeneralFieldCompConfig
                        name={name}
                        customProps={customProps?.generalTab}
                        rootName={rootName}
                        config={config}
                      />
                    ),
                  },
                ]
              : []),
            ...((config.allowConditionals && [
              {
                label: t("fieldsBuilder:tabs.displayConditions"),
                key: "displayConditions",
                children: <ConditionalFieldCompConfig name={name} rootName={rootName} />,
              },
            ]) ||
              []),
            {
              label: t("fieldsBuilder:tabs.advanced"),
              key: "advanced",
              forceRender: true, // Force render so the key can be initialized to something
              children: (
                <TextInput
                  name={[name, "key"]}
                  disabled={!isNewElement}
                  label={t("fieldsBuilder:schema.key.label")}
                  placeholder={t("fieldsBuilder:schema.key.placeholder")}
                  required
                />
              ),
            },
          ]}
        />
      </Collapse.Panel>
    </Collapse>
  ) : null;
};
