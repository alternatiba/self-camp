import {Button, ButtonProps, Tag} from "antd";
import {CheckableTagProps} from "antd/es/tag";

type BaseToggleElementProps = {
  checked: boolean;
  onChange?: (checked: boolean) => void;
  checkedIcon: React.ReactNode;
  uncheckedIcon: React.ReactNode;
};

type ToggleButtonProps = ButtonProps & BaseToggleElementProps;
type ToggleTagProps = CheckableTagProps & BaseToggleElementProps;

export const ToggleButton = ({
  children,
  checked,
  onChange,
  type = "default",
  onClick,
  checkedIcon,
  uncheckedIcon,
  ...props
}: ToggleButtonProps) => {
  return (
    <Button
      type={checked ? "primary" : type}
      onClick={(e) => {
        onChange?.(!checked);
        onClick?.(e);
      }}
      style={{flexGrow: 0, ...props.style}}
      icon={checked ? checkedIcon : uncheckedIcon}
      {...props}>
      {children}
    </Button>
  );
};

export const ToggleTag = ({
  checked,
  onChange,
  checkedIcon,
  uncheckedIcon,
  children,
  ...props
}: ToggleTagProps) => {
  return (
    <Tag.CheckableTag checked={checked} onChange={(checked) => onChange?.(checked)} {...props}>
      {checked ? checkedIcon : uncheckedIcon} {children}
    </Tag.CheckableTag>
  );
};
