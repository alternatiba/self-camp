import {useTranslation} from "react-i18next";
import {useParams} from "react-router-dom";
import {Button} from "antd";
import {normalize} from "@shared/utils/stringUtilities";
import {CloseOutlined} from "@ant-design/icons";
import {useWindowDimensions} from "@shared/hooks/useWindowDimensions";
import {useLocalStorageState} from "@shared/utils/localStorageUtilities";
import {fallbackLng} from "@shared/services/i18n";
import {GenericAnnouncementBanner} from "./GenericAnnouncemenBanner";

export const AnnouncementMessageBanner = () => {
  const {t, i18n} = useTranslation();
  const {"*": page} = useParams();
  const {isMobileView} = useWindowDimensions();
  // Don't display the banner on the agenda and welcome pages as it causes the page to jump
  const isOnPageWhereShouldnotDisplay = ["agenda", "welcome"].some((endpoint) =>
    page?.includes(endpoint)
  );

  const announcementMessageInDefaultLanguage =
    process.env[`REACT_APP_ANNOUNCEMENT_MESSAGE_${fallbackLng.toUpperCase()}`];
  const announcementMessageInCurrentLanguage =
    process.env[`REACT_APP_ANNOUNCEMENT_MESSAGE_${i18n.language.toUpperCase()}`];

  // Always be sure to have a key (sometimes announcement messages are not defined in the default language)
  const localStorageKey =
    "announcement-banner-" +
    normalize(announcementMessageInDefaultLanguage || announcementMessageInCurrentLanguage || "")
      .replace(" ", "-")
      .slice(0, 50);
  const [isBannerClosed, setIsBannerClosed] = useLocalStorageState(localStorageKey, false);

  // Display the real announcement message in the current language, or in the default one if not defined in the current language
  const announcementMessageToDisplay =
    announcementMessageInCurrentLanguage || announcementMessageInDefaultLanguage;

  return announcementMessageToDisplay && !isOnPageWhereShouldnotDisplay && !isBannerClosed ? (
    <div className={"full-width-content"}>
      <GenericAnnouncementBanner
        htmlString={announcementMessageToDisplay}
        styles={{
          container: isMobileView
            ? undefined
            : {
                borderRadius: 20,
                margin: "8px 8px 0 8px",
              },
          content: {
            padding: "4px 12px",
          },
        }}
        button={
          <Button
            icon={<CloseOutlined />}
            type={"link"}
            size={"small"}
            style={{fontSize: 12, flexGrow: 0}}
            onClick={() => setIsBannerClosed(true)}>
            {!isMobileView && t("common:close")}
          </Button>
        }
      />
    </div>
  ) : null;
};
