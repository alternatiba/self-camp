const express = require("express");
const i18next = require("i18next");
const i18nextMiddleware = require("i18next-http-middleware");
const fetch = require("node-fetch");
const path = require("path");
const fs = require("fs");
const helmet = require("helmet");
const {createProxyMiddleware} = require("http-proxy-middleware");
const compression = require("compression");
const parseHtml = require("node-html-parser").parse;

const app = express();

/*
 * CONSTANTS
 */

// Server type: either orga or inscription
const PORT = process.env.PORT ?? 5020;
const SERVER_TYPE = "inscription";
const instanceName = process.env.REACT_APP_INSTANCE_NAME || "NOÉ";

/*
  SECURITY
*/

const helmetNoCrossOrigin = helmet({
  crossOriginResourcePolicy: {policy: "same-site"},
  contentSecurityPolicy: false,
});
const helmetAllowedCrossOrigin = helmet({
  crossOriginResourcePolicy: {policy: "cross-origin"},
  contentSecurityPolicy: true,
});
// Allow cross-origin request for favicons. For the rest, stay same-site.
app.use((req, res, next) =>
  req.path.includes("favicon")
    ? helmetAllowedCrossOrigin(req, res, next)
    : helmetNoCrossOrigin(req, res, next)
);

app.disable("x-powered-by");

/*
  I18N SETUP
*/

const i18nFolderPath = path.join(__dirname, "..", "shared", "locales");

const i18nParams = {
  resources: {
    fr: require(path.join(i18nFolderPath, "fr", "dynamicServer.json"))[SERVER_TYPE],
    en: require(path.join(i18nFolderPath, "en", "dynamicServer.json"))[SERVER_TYPE],
  },
  fallbackLng: "fr",
  defaultNS: "common",
  preload: ["en", "fr"],
};

i18next.use(i18nextMiddleware.LanguageDetector).init(i18nParams);
app.use(i18nextMiddleware.handle(i18next));

/*
  SENTRY TUNNEL
*/

if (process.env.REACT_APP_SENTRY_DSN) {
  const {host: sentryHost, pathname} = new URL(process.env.REACT_APP_SENTRY_DSN);
  const sentryProjectId = pathname.slice(1);
  app.use(
    "/sentry-tunnel",
    createProxyMiddleware({
      target: `https://${sentryHost}/api/${sentryProjectId}/envelope/`,
      changeOrigin: true,
      pathRewrite: {"^/sentry-tunnel": "/"},
    })
  );
}

/*
  MATOMO TUNNEL
*/

if (process.env.REACT_APP_MATOMO_CLOUD_URL && process.env.REACT_APP_MATOMO_CONTAINER_JS_URL) {
  // Serve the matomo container JS file
  app.get("/m/a/t/o/m/o/-/j/s", compression(), async (req, res) => {
    try {
      const matomoJSResponse = await fetch(process.env.REACT_APP_MATOMO_CONTAINER_JS_URL);
      res.set("Content-Type", "text/javascript");
      res.send(await matomoJSResponse.text());
    } catch (error) {
      console.error("Error fetching Matomo container JS:", error);
      return res.status(500).send("Error fetching Matomo container JS");
    }
  });

  // Proxy the matomo data to the matomo cloud URL
  app.use(
    "/m/a/t/o/m/o/-/p/h/p",
    createProxyMiddleware({
      target: process.env.REACT_APP_MATOMO_CLOUD_URL,
      secure: false,
      changeOrigin: true,
      pathRewrite: {"^/m/a/t/o/m/o/./p/h/p": "/matomo.php"},
    })
  );
}

/*
  JS/CSS FILES COMPRESSED REDIRECTION
*/

const compressedStaticMiddleware = (contentType) => (req, res, next) => {
  if (req.headers["accept-encoding"]?.includes("br")) {
    // Brotli is the most efficient compression
    req.url = req.url + ".br";
    res.set("Content-Encoding", "br");
  } else if (req.headers["accept-encoding"]?.includes("gzip")) {
    // Otherwise serve Gzip
    req.url = req.url + ".gz";
    res.set("Content-Encoding", "gzip");
  }

  // Force content mime type to JS for the browser https://stackoverflow.com/a/60759499
  res.set("Content-Type", contentType);

  return express.static(path.resolve(__dirname, "build"), {maxAge: "30d"})(req, res, next);
};

// Redirect all JS and CSS files to their compressed variant, so the download is quicker
// WARNING : /service-worker.js should not be compressed, that is why we narrow using "/static/*.js"
app.get("/static/*.js", compressedStaticMiddleware("application/javascript"));
app.get("/static/*.css", compressedStaticMiddleware("text/css"));

/*
  STATIC FILES
*/

// Every resource that has an extension should be served by the static middleware.
app.get("*.*", express.static(path.resolve(__dirname, "build"), {maxAge: "30d"}));

/*
  COMMON INDEX.HTML BUILD FUNCTION
*/
// Generic function to build the index files
const indexPath = path.resolve(__dirname, "build", "index.html");

/**
 * Build the index file
 * @param {Express.Request} req
 * @param {Express.Response} res
 * @param {"project" | "default" | "session"} type
 * @param {{
 *    project?: {name: string, socialImageUrl?:string},
 *    session?: {activity: {name: string, summary?: string}}
 * }} context
 */
const buildIndexFile = (req, res, type, {project, session} = {}) => {
  fs.readFile(indexPath, "utf8", async (err, indexHtmlString) => {
    if (err) {
      console.error("Error during file reading", err);
      return res.sendStatus(404);
    }

    const tContext = {
      projectName: project?.name,
      activityName: session?.activity?.name,
      instanceName,
    };
    const tags = {
      title: req.t(`${type}.title`, tContext),
      description:
        session?.activity?.summary ||
        (session?.activity?.description && parseHtml(session?.activity?.description).innerText) ||
        req.t(`project.description`, tContext),
      socialImageUrl:
        project?.socialImageUrl ||
        `${process.env.REACT_APP_API_URL}/assets/images/social-card-${SERVER_TYPE}-front.jpg?v=1`,
    };

    indexHtmlString = indexHtmlString
      .replaceAll("NOÉ", tags.title)
      .replaceAll("%DESCRIPTION%", tags.description)
      .replaceAll("%SOCIAL_IMAGE_URL%", tags.socialImageUrl);

    return res.send(indexHtmlString);
  });
};

const getProject = async (projectId) => {
  try {
    return await fetch(
      `${process.env.REACT_APP_API_INTERNAL_URL}/projects/${projectId}/public`
    ).then((r) => r.json());
  } catch {
    return null;
  }
};

// // TODO Session index information not currently working. Waiting for a proper way to fetch "public" data about sessions (ie. public view of the planning)
// const getSession = async (projectId, id) => {
//   try {
//     return await fetch(
//       `${process.env.REACT_APP_API_INTERNAL_URL}/projects/${projectId}/sessions/${id}`
//     ).then((r) => r.json());
//   } catch {
//     return null;
//   }
// };

/*
  DEFAULT INDEX SERVE
*/

// First, serve common routes we often land to with default index
app.get(
  ["/", "/projects", "/public-projects", "/new", "/login", "/signup"],
  async function returnDefaultIndex(req, res) {
    return buildIndexFile(req, res, "default");
  }
);

/*
  SESSION INDEX SERVE
*/

// // TODO Session index information not currently working. Waiting for a proper way to fetch "public" data about sessions (ie. public view of the planning)
// // Then serve specific routes for sessions
// app.get(
//   [
//     "/:projectId/sessions/agenda", // ex: https://noe-app.io/my-project/sessions/agenda?session=670e49824faffdb0f66b6446
//     "/:projectId/sessions/:allOrSubscribed/agenda", // ex: https://noe-app.io/my-project/sessions/all/agenda?session=670e49824faffdb0f66b6446
//     "/:projectId/sessions/:id", // ex: https://noe-app.io/my-project/sessions/670e49824faffdb0f66b6446
//   ],
//   async function returnSessionIndex(req, res) {
//     // If no project found, serve the default index
//     const project = await getProject(req.params.projectId);
//     if (!project) return buildIndexFile(req, res, "default");
//
//     // If project found but no session found, serve the project's index
//     const session = await getSession(project._id, req.params.id || req.query.session);
//     if (!session) return buildIndexFile(req, res, "project", {project});
//
//     // If project and session found, serve the session's index
//     return buildIndexFile(req, res, "session", {project, session});
//   }
// );

/*
  PROJECT INDEX SERVE
*/

// Then only, serve routes that begin with project ID directly.
app.get(["/:projectId/*", "/:projectId"], async function returnProjectIndex(req, res) {
  // If no project found, serve the default index
  const project = await getProject(req.params.projectId);
  if (!project) return buildIndexFile(req, res, "default");

  // If project found, serve the project's index
  return buildIndexFile(req, res, "project", {project});
});

/*
  THE REST
 */

// All the rest: 404
app.use((req, res) => {
  res.status(404).send("Sorry, can't find that!");
});

// Global error Handler
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something broke, oops!");
});

/*
  START SERVER
 */

// Set up the server to listen on the right port
app.listen(PORT, (error) => {
  if (error) return console.log("Error during app startup", error);
  console.log("Dynamic server listening on " + PORT + "...");
});
